insert into TFS_RECETTE.CATEGORY (ID, CATEGORY_NAME_EN, CATEGORY_NAME_FR) values (1, 'Employee', 'Salarié ');
insert into TFS_RECETTE.CATEGORY (ID, CATEGORY_NAME_EN, CATEGORY_NAME_FR) values (2, 'Professional Liberal', 'Professionnel Libéral');
insert into TFS_RECETTE.CATEGORY (ID, CATEGORY_NAME_EN, CATEGORY_NAME_FR) values (3, 'Trader', 'Commerçant');
insert into TFS_RECETTE.CATEGORY (ID, CATEGORY_NAME_EN, CATEGORY_NAME_FR) values (4, 'Artisan', 'Artisan');
insert into TFS_RECETTE.CATEGORY (ID, CATEGORY_NAME_EN, CATEGORY_NAME_FR) values (5, 'Farmer', 'Agriculteur');
insert into TFS_RECETTE.CATEGORY (ID, CATEGORY_NAME_EN, CATEGORY_NAME_FR) values (6, 'Retired', 'Retraité');
