package com.attijeri.tfs.web.Utils;

import io.github.bucket4j.Bandwidth;
import io.github.bucket4j.Bucket;
import io.github.bucket4j.Bucket4j;
import io.github.bucket4j.Refill;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.time.Duration;

@Component
public class RateLimitToken {
    private Logger logger = LoggerFactory.getLogger(RateLimitToken.class.getName());

    public Bucket bucket;

    public RateLimitToken() {
        Refill refill = Refill.intervally(20, Duration.ofMinutes(1));
        Bandwidth limit = Bandwidth.classic(20, refill);
        this.bucket = Bucket4j.builder().addLimit(limit).build();
    }
}
