package com.attijeri.tfs.web.Utils;

import org.apache.commons.io.FilenameUtils;
import org.joda.time.Days;
import org.joda.time.LocalDate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;

@Service
public class StorageService {
    @Value("${upload.path}")
    private String uploads;

    @Value("${deblocage.path}")
    private String deblocage;

    public String store(MultipartFile file, long idCredit, long idCheck) {

      /*     file.getOriginalFilename().replace(file.getOriginalFilename(), FilenameUtils.getBaseName(idCheck+'-'+idCredit + "." + FilenameUtils.getExtension(file.getOriginalFilename())).toLowerCase());
           Files.copy(file.getInputStream(), this.rootLocation.resolve(file.getOriginalFilename()));
        } catch (Exception e) {

            System.out.println("FAIL "+e.getMessage());;
           throw new RuntimeException("FAIL!");
        }*/

        String filename = FilenameUtils.getBaseName(idCredit +"-"+idCheck+"-"+file.getOriginalFilename()+"." + FilenameUtils.getExtension(file.getOriginalFilename())).toLowerCase();
        Path saveTO = Paths.get(uploads +"/"+ filename);
        try {
            try {
                Files.copy(file.getInputStream(), saveTO);
            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException("Error : " + e.getMessage());
            }
            return filename;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public Resource loadFile(String filename) {
        try {
            File fil = new File(getClass().getClassLoader().getResource("").getFile());
            Path filePath = Paths.get(fil.getPath());
            System.out.println("filepth"+filePath);
            Path rootLocation = Paths.get(fil.getPath());
            Path file = rootLocation.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            System.out.print("ressource : "+resource);
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("FAIL!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("FAIL!");
        }
    }

    public Resource loadFileDeblocage(String filename) {
        try {
            File fil = new File(getClass().getClassLoader().getResource("").getFile());
            Path filePath = Paths.get(fil.getPath());
            System.out.println("filepth"+filePath);
            Path rootLocation = Paths.get(fil.getPath());
            Path file = rootLocation.resolve(filename);
            Resource resource = new UrlResource(file.toUri());
            System.out.print("ressource : "+resource);
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new RuntimeException("FAIL!");
            }
        } catch (MalformedURLException e) {
            throw new RuntimeException("FAIL!");
        }
    }

    public void deleteAll() {
        Path rootLocation = Paths.get(uploads);
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }


    public char[] genOtp(int len)
    {
        //System.out.println("Generating password using random() : ");
        //System.out.print("Your new password is : ");

        // A strong password has Cap_chars, Lower_chars,
        // numeric value and symbols. So we are using all of
        // them to generate our password
        String Capital_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        // String Small_chars = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";
        String symbols = "!@#$%^&*_=+-/.?<>)";


        // String values = Capital_chars + Small_chars + numbers + symbols;
        String values = Capital_chars +  numbers ;
        // Using random method
        Random rndm_method = new Random();

        char[] password = new char[len];

        for (int i = 0; i < len; i++)
        {
            // Use of charAt() method : to get character value
            // Use of nextInt() as it is scanning the value as int
            password[i] =
                values.charAt(rndm_method.nextInt(values.length()));

        }
        return password;
    }
    public String genReference(int len, String CIN)
    {
        //System.out.println("Generating password using random() : ");
        //System.out.print("Your new password is : ");

        // A strong password has Cap_chars, Lower_chars,
        // numeric value and symbols. So we are using all of
        // them to generate our password
        String Capital_chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String Small_chars = "abcdefghijklmnopqrstuvwxyz";
        String numbers = "0123456789";
        String symbols = "!@#$%^&*_=+-/.?<>)";

        String CINSub= CIN.substring(4);
        // String values = Capital_chars + Small_chars + numbers + symbols;
        String values = Capital_chars;
        // Using random method
        Random rndm_method = new Random();

        char[] password = new char[len];

        for (int i = 0; i < len; i++)
        {
            // Use of charAt() method : to get character value
            // Use of nextInt() as it is scanning the value as int
            password[i] =
                values.charAt(rndm_method.nextInt(values.length()));

        }
        CINSub= CINSub.concat(String.valueOf(password));

        return CINSub;
    }
    public Double calculateAge(Date birthDate)
    {
        int years = 0;
        int months = 0;
        int days = 0;

        //create calendar object for birth day
        Calendar birthDay = Calendar.getInstance();
        birthDay.setTimeInMillis(birthDate.getTime());

        //create calendar object for current day
        long currentTime = System.currentTimeMillis();
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(currentTime);

        //Get difference between years
        years = now.get(Calendar.YEAR) - birthDay.get(Calendar.YEAR);
        int currMonth = now.get(Calendar.MONTH) + 1;
        int birthMonth = birthDay.get(Calendar.MONTH) + 1;

        //Get difference between months
        months = currMonth - birthMonth;

        //if month difference is in negative then reduce years by one
        //and calculate the number of months.
        if (months < 0)
        {
            years--;
            months = 12 - birthMonth + currMonth;
            if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
                months--;
        } else if (months == 0 && now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
        {
            years--;
            months = 11;
        }

        //Calculate the days
        if (now.get(Calendar.DATE) > birthDay.get(Calendar.DATE))
            days = now.get(Calendar.DATE) - birthDay.get(Calendar.DATE);
        else if (now.get(Calendar.DATE) < birthDay.get(Calendar.DATE))
        {
            int today = now.get(Calendar.DAY_OF_MONTH);
            now.add(Calendar.MONTH, -1);
            days = now.getActualMaximum(Calendar.DAY_OF_MONTH) - birthDay.get(Calendar.DAY_OF_MONTH) + today;

        }
        else
        {
            days = 0;
            if (months == 12)
            {
                years++;
                months = 0;
            }
        }
        System.out.println("years="+years);
        System.out.println("months="+months*1.000);
        System.out.println("days="+days*1.000);

        double age  = years + months + days;

        System.out.print("age=="+age);
        return age;
    }
    public int calculateAgeWithJodaTime(
        org.joda.time.LocalDate birthDate,long duree, int maxYears) {
        //validate inputs ...

        LocalDate currentDate= LocalDate.now();
        LocalDate currentDates =currentDate.minusYears(maxYears);
        LocalDate birthDates= birthDate.minusMonths((int)duree);
        System.out.println("currentDates=="+currentDates);
        System.out.println("birthDates=="+birthDates);
        Days age = Days.daysBetween(birthDates, currentDates);
        return age.getDays();
    }
    public  int getAge(Date dateOfBirth) {

        Calendar today = Calendar.getInstance();
        Calendar birthDate = Calendar.getInstance();

        int age = 0;

        birthDate.setTime(dateOfBirth);
        if (birthDate.after(today)) {
            throw new IllegalArgumentException("Can't be born in the future");
        }

        age = today.get(Calendar.YEAR) - birthDate.get(Calendar.YEAR);

        // If birth date is greater than todays date (after 2 days adjustment of leap year) then decrement age one year
        if ( (birthDate.get(Calendar.DAY_OF_YEAR) - today.get(Calendar.DAY_OF_YEAR) > 3) ||
            (birthDate.get(Calendar.MONTH) > today.get(Calendar.MONTH ))){
            age--;

            // If birth date and todays date are of same month and birth day of month is greater than todays day of month then decrement age
        }else if ((birthDate.get(Calendar.MONTH) == today.get(Calendar.MONTH )) &&
            (birthDate.get(Calendar.DAY_OF_MONTH) > today.get(Calendar.DAY_OF_MONTH ))){
            age--;
        }

        return age;
    }
    public void init() {
        try {
            Path rootLocation = Paths.get(uploads);
            Files.createDirectory(rootLocation);
        } catch (IOException e) {
            System.out.println(e.getMessage());
            throw new RuntimeException("Could not initialize storage!");
        }
    }

    public String storeFileDeblocage(String bytes,String path) {
        File file = new File (path);
        try {
            try (FileOutputStream fos = new FileOutputStream(file); ) {
                byte[] decoder = Base64.getDecoder().decode(bytes);
                fos.write(decoder);
                System.out.println("PDF File Saved");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return path;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    public String storeContrat(String path) throws IOException {
        File fil = new File(getClass().getClassLoader().getResource("Bulletin de souscription PRIVILEGES BLEDI.pdf").getFile());
        Path filePath = Paths.get(fil.getPath());
        byte[] bytes = Files.readAllBytes(filePath);
        //  byte[] fileContent = FileUtils.readFileToByteArray(fil);
        File file = new File (path);
        try {
            try (FileOutputStream fos = new FileOutputStream(file); ) {
                byte[] decoder = Base64.getDecoder().decode(bytes);
                fos.write(decoder);
                System.out.println("PDF File Saved");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return path;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public byte[] dowanloadContrat(HttpServletResponse response)  throws IOException{
        final byte[] buf = new byte[1024];
        File file = new File(getClass().getClassLoader().getResource("Bulletin de souscription PRIVILEGES BLEDI.pdf").getFile());
        Path filePath = Paths.get(file.getPath());
        byte[] fileContent = Files.readAllBytes(filePath);
        response.setHeader("Content-Disposition", file.getName());
        response.setContentType("application/pdf");
        //  response.setContentType("application/doc"); // pour fiche type doc
        return fileContent;

    }
}
