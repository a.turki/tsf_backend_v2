package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.config.optConfig.MiddleWareService;
import com.attijeri.tfs.domain.Request;
import com.attijeri.tfs.repository.RequestRepository;
import com.attijeri.tfs.service.PersonalInfoService;
import com.attijeri.tfs.service.RequestService;
import com.attijeri.tfs.service.dto.RequestDTO;
import com.attijeri.tfs.service.dto.UserChoiceDTO;
import com.attijeri.tfs.web.rest.errors.BadRequestAlertException;
import com.attijeri.tfs.service.dto.PersonalInfoDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Nullable;
import java.net.URI;
import java.net.URISyntaxException;
import java.time.LocalDate;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.attijeri.tfs.domain.PersonalInfo}.
 */
@RestController
@RequestMapping("/api")
public class PersonalInfoResource {
    private static final String ENTITY_NAME = "personalinfo";
    private final Logger log = LoggerFactory.getLogger(PersonalInfoResource.class);
    private final MiddleWareService middleWareService;
    private final RequestService requestService;
    private final PersonalInfoService personalInfoService;
    private final RequestRepository requestRepository;
    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    public PersonalInfoResource(RequestRepository requestRepository, PersonalInfoService personalInfoService, MiddleWareService middleWareService, RequestService requestService) {
        this.personalInfoService = personalInfoService;
        this.middleWareService = middleWareService;
        this.requestService = requestService;
        this.requestRepository = requestRepository;
    }

    /**
     * {@code GET  /personal-infos} : get all the personalInfos.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of personalInfos in body.
     */
    @GetMapping("/personal-infos")
    public List<PersonalInfoDTO> getAllPersonalInfos() {
        log.debug("REST request to get all PersonalInfos");
        return personalInfoService.findAll();
    }

    /**
     * {@code GET  /personal-infos/:id} : get the "id" personalInfo.
     *
     * @param id the id of the personalInfoDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the personalInfoDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/personal-infos/{id}")
    public ResponseEntity<PersonalInfoDTO> getPersonalInfo(@PathVariable Long id) {
        log.debug("REST request to get PersonalInfo : {}", id);
        Optional<PersonalInfoDTO> personalInfoDTO = personalInfoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(personalInfoDTO);
    }

    @GetMapping("/personal")
    public void sendMail() {
        // middleWareService.sendConfirmationEmail("Confirmation du mail");
    }

    @PostMapping(value = "/addNewRequest/{idOffer}")
    public ResponseEntity<PersonalInfoDTO> addNewRequest(@RequestBody UserChoiceDTO UserChoiceDTO,@PathVariable Long idOffer) throws URISyntaxException {
        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setOfferId(idOffer);
        requestDTO.setBankAccounts(UserChoiceDTO.getAccounts());
        requestDTO.setSendingMailDate(LocalDate.now());
        requestDTO.setState(false);
        requestDTO.setCodeVerification("");
        requestDTO.setStep("Confirmation Mail");
        PersonalInfoDTO personalInfoDTO1 = new PersonalInfoDTO();
        personalInfoDTO1.setCivility(UserChoiceDTO.getCivility());
        personalInfoDTO1.setFirstName(UserChoiceDTO.getFirstName());
        personalInfoDTO1.setLastName(UserChoiceDTO.getLastName());
        personalInfoDTO1.setEmail(UserChoiceDTO.getEmail());
        personalInfoDTO1.setPhone(UserChoiceDTO.getPhone());
        personalInfoDTO1.setNativeCountry(UserChoiceDTO.getNativeCountry());
        personalInfoDTO1.setBirthday(UserChoiceDTO.getBirthday());
        RequestDTO dto = requestService.save(requestDTO);
        personalInfoDTO1.setRequestId(dto.getId());
        String encodedString = Base64.getEncoder().encodeToString(dto.getId().toString().getBytes());
        System.out.println(encodedString);
        PersonalInfoDTO personalInfoDTO2 = personalInfoService.save(personalInfoDTO1);
        middleWareService.sendConfirmationEmail(personalInfoDTO2, "Validation du mail", UserChoiceDTO.getLangue());

        if (personalInfoDTO2 != null) {
            return new ResponseEntity<>(personalInfoDTO2, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/updatePersonalInfo")
    public ResponseEntity<PersonalInfoDTO> update(@RequestBody PersonalInfoDTO personalInfo) {
        log.debug("REST request to update Personal Info : {}", personalInfo.getId());

        PersonalInfoDTO savedDTO = personalInfoService.update(personalInfo);
        if (savedDTO != null) {
            return new ResponseEntity<>(savedDTO, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/findByRequestId/{requestId}")
    public ResponseEntity<PersonalInfoDTO> findByRequestId(@PathVariable Long requestId) {
        PersonalInfoDTO personalInfoDTO = personalInfoService.findByRequestId(requestId);
        return new ResponseEntity<>(personalInfoDTO, HttpStatus.OK);
    }

    @GetMapping("/RequestRegistration/{idRequest}/{stepName}")
    public ResponseEntity<PersonalInfoDTO> RequestRegistration(@PathVariable Long idRequest, @PathVariable String stepName) throws URISyntaxException {
        PersonalInfoDTO personalInfoDTO = personalInfoService.findByRequestId(idRequest);
        System.out.println(personalInfoDTO);
        RequestDTO requestDTO = requestService.findOne(idRequest).get();
        requestDTO.setStep(stepName);
        requestService.save(requestDTO);
        middleWareService.sendRequestRegistrationMail(personalInfoDTO, "Validation du mail", idRequest);
        if (personalInfoDTO != null) {
            return new ResponseEntity<>(personalInfoDTO, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping(value = "/addRequest")
    public ResponseEntity<PersonalInfoDTO> addNewRequest(@RequestBody UserChoiceDTO UserChoiceDTO) throws URISyntaxException {
        RequestDTO requestDTO = new RequestDTO();
        requestDTO.setBankAccounts(UserChoiceDTO.getAccounts());
        requestDTO.setSendingMailDate(LocalDate.now());
        requestDTO.setState(false);
        PersonalInfoDTO personalInfoDTO1 = new PersonalInfoDTO();
        personalInfoDTO1.setCivility(UserChoiceDTO.getCivility());
        personalInfoDTO1.setFirstName(UserChoiceDTO.getFirstName());
        personalInfoDTO1.setLastName(UserChoiceDTO.getLastName());
        personalInfoDTO1.setEmail(UserChoiceDTO.getEmail());
        personalInfoDTO1.setPhone(UserChoiceDTO.getPhone());
        personalInfoDTO1.setNativeCountry(UserChoiceDTO.getNativeCountry());
        personalInfoDTO1.setBirthday(UserChoiceDTO.getBirthday());
        RequestDTO dto = requestService.save(requestDTO);
        personalInfoDTO1.setRequestId(dto.getId());
        PersonalInfoDTO personalInfoDTO2 = personalInfoService.save(personalInfoDTO1);
        middleWareService.sendConfirmationEmail(personalInfoDTO2, "Validation du mail", UserChoiceDTO.getLangue());

        if (personalInfoDTO2 != null) {
            return new ResponseEntity<>(personalInfoDTO2, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
