package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.config.optConfig.WebServiceSign;
import com.attijeri.tfs.domain.AuthentitficationToSign;
import com.attijeri.tfs.domain.PersonalInfo;
import com.attijeri.tfs.repository.AuthentitficationToSignRepository;
import com.attijeri.tfs.repository.PersonalInfoRepository;
import com.attijeri.tfs.service.*;
import com.attijeri.tfs.service.dto.*;
import com.attijeri.tfs.web.Utils.BusinessResourceException;
import com.attijeri.tfs.web.Utils.StorageService;
import com.google.common.io.ByteStreams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang.time.DateUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;

@RestController
@RequestMapping("/api")
public class RestSignDoc {

    @Autowired
    private WebServiceSign webServiceSign;

    @Autowired
    private RequestService requestService;

    @Autowired
    private StorageService storageService;

    @Autowired
    private DocumentService documentService;

    @Autowired
    private PersonalInfoService personalInfoService;

    @Autowired
    private AuthentitficationToSignRepository iAuthentificationToSign;

    @Autowired
    private PersonalInfoRepository iInfosPers;


    @Autowired
    private RequiredDocService requiredDocService;

    private static final Logger logger = LogManager.getLogger(RestSignDoc.class);

    @ApiOperation(value = "Authentification", hidden = true)
    @PostMapping("/login")
    public HashMap<String, String> login() throws NoSuchAlgorithmException, KeyStoreException, KeyManagementException {
        HashMap<String, String> hashMap = new HashMap<>();
        String sessionId = WebServiceSign.wsAuthetificationSign();
        System.out.println("session Id of authentification sign:" + sessionId);
        hashMap.put("sessionId", sessionId);
        return hashMap;
    }

    @ApiOperation(value = "addDocument", hidden = true)
    @PostMapping("/addDocument/{idRequest}")
    public ResponseEntity<HashMap<String, String>> addDocument(@PathVariable("idRequest") Long idRequest) {
        try {
            HashMap<String, String> hashMap = new HashMap<>();
            DocumentDTO documentDTO = documentService.findDocumentByRequestId(idRequest);
            RequestDTO requestDTO = requestService.findByRequestId(idRequest);
            String fileName = null;
            // get the id credit
            // get the email from demande credit

            //   BigDecimal idCredit = docsDeblocage.getIdCredit();
            PersonalInfoDTO personalInfoDTO = personalInfoService.findByRequestId(idRequest);
            String emailAdress = personalInfoDTO.getEmail();
            System.out.println(requestDTO.getOfferId());
            //String emailAdress = "mohamedamine.bader@teamwillgroup.com";

            //System.out.println("add document  of get docsDeblocage:" + docsDeblocage);
            if (emailAdress != null) {
                if (requestDTO.getOfferId() == 1) {
                    fileName = "Bulletin de souscription pack BLEDI+.pdf";
                } else if (requestDTO.getOfferId() == 2) {
                    fileName = "Bulletin de souscription PRIVILEGES BLEDI.pdf";
                } else {
                    fileName = "Convention de compte.pdf";
                }
                // String fileName = "auter2.pdf";
                // load Resource
                Resource resource = this.storageService.loadFileDeblocage(fileName);
                //Resource resource = "URL [file:/C:/uploads/47000-9-demande_credit_to_signed.pdf]";

                InputStream inputStream = resource.getInputStream();
                byte[] bytes = ByteStreams.toByteArray(inputStream);
                //byte[] bytes = FileUtils.readFileToByteArray(resource.getFile());
                byte[] encoded = java.util.Base64.getEncoder().encode(bytes);
                String byteString = new String(encoded, StandardCharsets.UTF_8);


                //byte[] fileContent = FileUtils.readFileToByteArray(new File("file:/C:/deblocage/demande_credit_to_signed"));
                //String byteString = Base64.getEncoder().encodeToString(fileContent);
                //String byteString = new String (encodedString, StandardCharsets.UTF_8);

                String sessionId = WebServiceSign.wsAuthetificationSign();
                String dossierId = WebServiceSign.wsAddDocument(sessionId, byteString, fileName, emailAdress, requestDTO);
                System.out.println("dossier Id of add document:" + dossierId);
                if (dossierId != null) {
                    documentDTO.setIdDossierSigned(dossierId);
                    documentDTO.setNomFichier(fileName);
                    documentService.save(documentDTO);
                }
                hashMap.put("dossierId", dossierId);
                hashMap.put("emailAdress", emailAdress);
                return ResponseEntity.ok(hashMap);
            } else
                hashMap.put("error", "document does not exist");
            return ResponseEntity.ok(hashMap);

        } catch (Exception e) {
            logger.info("exeption -> addDocument" + e.getMessage());
            throw new BusinessResourceException("technical Error", "technical Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @ApiOperation(value = "getDocument", hidden = true)
    @GetMapping("/getDocument/{dossierId}")
    public ResponseEntity<HashMap<String, String>> getDocument(@PathVariable("dossierId") String dossierId) throws IOException {
        try {
            String sessionId = WebServiceSign.wsAuthetificationSign();
            HashMap<String, String> hashMap = new HashMap<>();
            String bytes = WebServiceSign.wsGetDocument(sessionId, dossierId);
            if (bytes != null) {
                DocumentDTO deblocageEntity = documentService.findDocumentByDossierId(dossierId);
                System.out.println("------------------------------" + deblocageEntity);
                if (deblocageEntity != null && deblocageEntity.getNomFichier() != null && deblocageEntity.getEmplacement() != null) {
                    String path = deblocageEntity.getEmplacement() + deblocageEntity.getNomFichier();
                    this.storageService.storeFileDeblocage(bytes, path);
                    deblocageEntity.setHasSigned(true);
                    documentService.save(deblocageEntity);
                }
            }
            System.out.println("bytes of get document:" + bytes);
            hashMap.put("bytes", bytes);
            return ResponseEntity.ok(hashMap);
        } catch (Exception e) {
            logger.info("exeption -> addDocument" + e.getMessage());
            throw new BusinessResourceException("technical Error", "technical Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }


    public void getDoc() throws IOException {
        String path = "contrat.pdf";
        this.storageService.loadFile(path);

    }


    @RequestMapping(value = "/downloadFile", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<Resource> getFoos(@RequestParam(required = false) Long idOffer,HttpServletRequest request) {
        try {
            Resource resource = null;
            // Load file as Resource

            if (idOffer == 1) {
                resource = storageService.loadFile("Bulletin de souscription pack BLEDI+.pdf");
            } else if (idOffer == 2) {
                resource = storageService.loadFile("Bulletin de souscription PRIVILEGES BLEDI.pdf");
            }
                // Try to determine file's content type
                String contentType = null;
                try {
                    contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
                } catch (IOException ex) {
                    logger.info("Could not determine file type.");
                }

                // Fallback to the default content type if type could not be determined
                if (contentType == null) {
                    contentType = "application/octet-stream";
                }

                return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(contentType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                    .body(resource);


            } catch(Exception e){
                throw new BusinessResourceException("technical Error", "technical Error", HttpStatus.INTERNAL_SERVER_ERROR);
            }

    }

        @GetMapping(value = "/download")
        @ResponseBody
        public ResponseEntity<Resource> addDoc(HttpServletRequest request) {
            try {
                Resource resource = null;
                // Load file as Resource


                resource = storageService.loadFile("Bulletin de souscription PRIVILEGES BLEDI.pdf");

                // Try to determine file's content type
                String contentType = null;
                try {
                    contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
                } catch (IOException ex) {
                    logger.info("Could not determine file type.");
                }

                // Fallback to the default content type if type could not be determined
                if (contentType == null) {
                    contentType = "application/octet-stream";
                }

                return ResponseEntity.ok()
                    .contentType(MediaType.parseMediaType(contentType))
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                    .body(resource);


            } catch (Exception e) {
                throw new BusinessResourceException("technical Error", "technical Error", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        @GetMapping("/saveTokenToSign/{email}")
        public AuthentitficationToSign saveTokenToSign(@PathVariable String email) {
            try {
                Optional<AuthentitficationToSign> authentificationToSign1 = iAuthentificationToSign.findByEmail(email);
                if (authentificationToSign1.isPresent()) {
                    String token = RandomStringUtils.random(30, true, true);
                    authentificationToSign1.get().setToken(token);
                    authentificationToSign1.get().setDateCreation(new Date());
                    authentificationToSign1.get().setValide(true);
                    iAuthentificationToSign.save(authentificationToSign1.get());
                    return authentificationToSign1.get();
                } else {
                    String token = RandomStringUtils.random(30, true, true);
                    AuthentitficationToSign authentificationToSign = new AuthentitficationToSign();
                    authentificationToSign.setEmail(email);
                    authentificationToSign.setToken(token);
                    authentificationToSign.setDateCreation(new Date());
                    authentificationToSign.setValide(true);

                    authentificationToSign = iAuthentificationToSign.save(authentificationToSign);
                    return authentificationToSign;
                }
            } catch (Exception e) {
                logger.info("exeption -> addDocument" + e.getMessage());
                throw new BusinessResourceException("technical Error", "technical Error", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }


        @PostMapping("/getSubscriberInfo/{email}/{token}")
        public ResponseEntity<HashMap<String, SubscriberInfoOutputDTO>> getSubscriberInfo(@PathVariable String email, @PathVariable String token) {
            try {
                Optional<AuthentitficationToSign> authentificationToSign = iAuthentificationToSign.findByEmail(email);
                System.out.println(authentificationToSign);
                System.out.println(authentificationToSign.get().getToken());
                System.out.println(authentificationToSign.get().isValide());
                int addMinuteTime = 5;
                Date date = DateUtils.addMinutes(authentificationToSign.get().getDateCreation(), addMinuteTime);
                System.out.println(date);
                System.out.println(date.after(new Date()));
                if (date.after(new Date())
                    && authentificationToSign.get().getToken().equals(token)
                    && authentificationToSign.get().isValide() == true) {
                    HashMap<String, SubscriberInfoOutputDTO> hashMap = new HashMap<>();
                    PersonalInfo infosPersonnelles = iInfosPers.findByEmail(email);
                    System.out.println(infosPersonnelles);
                    if (infosPersonnelles != null) {
                        SubscriberInfoOutputDTO subscriberInfoOutputDTO = new SubscriberInfoOutputDTO();
                        subscriberInfoOutputDTO.setEmail(email);
                        System.out.println(infosPersonnelles.getBirthday().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));

                        //  SimpleDateFormat sm = new SimpleDateFormat("dd/MM/yyyy");
                        subscriberInfoOutputDTO.setBirthday(infosPersonnelles.getBirthday().format(DateTimeFormatter.ofPattern("dd/MM/yyyy")));
                        subscriberInfoOutputDTO.setFirst_name(infosPersonnelles.getFirstName());
                        subscriberInfoOutputDTO.setLast_name(infosPersonnelles.getLastName());
                        RequiredDocDTO requiredDocDTO = requiredDocService.findByRequestId(infosPersonnelles.getRequest().getId());
                        subscriberInfoOutputDTO.setCin(requiredDocDTO.getNumCIN());
                        // List<AttachementEntity> attachementList = new ArrayList<>();
                        if (infosPersonnelles != null) {
                            //attachementList = iAttachement.findByIdCreditCIN(infosPersonnelles.getIdCredit().longValue());

                            Resource resource = this.storageService.loadFile("Bulletin de souscription PRIVILEGES BLEDI.pdf");
                            byte[] bytes = ByteStreams.toByteArray(resource.getInputStream());
                            byte[] encoded = java.util.Base64.getEncoder().encode(bytes);
                            String byteString = new String(encoded, StandardCharsets.UTF_8);
                            subscriberInfoOutputDTO.setCin_recto(byteString);


                            Resource resource1 = this.storageService.loadFile("Bulletin de souscription PRIVILEGES BLEDI.pdf");
                            byte[] bytes1 = ByteStreams.toByteArray(resource1.getInputStream());
                            byte[] encoded1 = java.util.Base64.getEncoder().encode(bytes1);
                            String byteString1 = new String(encoded1, StandardCharsets.UTF_8);
                            subscriberInfoOutputDTO.setCin_verso(byteString1);

                        }
                        subscriberInfoOutputDTO.setPhone(infosPersonnelles.getPhone());
                        hashMap.put("data", subscriberInfoOutputDTO);
                        return ResponseEntity.ok(hashMap);
                    }
                } else {
                    authentificationToSign.get().setValide(false);
                    iAuthentificationToSign.save(authentificationToSign.get());

                }
                return ResponseEntity.status(HttpStatus.TOO_MANY_REQUESTS).build();
            } catch (Exception e) {
                logger.info("exeption -> addDocument" + e.getMessage());
                throw new BusinessResourceException("technical Error", "technical Error", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }


        @GetMapping("/getSubsciberSatus/{idRequest}")
        public WebServiceSign.SubsciberSatusOut getSubsciberSatus(@PathVariable Long idRequest) {
            try {
                PersonalInfoDTO infosPersonnelles = personalInfoService.findByRequestId(idRequest);
                String sessionId = WebServiceSign.wsAuthetificationSign();
                WebServiceSign.SubsciberSatusOut subsciberSatusOut = WebServiceSign.wsGetSubsciberSatus(sessionId, infosPersonnelles.getEmail());
                return subsciberSatusOut;
            } catch (Exception e) {
                logger.info("exeption -> addDocument" + e.getMessage());
                throw new BusinessResourceException("technical Error", "technical Error", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }

        @GetMapping("/doc/{iddoc}")
        public void doc(@PathVariable String iddoc) {
            DocumentDTO deblocageEntity = documentService.findDocumentByDossierId(iddoc);

        }


    @ApiOperation(value = "addDocument", hidden = true)
    @PostMapping("/addDocumentCompte/{idRequest}")
    public ResponseEntity<HashMap<String, String>> addDocu(@PathVariable("idRequest") Long idRequest) {
        try {
            HashMap<String, String> hashMap = new HashMap<>();
            DocumentDTO documentDTO = documentService.findDocumentByRequestId(idRequest);
            RequestDTO requestDTO = requestService.findByRequestId(idRequest);
            String fileName = null;
            // get the id credit
            // get the email from demande credit

            //   BigDecimal idCredit = docsDeblocage.getIdCredit();
            PersonalInfoDTO personalInfoDTO = personalInfoService.findByRequestId(idRequest);
            String emailAdress = personalInfoDTO.getEmail();
            System.out.println(requestDTO.getOfferId());
            //String emailAdress = "mohamedamine.bader@teamwillgroup.com";

            //System.out.println("add document  of get docsDeblocage:" + docsDeblocage);
            if (emailAdress != null) {

                    fileName = "Convention de compte.pdf";

                // String fileName = "auter2.pdf";
                // load Resource
                Resource resource = this.storageService.loadFileDeblocage(fileName);
                //Resource resource = "URL [file:/C:/uploads/47000-9-demande_credit_to_signed.pdf]";

                InputStream inputStream = resource.getInputStream();
                byte[] bytes = ByteStreams.toByteArray(inputStream);
                //byte[] bytes = FileUtils.readFileToByteArray(resource.getFile());
                byte[] encoded = java.util.Base64.getEncoder().encode(bytes);
                String byteString = new String(encoded, StandardCharsets.UTF_8);


                //byte[] fileContent = FileUtils.readFileToByteArray(new File("file:/C:/deblocage/demande_credit_to_signed"));
                //String byteString = Base64.getEncoder().encodeToString(fileContent);
                //String byteString = new String (encodedString, StandardCharsets.UTF_8);

                String sessionId = WebServiceSign.wsAuthetificationSign();
                String dossierId = WebServiceSign.wsAddDocu(sessionId, byteString, fileName, emailAdress);
                System.out.println("dossier Id of add document:" + dossierId);
                if (dossierId != null) {
                    documentDTO.setIdDossierSigned(dossierId);
                    documentDTO.setNomFichier(fileName);
                    documentService.save(documentDTO);
                }
                hashMap.put("dossierId", dossierId);
                hashMap.put("emailAdress", emailAdress);
                return ResponseEntity.ok(hashMap);
            } else
                hashMap.put("error", "document does not exist");
            return ResponseEntity.ok(hashMap);

        } catch (Exception e) {
            logger.info("exeption -> addDocument" + e.getMessage());
            throw new BusinessResourceException("technical Error", "technical Error", HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}

