package com.attijeri.tfs.web.rest;
import com.attijeri.tfs.service.RequiredDocResidencyService;
import com.attijeri.tfs.web.rest.errors.BadRequestAlertException;
import com.attijeri.tfs.service.dto.RequiredDocResidencyDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.attijeri.tfs.domain.RequiredDocResidency}.
 */
@RestController
@RequestMapping("/api")
public class RequiredDocResidencyResource {

    private final Logger log = LoggerFactory.getLogger(RequiredDocResidencyResource.class);

    private static final String ENTITY_NAME = "requiredDocResidency";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RequiredDocResidencyService requiredDocResidencyService;

    public RequiredDocResidencyResource(RequiredDocResidencyService requiredDocResidencyService) {
        this.requiredDocResidencyService = requiredDocResidencyService;
    }

    /**
     * {@code POST  /required-doc-residencies} : Create a new requiredDocResidency.
     *
     * @param requiredDocResidencyDTO the requiredDocResidencyDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new requiredDocResidencyDTO, or with status {@code 400 (Bad Request)} if the requiredDocResidency has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/required-doc-residencies")
    public ResponseEntity<RequiredDocResidencyDTO> createRequiredDocResidency(@RequestBody RequiredDocResidencyDTO requiredDocResidencyDTO) throws URISyntaxException {
        log.debug("REST request to save RequiredDocResidency : {}", requiredDocResidencyDTO);
        if (requiredDocResidencyDTO.getId() != null) {
            throw new BadRequestAlertException("A new requiredDocResidency cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RequiredDocResidencyDTO result = requiredDocResidencyService.save(requiredDocResidencyDTO);
        return ResponseEntity.created(new URI("/api/required-doc-residencies/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /required-doc-residencies} : Updates an existing requiredDocResidency.
     *
     * @param requiredDocResidencyDTO the requiredDocResidencyDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated requiredDocResidencyDTO,
     * or with status {@code 400 (Bad Request)} if the requiredDocResidencyDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the requiredDocResidencyDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/required-doc-residencies")
    public ResponseEntity<RequiredDocResidencyDTO> updateRequiredDocResidency(@RequestBody RequiredDocResidencyDTO requiredDocResidencyDTO) throws URISyntaxException {
        log.debug("REST request to update RequiredDocResidency : {}", requiredDocResidencyDTO);
        if (requiredDocResidencyDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RequiredDocResidencyDTO result = requiredDocResidencyService.save(requiredDocResidencyDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, requiredDocResidencyDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /required-doc-residencies} : get all the requiredDocResidencies.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of requiredDocResidencies in body.
     */
    @GetMapping("/required-doc-residencies")
    public List<RequiredDocResidencyDTO> getAllRequiredDocResidencies() {
        log.debug("REST request to get all RequiredDocResidencies");
        return requiredDocResidencyService.findAll();
    }

    /**
     * {@code GET  /required-doc-residencies/:id} : get the "id" requiredDocResidency.
     *
     * @param id the id of the requiredDocResidencyDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the requiredDocResidencyDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/required-doc-residencies/{id}")
    public ResponseEntity<RequiredDocResidencyDTO> getRequiredDocResidency(@PathVariable Long id) {
        log.debug("REST request to get RequiredDocResidency : {}", id);
        Optional<RequiredDocResidencyDTO> requiredDocResidencyDTO = requiredDocResidencyService.findOne(id);
        return ResponseUtil.wrapOrNotFound(requiredDocResidencyDTO);
    }
    @GetMapping("/required-doc-residencies-by-requestId/{id}")
    public ResponseEntity<RequiredDocResidencyDTO> getRequiredDocResidencyByREquestId(@PathVariable Long id) {
        log.debug("REST request to get RequiredDocResidency : {}", id);
        Optional<RequiredDocResidencyDTO> requiredDocResidencyDTO = requiredDocResidencyService.getRequiredResidencyByRequestId(id);
        return ResponseUtil.wrapOrNotFound(requiredDocResidencyDTO);
    }




    /**
     * {@code DELETE  /required-doc-residencies/:id} : delete the "id" requiredDocResidency.
     *
     * @param id the id of the requiredDocResidencyDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/required-doc-residencies/{id}")
    public ResponseEntity<Void> deleteRequiredDocResidency(@PathVariable Long id) {
        log.debug("REST request to delete RequiredDocResidency : {}", id);
        requiredDocResidencyService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

}
