package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.service.GovernorateService;
import com.attijeri.tfs.web.rest.errors.BadRequestAlertException;
import com.attijeri.tfs.service.dto.GovernorateDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.attijeri.tfs.domain.Governorate}.
 */
@RestController
@RequestMapping("/api")
public class GovernorateResource {

    private final Logger log = LoggerFactory.getLogger(GovernorateResource.class);

    private static final String ENTITY_NAME = "governorate";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final GovernorateService governorateService;

    public GovernorateResource(GovernorateService governorateService) {
        this.governorateService = governorateService;
    }

    /**
     * {@code POST  /governorates} : Create a new governorate.
     *
     * @param governorateDTO the governorateDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new governorateDTO, or with status {@code 400 (Bad Request)} if the governorate has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/governorates")
    public ResponseEntity<GovernorateDTO> createGovernorate(@RequestBody GovernorateDTO governorateDTO) throws URISyntaxException {
        log.debug("REST request to save Governorate : {}", governorateDTO);
        if (governorateDTO.getId() != null) {
            throw new BadRequestAlertException("A new governorate cannot already have an ID", ENTITY_NAME, "idexists");
        }
        GovernorateDTO result = governorateService.save(governorateDTO);
        return ResponseEntity.created(new URI("/api/governorates/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /governorates} : Updates an existing governorate.
     *
     * @param governorateDTO the governorateDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated governorateDTO,
     * or with status {@code 400 (Bad Request)} if the governorateDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the governorateDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/governorates")
    public ResponseEntity<GovernorateDTO> updateGovernorate(@RequestBody GovernorateDTO governorateDTO) throws URISyntaxException {
        log.debug("REST request to update Governorate : {}", governorateDTO);
        if (governorateDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        GovernorateDTO result = governorateService.save(governorateDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, governorateDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /governorates} : get all the governorates.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of governorates in body.
     */
    @GetMapping("/governorates")
    public List<GovernorateDTO> getAllGovernorates(@RequestParam(required = false, defaultValue = "true") boolean eagerload) {
        log.debug("REST request to get all Governorates");
        return governorateService.findAll();
    }

    /**
     * {@code GET  /governorates/:id} : get the "id" governorate.
     *
     * @param id the id of the governorateDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the governorateDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/governorates/{id}")
    public ResponseEntity<GovernorateDTO> getGovernorate(@PathVariable Long id) {
        log.debug("REST request to get Governorate : {}", id);
        Optional<GovernorateDTO> governorateDTO = governorateService.findOne(id);
        return ResponseUtil.wrapOrNotFound(governorateDTO);
    }

    /**
     * {@code DELETE  /governorates/:id} : delete the "id" governorate.
     *
     * @param id the id of the governorateDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/governorates/{id}")
    public ResponseEntity<Void> deleteGovernorate(@PathVariable Long id) {
        log.debug("REST request to delete Governorate : {}", id);
        governorateService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
