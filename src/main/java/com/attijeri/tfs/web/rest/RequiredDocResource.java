package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.service.RequiredDocService;
import com.attijeri.tfs.service.dto.AllDataOfDocDto;
import com.attijeri.tfs.service.dto.FileDataDao;
import com.attijeri.tfs.utlis.Dowanload;
import com.attijeri.tfs.web.rest.errors.BadRequestAlertException;
import com.attijeri.tfs.service.dto.RequiredDocDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.attijeri.tfs.domain.RequiredDoc}.
 */
@RestController
@RequestMapping("/api")
public class RequiredDocResource {

    private final Logger log = LoggerFactory.getLogger(RequiredDocResource.class);

    private static final String ENTITY_NAME = "requiredDoc";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    @Autowired
    private RequiredDocService requiredDocService;
    @Autowired
    Dowanload dowanload;



    public RequiredDocResource(RequiredDocService requiredDocService) {
        this.requiredDocService = requiredDocService;
    }


    /*  @PostMapping("/required-docs")
      public ResponseEntity<RequiredDocDTO> createRequiredDoc(@RequestBody RequiredDocDTO requiredDocDTO) throws URISyntaxException {
          log.debug("REST request to save RequiredDoc : {}", requiredDocDTO);
          if (requiredDocDTO.getId() != null) {
              throw new BadRequestAlertException("A new requiredDoc cannot already have an ID", ENTITY_NAME, "idexists");
          }
          RequiredDocDTO result = requiredDocService.save(requiredDocDTO);
          return ResponseEntity.created(new URI("/api/required-docs/" + result.getId()))
              .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
              .body(result);
      }*/
    @PostMapping(value ="/required-docs/{requestId}",produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<RequiredDocDTO> createRequiredDoc( @PathVariable Long requestId,@RequestBody AllDataOfDocDto allDataOfDocDto) throws URISyntaxException {
        requiredDocService.saveAllDataOfDoc(allDataOfDocDto,requestId);
        return null;
    }

    /**
     * {@code PUT  /required-docs} : Updates an existing requiredDoc.
     *
     * @param requiredDocDTO the requiredDocDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated requiredDocDTO,
     * or with status {@code 400 (Bad Request)} if the requiredDocDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the requiredDocDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/required-docs")
    public ResponseEntity<RequiredDocDTO> updateRequiredDoc(@RequestBody RequiredDocDTO requiredDocDTO) throws URISyntaxException {
        log.debug("REST request to update RequiredDoc : {}", requiredDocDTO);
        if (requiredDocDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RequiredDocDTO result = requiredDocService.save(requiredDocDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, requiredDocDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /required-docs} : get all the requiredDocs.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of requiredDocs in body.
     */
    @GetMapping("/required-docs")
    public List<RequiredDocDTO> getAllRequiredDocs() {
        log.debug("REST request to get all RequiredDocs");
        return requiredDocService.findAll();
    }

    /**
     * {@code GET  /required-docs/:id} : get the "id" requiredDoc.
     *
     * @param id the id of the requiredDocDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the requiredDocDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/required-docs/{id}")
    public ResponseEntity<RequiredDocDTO> getRequiredDoc(@PathVariable Long id) {
        log.debug("REST request to get RequiredDoc : {}", id);
        Optional<RequiredDocDTO> requiredDocDTO = requiredDocService.findOne(id);
        return ResponseUtil.wrapOrNotFound(requiredDocDTO);
    }
    @GetMapping("/get-all-data-of-doc/{id}")
    public AllDataOfDocDto  getAllDataOfDoc(@PathVariable Long id) {
        AllDataOfDocDto allDataOfDocDto= new AllDataOfDocDto();
        allDataOfDocDto=requiredDocService.getDataJustif(id);
        return allDataOfDocDto;
    }
    @GetMapping("/required-docs-by-request/{id}")
    public ResponseEntity<RequiredDocDTO> getRequiredDocByRequest(@PathVariable Long id) {
        log.debug("REST request to get RequiredDoc : {}", id);
        Optional<RequiredDocDTO> requiredDocDTO = requiredDocService.getRequiredDocByRequestId(id);
        return ResponseUtil.wrapOrNotFound(requiredDocDTO);
    }


    /**
     * {@code DELETE  /required-docs/:id} : delete the "id" requiredDoc.
     *
     * @param id the id of the requiredDocDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/required-docs/{id}")
    public ResponseEntity<Void> deleteRequiredDoc(@PathVariable Long id) {
        log.debug("REST request to delete RequiredDoc : {}", id);
        requiredDocService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
    @GetMapping("/deleteFile/{id}/{typeDoc}")
    public ResponseEntity<Void> deleteFile(@PathVariable Long id,@PathVariable String typeDoc) {
        log.debug("REST request to delete RequiredDoc : {}", id);
        requiredDocService.deleteImportantDoc(id,typeDoc);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
    @RequestMapping(value = "/updateFile",consumes = "multipart/form-data")
    public FileDataDao upload(@RequestParam(value = "file", required = false) MultipartFile file,
                              @RequestParam (value = "requestId", required = false) Long requestId,
                              @RequestParam (value = "typeDoc", required = false) String typeDoc)


        throws IOException {
        return requiredDocService.uploadFile(file,  requestId,typeDoc);
    }
    @RequestMapping(value = "/uploadFileRevenu",consumes = "multipart/form-data")
    public FileDataDao uploadFileRevenu(@RequestParam(value = "file", required = false) MultipartFile file,
                                        @RequestParam (value = "requestId", required = false) Long requestId

    )

        throws IOException {
        return requiredDocService.uploadFileRevenu(file,requestId);
    }
    @RequestMapping(value = "/uploadFileResidency",consumes = "multipart/form-data")
    public FileDataDao uploadFileResidency(@RequestParam(value = "file", required = false) MultipartFile file,
                                           @RequestParam (value = "requestId", required = false) Long requestId

    )

        throws IOException {
        return requiredDocService.uploadFileResidency(file,requestId);
    }




    @GetMapping("/required-docs/downloadFile/{id}/{typeDoc}")
    public @ResponseBody byte[] downloadfile(@PathVariable("id") Long requestId,
                                             @PathVariable("typeDoc") String typeDoc, HttpServletResponse response) throws IOException {

        byte[] blob = new byte[1024];
        return blob = requiredDocService.downloadfile(requestId, typeDoc, response);


    }
    @GetMapping("/required-docs/downloadfileRevenu/{id}")
    public @ResponseBody byte[] downloadfileRevenu(@PathVariable("id") Long requestId,
                                                   HttpServletResponse response) throws IOException {

        byte[] blob = new byte[1024];
        return blob = requiredDocService.downloadfileRevenu(requestId, response);
    }
    @GetMapping("/required-docs/downloadfileResidency/{id}")
    public @ResponseBody byte[] downloadfileResidency(@PathVariable("id") Long requestId,
                                                   HttpServletResponse response) throws IOException {

        byte[] blob = new byte[1024];
        return blob = requiredDocService.downloadfileResidency(requestId, response);
    }
    @GetMapping("/required-docs/downloadfilefatca")
    public @ResponseBody byte[] downloadfilefatca(HttpServletResponse response) throws IOException {

        byte[] blob = new byte[1024];
        return blob = requiredDocService.downloadfilefatca( response);
    }
}
