package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.service.DetailOfferService;
import com.attijeri.tfs.web.rest.errors.BadRequestAlertException;
import com.attijeri.tfs.service.dto.DetailOfferDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.attijeri.tfs.domain.DetailOffer}.
 */
@RestController
@RequestMapping("/api")
public class DetailOfferResource {

    private final Logger log = LoggerFactory.getLogger(DetailOfferResource.class);

    private static final String ENTITY_NAME = "detailOffer";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final DetailOfferService detailOfferService;

    public DetailOfferResource(DetailOfferService detailOfferService) {
        this.detailOfferService = detailOfferService;
    }

    /**
     * {@code POST  /detail-offers} : Create a new detailOffer.
     *
     * @param detailOfferDTO the detailOfferDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new detailOfferDTO, or with status {@code 400 (Bad Request)} if the detailOffer has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/detail-offers")
    public ResponseEntity<DetailOfferDTO> createDetailOffer(@RequestBody DetailOfferDTO detailOfferDTO) throws URISyntaxException {
        log.debug("REST request to save DetailOffer : {}", detailOfferDTO);
        if (detailOfferDTO.getId() != null) {
            throw new BadRequestAlertException("A new detailOffer cannot already have an ID", ENTITY_NAME, "idexists");
        }
        DetailOfferDTO result = detailOfferService.save(detailOfferDTO);
        return ResponseEntity.created(new URI("/api/detail-offers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /detail-offers} : Updates an existing detailOffer.
     *
     * @param detailOfferDTO the detailOfferDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated detailOfferDTO,
     * or with status {@code 400 (Bad Request)} if the detailOfferDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the detailOfferDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/detail-offers")
    public ResponseEntity<DetailOfferDTO> updateDetailOffer(@RequestBody DetailOfferDTO detailOfferDTO) throws URISyntaxException {
        log.debug("REST request to update DetailOffer : {}", detailOfferDTO);
        if (detailOfferDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        DetailOfferDTO result = detailOfferService.save(detailOfferDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, detailOfferDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /detail-offers} : get all the detailOffers.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of detailOffers in body.
     */
    @GetMapping("/detail-offers")
    public List<DetailOfferDTO> getAllDetailOffers() {
        log.debug("REST request to get all DetailOffers");
        return detailOfferService.findAll();
    }

    /**
     * {@code GET  /detail-offers/:id} : get the "id" detailOffer.
     *
     * @param id the id of the detailOfferDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the detailOfferDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/detail-offers/{id}")
    public ResponseEntity<DetailOfferDTO> getDetailOffer(@PathVariable Long id) {
        log.debug("REST request to get DetailOffer : {}", id);
        Optional<DetailOfferDTO> detailOfferDTO = detailOfferService.findOne(id);
        return ResponseUtil.wrapOrNotFound(detailOfferDTO);
    }

    /**
     * {@code DELETE  /detail-offers/:id} : delete the "id" detailOffer.
     *
     * @param id the id of the detailOfferDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/detail-offers/{id}")
    public ResponseEntity<Void> deleteDetailOffer(@PathVariable Long id) {
        log.debug("REST request to delete DetailOffer : {}", id);
        detailOfferService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
