package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.service.AuthentitficationToSignService;
import com.attijeri.tfs.web.rest.errors.BadRequestAlertException;
import com.attijeri.tfs.service.dto.AuthentitficationToSignDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.attijeri.tfs.domain.AuthentitficationToSign}.
 */
@RestController
@RequestMapping("/api")
public class AuthentitficationToSignResource {

    private final Logger log = LoggerFactory.getLogger(AuthentitficationToSignResource.class);

    private static final String ENTITY_NAME = "authentitficationToSign";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AuthentitficationToSignService authentitficationToSignService;

    public AuthentitficationToSignResource(AuthentitficationToSignService authentitficationToSignService) {
        this.authentitficationToSignService = authentitficationToSignService;
    }

    /**
     * {@code POST  /authentitfication-to-signs} : Create a new authentitficationToSign.
     *
     * @param authentitficationToSignDTO the authentitficationToSignDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new authentitficationToSignDTO, or with status {@code 400 (Bad Request)} if the authentitficationToSign has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/authentitfication-to-signs")
    public ResponseEntity<AuthentitficationToSignDTO> createAuthentitficationToSign(@RequestBody AuthentitficationToSignDTO authentitficationToSignDTO) throws URISyntaxException {
        log.debug("REST request to save AuthentitficationToSign : {}", authentitficationToSignDTO);
        if (authentitficationToSignDTO.getId() != null) {
            throw new BadRequestAlertException("A new authentitficationToSign cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AuthentitficationToSignDTO result = authentitficationToSignService.save(authentitficationToSignDTO);
        return ResponseEntity.created(new URI("/api/authentitfication-to-signs/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /authentitfication-to-signs} : Updates an existing authentitficationToSign.
     *
     * @param authentitficationToSignDTO the authentitficationToSignDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated authentitficationToSignDTO,
     * or with status {@code 400 (Bad Request)} if the authentitficationToSignDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the authentitficationToSignDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/authentitfication-to-signs")
    public ResponseEntity<AuthentitficationToSignDTO> updateAuthentitficationToSign(@RequestBody AuthentitficationToSignDTO authentitficationToSignDTO) throws URISyntaxException {
        log.debug("REST request to update AuthentitficationToSign : {}", authentitficationToSignDTO);
        if (authentitficationToSignDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        AuthentitficationToSignDTO result = authentitficationToSignService.save(authentitficationToSignDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, authentitficationToSignDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /authentitfication-to-signs} : get all the authentitficationToSigns.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of authentitficationToSigns in body.
     */
    @GetMapping("/authentitfication-to-signs")
    public List<AuthentitficationToSignDTO> getAllAuthentitficationToSigns() {
        log.debug("REST request to get all AuthentitficationToSigns");
        return authentitficationToSignService.findAll();
    }

    /**
     * {@code GET  /authentitfication-to-signs/:id} : get the "id" authentitficationToSign.
     *
     * @param id the id of the authentitficationToSignDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the authentitficationToSignDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/authentitfication-to-signs/{id}")
    public ResponseEntity<AuthentitficationToSignDTO> getAuthentitficationToSign(@PathVariable Long id) {
        log.debug("REST request to get AuthentitficationToSign : {}", id);
        Optional<AuthentitficationToSignDTO> authentitficationToSignDTO = authentitficationToSignService.findOne(id);
        return ResponseUtil.wrapOrNotFound(authentitficationToSignDTO);
    }

    /**
     * {@code DELETE  /authentitfication-to-signs/:id} : delete the "id" authentitficationToSign.
     *
     * @param id the id of the authentitficationToSignDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/authentitfication-to-signs/{id}")
    public ResponseEntity<Void> deleteAuthentitficationToSign(@PathVariable Long id) {
        log.debug("REST request to delete AuthentitficationToSign : {}", id);
        authentitficationToSignService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
