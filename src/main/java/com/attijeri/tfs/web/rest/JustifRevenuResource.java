package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.service.JustifRevenuService;
import com.attijeri.tfs.web.rest.errors.BadRequestAlertException;
import com.attijeri.tfs.service.dto.JustifRevenuDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.attijeri.tfs.domain.JustifRevenu}.
 */
@RestController
@RequestMapping("/api")
public class JustifRevenuResource {

    private final Logger log = LoggerFactory.getLogger(JustifRevenuResource.class);

    private static final String ENTITY_NAME = "justifRevenu";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final JustifRevenuService justifRevenuService;

    public JustifRevenuResource(JustifRevenuService justifRevenuService) {
        this.justifRevenuService = justifRevenuService;
    }

    /**
     * {@code POST  /justif-revenus} : Create a new justifRevenu.
     *
     * @param justifRevenuDTO the justifRevenuDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new justifRevenuDTO, or with status {@code 400 (Bad Request)} if the justifRevenu has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/justif-revenus")
    public ResponseEntity<JustifRevenuDTO> createJustifRevenu(@RequestBody JustifRevenuDTO justifRevenuDTO) throws URISyntaxException {
        log.debug("REST request to save JustifRevenu : {}", justifRevenuDTO);
        if (justifRevenuDTO.getId() != null) {
            throw new BadRequestAlertException("A new justifRevenu cannot already have an ID", ENTITY_NAME, "idexists");
        }
        JustifRevenuDTO result = justifRevenuService.save(justifRevenuDTO);
        return ResponseEntity.created(new URI("/api/justif-revenus/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /justif-revenus} : Updates an existing justifRevenu.
     *
     * @param justifRevenuDTO the justifRevenuDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated justifRevenuDTO,
     * or with status {@code 400 (Bad Request)} if the justifRevenuDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the justifRevenuDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/justif-revenus")
    public ResponseEntity<JustifRevenuDTO> updateJustifRevenu(@RequestBody JustifRevenuDTO justifRevenuDTO) throws URISyntaxException {
        log.debug("REST request to update JustifRevenu : {}", justifRevenuDTO);
        if (justifRevenuDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        JustifRevenuDTO result = justifRevenuService.save(justifRevenuDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, justifRevenuDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /justif-revenus} : get all the justifRevenus.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of justifRevenus in body.
     */
    @GetMapping("/justif-revenus")
    public List<JustifRevenuDTO> getAllJustifRevenus() {
        log.debug("REST request to get all JustifRevenus");
        return justifRevenuService.findAll();
    }

    /**
     * {@code GET  /justif-revenus/:id} : get the "id" justifRevenu.
     *
     * @param id the id of the justifRevenuDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the justifRevenuDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/justif-revenus/{id}")
    public ResponseEntity<JustifRevenuDTO> getJustifRevenu(@PathVariable Long id) {
        log.debug("REST request to get JustifRevenu : {}", id);
        Optional<JustifRevenuDTO> justifRevenuDTO = justifRevenuService.findOne(id);
        return ResponseUtil.wrapOrNotFound(justifRevenuDTO);
    }

    /**
     * {@code DELETE  /justif-revenus/:id} : delete the "id" justifRevenu.
     *
     * @param id the id of the justifRevenuDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/justif-revenus/{id}")
    public ResponseEntity<Void> deleteJustifRevenu(@PathVariable Long id) {
        log.debug("REST request to delete JustifRevenu : {}", id);
        justifRevenuService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
