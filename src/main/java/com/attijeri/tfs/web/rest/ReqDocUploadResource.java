package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.domain.ReqDocUpload;
import com.attijeri.tfs.service.ReqDocUploadService;
import com.attijeri.tfs.service.dto.FileDataDao;
import com.attijeri.tfs.web.rest.errors.BadRequestAlertException;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.attijeri.tfs.domain.ReqDocUpload}.
 */
@RestController
@RequestMapping("/api")
public class ReqDocUploadResource {

    private final Logger log = LoggerFactory.getLogger(ReqDocUploadResource.class);

    private static final String ENTITY_NAME = "reqDocUpload";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ReqDocUploadService reqDocUploadService;

    public ReqDocUploadResource(ReqDocUploadService reqDocUploadService) {
        this.reqDocUploadService = reqDocUploadService;
    }

    /**
     * {@code POST  /req-doc-uploads} : Create a new reqDocUpload.
     *
     * @param reqDocUpload the reqDocUpload to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new reqDocUpload, or with status {@code 400 (Bad Request)} if the reqDocUpload has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/req-doc-uploads")
    public ResponseEntity<ReqDocUpload> createReqDocUpload(@RequestBody ReqDocUpload reqDocUpload) throws URISyntaxException {
        log.debug("REST request to save ReqDocUpload : {}", reqDocUpload);
        if (reqDocUpload.getId() != null) {
            throw new BadRequestAlertException("A new reqDocUpload cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ReqDocUpload result = reqDocUploadService.save(reqDocUpload);
        return ResponseEntity.created(new URI("/api/req-doc-uploads/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /req-doc-uploads} : Updates an existing reqDocUpload.
     *
     * @param reqDocUpload the reqDocUpload to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated reqDocUpload,
     * or with status {@code 400 (Bad Request)} if the reqDocUpload is not valid,
     * or with status {@code 500 (Internal Server Error)} if the reqDocUpload couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/req-doc-uploads")
    public ResponseEntity<ReqDocUpload> updateReqDocUpload(@RequestBody ReqDocUpload reqDocUpload) throws URISyntaxException {
        log.debug("REST request to update ReqDocUpload : {}", reqDocUpload);
        if (reqDocUpload.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ReqDocUpload result = reqDocUploadService.save(reqDocUpload);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, reqDocUpload.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /req-doc-uploads} : get all the reqDocUploads.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of reqDocUploads in body.
     */
    @GetMapping("/req-doc-uploads")
    public List<ReqDocUpload> getAllReqDocUploads() {
        log.debug("REST request to get all ReqDocUploads");
        return reqDocUploadService.findAll();
    }

    /**
     * {@code GET  /req-doc-uploads/:id} : get the "id" reqDocUpload.
     *
     * @param id the id of the reqDocUpload to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the reqDocUpload, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/req-doc-uploads/{id}")
    public ResponseEntity<ReqDocUpload> getReqDocUpload(@PathVariable Long id) {
        log.debug("REST request to get ReqDocUpload : {}", id);
        Optional<ReqDocUpload> reqDocUpload = reqDocUploadService.findOne(id);
        return ResponseUtil.wrapOrNotFound(reqDocUpload);
    }

    /**
     * {@code DELETE  /req-doc-uploads/:id} : delete the "id" reqDocUpload.
     *
     * @param id the id of the reqDocUpload to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/req-doc-uploads/{id}")
    public ResponseEntity<Void> deleteReqDocUpload(@PathVariable Long id) {
        log.debug("REST request to delete ReqDocUpload : {}", id);
        reqDocUploadService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }

    @RequestMapping(value = "/uploadReqFile",consumes = "multipart/form-data")
    public ReqDocUpload uploadReqFile(@RequestParam(value = "file", required = false) MultipartFile file,
                                      @RequestParam (value = "requestId", required = false) Long requestId,
                                      @RequestParam (value = "docType", required = false) String docType)
        throws Exception
    {
        return reqDocUploadService.uploadFile(file, requestId, docType);
    }
    @DeleteMapping(value = "/deleteUploadedFile/{id}")
    public boolean uploadReqFile(@PathVariable Long id)
        throws Exception
    {
        return reqDocUploadService.deleteFile(id);
    }


}
