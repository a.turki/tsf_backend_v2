package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.service.ResidencyDocumentService;
import com.attijeri.tfs.web.rest.errors.BadRequestAlertException;
import com.attijeri.tfs.service.dto.ResidencyDocumentDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.attijeri.tfs.domain.ResidencyDocument}.
 */
@RestController
@RequestMapping("/api")
public class ResidencyDocumentResource {

    private final Logger log = LoggerFactory.getLogger(ResidencyDocumentResource.class);

    private static final String ENTITY_NAME = "residencyDocument";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ResidencyDocumentService residencyDocumentService;

    public ResidencyDocumentResource(ResidencyDocumentService residencyDocumentService) {
        this.residencyDocumentService = residencyDocumentService;
    }

    /**
     * {@code POST  /residency-documents} : Create a new residencyDocument.
     *
     * @param residencyDocumentDTO the residencyDocumentDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new residencyDocumentDTO, or with status {@code 400 (Bad Request)} if the residencyDocument has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/residency-documents")
    public ResponseEntity<ResidencyDocumentDTO> createResidencyDocument(@RequestBody ResidencyDocumentDTO residencyDocumentDTO) throws URISyntaxException {
        log.debug("REST request to save ResidencyDocument : {}", residencyDocumentDTO);
        if (residencyDocumentDTO.getId() != null) {
            throw new BadRequestAlertException("A new residencyDocument cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ResidencyDocumentDTO result = residencyDocumentService.save(residencyDocumentDTO);
        return ResponseEntity.created(new URI("/api/residency-documents/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /residency-documents} : Updates an existing residencyDocument.
     *
     * @param residencyDocumentDTO the residencyDocumentDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated residencyDocumentDTO,
     * or with status {@code 400 (Bad Request)} if the residencyDocumentDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the residencyDocumentDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/residency-documents")
    public ResponseEntity<ResidencyDocumentDTO> updateResidencyDocument(@RequestBody ResidencyDocumentDTO residencyDocumentDTO) throws URISyntaxException {
        log.debug("REST request to update ResidencyDocument : {}", residencyDocumentDTO);
        if (residencyDocumentDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        ResidencyDocumentDTO result = residencyDocumentService.save(residencyDocumentDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, residencyDocumentDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /residency-documents} : get all the residencyDocuments.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of residencyDocuments in body.
     */
    @GetMapping("/residency-documents")
    public List<ResidencyDocumentDTO> getAllResidencyDocuments() {
        log.debug("REST request to get all ResidencyDocuments");
        return residencyDocumentService.findAll();
    }

    /**
     * {@code GET  /residency-documents/:id} : get the "id" residencyDocument.
     *
     * @param id the id of the residencyDocumentDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the residencyDocumentDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/residency-documents/{id}")
    public ResponseEntity<ResidencyDocumentDTO> getResidencyDocument(@PathVariable Long id) {
        log.debug("REST request to get ResidencyDocument : {}", id);
        Optional<ResidencyDocumentDTO> residencyDocumentDTO = residencyDocumentService.findOne(id);
        return ResponseUtil.wrapOrNotFound(residencyDocumentDTO);
    }

    /**
     * {@code DELETE  /residency-documents/:id} : delete the "id" residencyDocument.
     *
     * @param id the id of the residencyDocumentDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/residency-documents/{id}")
    public ResponseEntity<Void> deleteResidencyDocument(@PathVariable Long id) {
        log.debug("REST request to delete ResidencyDocument : {}", id);
        residencyDocumentService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
