package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.domain.RequiredDoc;
import com.attijeri.tfs.repository.RequiredDocRepository;
import com.attijeri.tfs.service.OtherResidencyFileService;
import com.attijeri.tfs.service.dto.OtherRevenuFileDTO;
import com.attijeri.tfs.web.rest.errors.BadRequestAlertException;
import com.attijeri.tfs.service.dto.OtherResidencyFileDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.attijeri.tfs.domain.OtherResidencyFile}.
 */
@RestController
@RequestMapping("/api")
public class OtherResidencyFileResource {

    private final Logger log = LoggerFactory.getLogger(OtherResidencyFileResource.class);

    private static final String ENTITY_NAME = "otherResidencyFile";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;
    @Autowired
    RequiredDocRepository requiredDocRepository;

    private final OtherResidencyFileService otherResidencyFileService;

    public OtherResidencyFileResource(OtherResidencyFileService otherResidencyFileService) {
        this.otherResidencyFileService = otherResidencyFileService;
    }

    /**
     * {@code POST  /other-residency-files} : Create a new otherResidencyFile.
     *
     * @param otherResidencyFileDTO the otherResidencyFileDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new otherResidencyFileDTO, or with status {@code 400 (Bad Request)} if the otherResidencyFile has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/other-residency-files")
    public ResponseEntity<OtherResidencyFileDTO> createOtherResidencyFile(@RequestBody OtherResidencyFileDTO otherResidencyFileDTO) throws URISyntaxException {
        log.debug("REST request to save OtherResidencyFile : {}", otherResidencyFileDTO);
        if (otherResidencyFileDTO.getId() != null) {
            throw new BadRequestAlertException("A new otherResidencyFile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OtherResidencyFileDTO result = otherResidencyFileService.save(otherResidencyFileDTO);
        return ResponseEntity.created(new URI("/api/other-residency-files/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /other-residency-files} : Updates an existing otherResidencyFile.
     *
     * @param otherResidencyFileDTO the otherResidencyFileDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated otherResidencyFileDTO,
     * or with status {@code 400 (Bad Request)} if the otherResidencyFileDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the otherResidencyFileDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/other-residency-files")
    public ResponseEntity<OtherResidencyFileDTO> updateOtherResidencyFile(@RequestBody OtherResidencyFileDTO otherResidencyFileDTO) throws URISyntaxException {
        log.debug("REST request to update OtherResidencyFile : {}", otherResidencyFileDTO);
        if (otherResidencyFileDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OtherResidencyFileDTO result = otherResidencyFileService.save(otherResidencyFileDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, otherResidencyFileDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /other-residency-files} : get all the otherResidencyFiles.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of otherResidencyFiles in body.
     */
    @GetMapping("/other-residency-files")
    public List<OtherResidencyFileDTO> getAllOtherResidencyFiles() {
        log.debug("REST request to get all OtherResidencyFiles");
        return otherResidencyFileService.findAll();
    }
    @GetMapping("/other-residency-files-by-requestId")
    public List<OtherResidencyFileDTO> getAllOtherResidencyFilesByRequestId(Long id) {
        log.debug("REST request to get all OtherResidencyFiles");
        return otherResidencyFileService.getAllResidencySameRequestId(id);
    }

    /**
     * {@code GET  /other-residency-files/:id} : get the "id" otherResidencyFile.
     *
     * @param id the id of the otherResidencyFileDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the otherResidencyFileDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/other-residency-files/{id}")
    public ResponseEntity<OtherResidencyFileDTO> getOtherResidencyFile(@PathVariable Long id) {
        log.debug("REST request to get OtherResidencyFile : {}", id);
        Optional<OtherResidencyFileDTO> otherResidencyFileDTO = otherResidencyFileService.findOne(id);
        return ResponseUtil.wrapOrNotFound(otherResidencyFileDTO);
    }

    /**
     * {@code DELETE  /other-residency-files/:id} : delete the "id" otherResidencyFile.
     *
     * @param id the id of the otherResidencyFileDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/other-residency-files/{id}")
    public ResponseEntity<Void> deleteOtherResidencyFile(@PathVariable Long id) {
        log.debug("REST request to delete OtherResidencyFile : {}", id);
        try {
            otherResidencyFileService.delete(id);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
    @GetMapping("/other-residency-files-by-requestId/{id}")
    public List<OtherResidencyFileDTO> getAllOtherRevenuFilesByRequest(@PathVariable Long id) {
        RequiredDoc requiredDoc =  requiredDocRepository.findByRequestId(id);
        long requiredDocId=requiredDoc.getId();
        log.debug("REST request to get all OtherRevenuFiles");
        return otherResidencyFileService.getAllResidencySameRequestId(requiredDocId);
    }

}
