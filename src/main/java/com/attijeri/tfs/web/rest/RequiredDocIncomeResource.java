package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.service.RequiredDocIncomeService;
import com.attijeri.tfs.web.rest.errors.BadRequestAlertException;
import com.attijeri.tfs.service.dto.RequiredDocIncomeDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.attijeri.tfs.domain.RequiredDocIncome}.
 */
@RestController
@RequestMapping("/api")
public class RequiredDocIncomeResource {

    private final Logger log = LoggerFactory.getLogger(RequiredDocIncomeResource.class);

    private static final String ENTITY_NAME = "requiredDocIncome";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RequiredDocIncomeService requiredDocIncomeService;

    public RequiredDocIncomeResource(RequiredDocIncomeService requiredDocIncomeService) {
        this.requiredDocIncomeService = requiredDocIncomeService;
    }

    /**
     * {@code POST  /required-doc-incomes} : Create a new requiredDocIncome.
     *
     * @param requiredDocIncomeDTO the requiredDocIncomeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new requiredDocIncomeDTO, or with status {@code 400 (Bad Request)} if the requiredDocIncome has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/required-doc-incomes")
    public ResponseEntity<RequiredDocIncomeDTO> createRequiredDocIncome(@RequestBody RequiredDocIncomeDTO requiredDocIncomeDTO) throws URISyntaxException {
        log.debug("REST request to save RequiredDocIncome : {}", requiredDocIncomeDTO);
        if (requiredDocIncomeDTO.getId() != null) {
            throw new BadRequestAlertException("A new requiredDocIncome cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RequiredDocIncomeDTO result = requiredDocIncomeService.save(requiredDocIncomeDTO);
        return ResponseEntity.created(new URI("/api/required-doc-incomes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /required-doc-incomes} : Updates an existing requiredDocIncome.
     *
     * @param requiredDocIncomeDTO the requiredDocIncomeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated requiredDocIncomeDTO,
     * or with status {@code 400 (Bad Request)} if the requiredDocIncomeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the requiredDocIncomeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/required-doc-incomes")
    public ResponseEntity<RequiredDocIncomeDTO> updateRequiredDocIncome(@RequestBody RequiredDocIncomeDTO requiredDocIncomeDTO) throws URISyntaxException {
        log.debug("REST request to update RequiredDocIncome : {}", requiredDocIncomeDTO);
        if (requiredDocIncomeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        RequiredDocIncomeDTO result = requiredDocIncomeService.save(requiredDocIncomeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, requiredDocIncomeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /required-doc-incomes} : get all the requiredDocIncomes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of requiredDocIncomes in body.
     */
    @GetMapping("/required-doc-incomes")
    public List<RequiredDocIncomeDTO> getAllRequiredDocIncomes() {
        log.debug("REST request to get all RequiredDocIncomes");
        return requiredDocIncomeService.findAll();
    }

    /**
     * {@code GET  /required-doc-incomes/:id} : get the "id" requiredDocIncome.
     *
     * @param id the id of the requiredDocIncomeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the requiredDocIncomeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/required-doc-incomes/{id}")
    public ResponseEntity<RequiredDocIncomeDTO> getRequiredDocIncome(@PathVariable Long id) {
        log.debug("REST request to get RequiredDocIncome : {}", id);
        Optional<RequiredDocIncomeDTO> requiredDocIncomeDTO = requiredDocIncomeService.findByRequestId(id);
        return ResponseUtil.wrapOrNotFound(requiredDocIncomeDTO);
    }

}
