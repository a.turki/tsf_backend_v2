package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.service.MonthlyNetIncomeService;
import com.attijeri.tfs.web.rest.errors.BadRequestAlertException;
import com.attijeri.tfs.service.dto.MonthlyNetIncomeDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;
import java.util.stream.StreamSupport;

/**
 * REST controller for managing {@link com.attijeri.tfs.domain.MonthlyNetIncome}.
 */
@RestController
@RequestMapping("/api")
public class MonthlyNetIncomeResource {

    private final Logger log = LoggerFactory.getLogger(MonthlyNetIncomeResource.class);

    private static final String ENTITY_NAME = "monthlyNetIncome";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MonthlyNetIncomeService monthlyNetIncomeService;

    public MonthlyNetIncomeResource(MonthlyNetIncomeService monthlyNetIncomeService) {
        this.monthlyNetIncomeService = monthlyNetIncomeService;
    }

    /**
     * {@code POST  /monthly-net-incomes} : Create a new monthlyNetIncome.
     *
     * @param monthlyNetIncomeDTO the monthlyNetIncomeDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new monthlyNetIncomeDTO, or with status {@code 400 (Bad Request)} if the monthlyNetIncome has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/monthly-net-incomes")
    public ResponseEntity<MonthlyNetIncomeDTO> createMonthlyNetIncome(@RequestBody MonthlyNetIncomeDTO monthlyNetIncomeDTO) throws URISyntaxException {
        log.debug("REST request to save MonthlyNetIncome : {}", monthlyNetIncomeDTO);
        if (monthlyNetIncomeDTO.getId() != null) {
            throw new BadRequestAlertException("A new monthlyNetIncome cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MonthlyNetIncomeDTO result = monthlyNetIncomeService.save(monthlyNetIncomeDTO);
        return ResponseEntity.created(new URI("/api/monthly-net-incomes/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /monthly-net-incomes} : Updates an existing monthlyNetIncome.
     *
     * @param monthlyNetIncomeDTO the monthlyNetIncomeDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated monthlyNetIncomeDTO,
     * or with status {@code 400 (Bad Request)} if the monthlyNetIncomeDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the monthlyNetIncomeDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/monthly-net-incomes")
    public ResponseEntity<MonthlyNetIncomeDTO> updateMonthlyNetIncome(@RequestBody MonthlyNetIncomeDTO monthlyNetIncomeDTO) throws URISyntaxException {
        log.debug("REST request to update MonthlyNetIncome : {}", monthlyNetIncomeDTO);
        if (monthlyNetIncomeDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        MonthlyNetIncomeDTO result = monthlyNetIncomeService.save(monthlyNetIncomeDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, monthlyNetIncomeDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /monthly-net-incomes} : get all the monthlyNetIncomes.
     *
     * @param filter the filter of the request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of monthlyNetIncomes in body.
     */
    @GetMapping("/monthly-net-incomes")
    public List<MonthlyNetIncomeDTO> getAllMonthlyNetIncomes(@RequestParam(required = false) String filter) {
        if ("financialinfo-is-null".equals(filter)) {
            log.debug("REST request to get all MonthlyNetIncomes where financialInfo is null");
            return monthlyNetIncomeService.findAllWhereFinancialInfoIsNull();
        }
        log.debug("REST request to get all MonthlyNetIncomes");
        return monthlyNetIncomeService.findAll();
    }

    /**
     * {@code GET  /monthly-net-incomes/:id} : get the "id" monthlyNetIncome.
     *
     * @param id the id of the monthlyNetIncomeDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the monthlyNetIncomeDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/monthly-net-incomes/{id}")
    public ResponseEntity<MonthlyNetIncomeDTO> getMonthlyNetIncome(@PathVariable Long id) {
        log.debug("REST request to get MonthlyNetIncome : {}", id);
        Optional<MonthlyNetIncomeDTO> monthlyNetIncomeDTO = monthlyNetIncomeService.findOne(id);
        return ResponseUtil.wrapOrNotFound(monthlyNetIncomeDTO);
    }

    /**
     * {@code DELETE  /monthly-net-incomes/:id} : delete the "id" monthlyNetIncome.
     *
     * @param id the id of the monthlyNetIncomeDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/monthly-net-incomes/{id}")
    public ResponseEntity<Void> deleteMonthlyNetIncome(@PathVariable Long id) {
        log.debug("REST request to delete MonthlyNetIncome : {}", id);
        monthlyNetIncomeService.delete(id);
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
