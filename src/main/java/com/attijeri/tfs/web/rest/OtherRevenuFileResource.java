package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.domain.RequiredDoc;
import com.attijeri.tfs.repository.RequiredDocRepository;
import com.attijeri.tfs.service.OtherRevenuFileService;
import com.attijeri.tfs.web.rest.errors.BadRequestAlertException;
import com.attijeri.tfs.service.dto.OtherRevenuFileDTO;

import io.github.jhipster.web.util.HeaderUtil;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing {@link com.attijeri.tfs.domain.OtherRevenuFile}.
 */
@RestController
@RequestMapping("/api")
public class OtherRevenuFileResource {

    private final Logger log = LoggerFactory.getLogger(OtherRevenuFileResource.class);

    private static final String ENTITY_NAME = "otherRevenuFile";
    @Autowired
    RequiredDocRepository requiredDocRepository;

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final OtherRevenuFileService otherRevenuFileService;

    public OtherRevenuFileResource(OtherRevenuFileService otherRevenuFileService) {
        this.otherRevenuFileService = otherRevenuFileService;
    }

    /**
     * {@code POST  /other-revenu-files} : Create a new otherRevenuFile.
     *
     * @param otherRevenuFileDTO the otherRevenuFileDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new otherRevenuFileDTO, or with status {@code 400 (Bad Request)} if the otherRevenuFile has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/other-revenu-files")
    public ResponseEntity<OtherRevenuFileDTO> createOtherRevenuFile(@RequestBody OtherRevenuFileDTO otherRevenuFileDTO) throws URISyntaxException {
        log.debug("REST request to save OtherRevenuFile : {}", otherRevenuFileDTO);
        if (otherRevenuFileDTO.getId() != null) {
            throw new BadRequestAlertException("A new otherRevenuFile cannot already have an ID", ENTITY_NAME, "idexists");
        }
        OtherRevenuFileDTO result = otherRevenuFileService.save(otherRevenuFileDTO);
        return ResponseEntity.created(new URI("/api/other-revenu-files/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /other-revenu-files} : Updates an existing otherRevenuFile.
     *
     * @param otherRevenuFileDTO the otherRevenuFileDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated otherRevenuFileDTO,
     * or with status {@code 400 (Bad Request)} if the otherRevenuFileDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the otherRevenuFileDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/other-revenu-files")
    public ResponseEntity<OtherRevenuFileDTO> updateOtherRevenuFile(@RequestBody OtherRevenuFileDTO otherRevenuFileDTO) throws URISyntaxException {
        log.debug("REST request to update OtherRevenuFile : {}", otherRevenuFileDTO);
        if (otherRevenuFileDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        OtherRevenuFileDTO result = otherRevenuFileService.save(otherRevenuFileDTO);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, otherRevenuFileDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code GET  /other-revenu-files} : get all the otherRevenuFiles.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of otherRevenuFiles in body.
     */
    @GetMapping("/other-revenu-files")
    public List<OtherRevenuFileDTO> getAllOtherRevenuFiles() {
        log.debug("REST request to get all OtherRevenuFiles");
        return otherRevenuFileService.findAll();
    }

    @GetMapping("/other-revenu-files-by-requestId/{id}")
    public List<OtherRevenuFileDTO> getAllOtherRevenuFilesByRequest(@PathVariable Long id) {
        try {
            RequiredDoc requiredDoc = requiredDocRepository.findByRequestId(id);
            if (requiredDoc != null) {
                long requiredDocId = requiredDoc.getId();
                log.debug("REST request to get all OtherRevenuFiles");

                return otherRevenuFileService.getAllRevenuSameRequestId(requiredDocId);
            } else {
                return null;
            }
        } catch (Exception e) {

        }
        return null;
    }

    /**
     * {@code GET  /other-revenu-files/:id} : get the "id" otherRevenuFile.
     *
     * @param id the id of the otherRevenuFileDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the otherRevenuFileDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/other-revenu-files/{id}")
    public ResponseEntity<OtherRevenuFileDTO> getOtherRevenuFile(@PathVariable Long id) {
        log.debug("REST request to get OtherRevenuFile : {}", id);
        Optional<OtherRevenuFileDTO> otherRevenuFileDTO = otherRevenuFileService.findOne(id);
        return ResponseUtil.wrapOrNotFound(otherRevenuFileDTO);
    }


    /**
     * {@code DELETE  /other-revenu-files/:id} : delete the "id" otherRevenuFile.
     *
     * @param id the id of the otherRevenuFileDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/other-revenu-files/{id}")
    public ResponseEntity<Void> deleteOtherRevenuFile(@PathVariable Long id) {
        log.debug("REST request to delete OtherRevenuFile : {}", id);
        try {
            otherRevenuFileService.delete(id);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.noContent().headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString())).build();
    }
}
