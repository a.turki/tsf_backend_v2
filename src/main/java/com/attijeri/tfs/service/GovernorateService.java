package com.attijeri.tfs.service;

import com.attijeri.tfs.service.dto.GovernorateDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.attijeri.tfs.domain.Governorate}.
 */
public interface GovernorateService {

    /**
     * Save a governorate.
     *
     * @param governorateDTO the entity to save.
     * @return the persisted entity.
     */
    GovernorateDTO save(GovernorateDTO governorateDTO);

    /**
     * Get all the governorates.
     *
     * @return the list of entities.
     */
    List<GovernorateDTO> findAll();


    /**
     * Get the "id" governorate.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<GovernorateDTO> findOne(Long id);

    /**
     * Delete the "id" governorate.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
