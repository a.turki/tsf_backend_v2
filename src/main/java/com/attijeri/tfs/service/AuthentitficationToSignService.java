package com.attijeri.tfs.service;

import com.attijeri.tfs.service.dto.AuthentitficationToSignDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.attijeri.tfs.domain.AuthentitficationToSign}.
 */
public interface AuthentitficationToSignService {

    /**
     * Save a authentitficationToSign.
     *
     * @param authentitficationToSignDTO the entity to save.
     * @return the persisted entity.
     */
    AuthentitficationToSignDTO save(AuthentitficationToSignDTO authentitficationToSignDTO);

    /**
     * Get all the authentitficationToSigns.
     *
     * @return the list of entities.
     */
    List<AuthentitficationToSignDTO> findAll();


    /**
     * Get the "id" authentitficationToSign.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AuthentitficationToSignDTO> findOne(Long id);

    /**
     * Delete the "id" authentitficationToSign.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
