package com.attijeri.tfs.service;

import com.attijeri.tfs.domain.Request;
import com.attijeri.tfs.service.dto.RequestDTO;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.attijeri.tfs.domain.Request}.
 */
public interface RequestService {

     Request getRequestById(Long refCode);

        boolean followRequestMail(Long refCode);
    RequestDTO verifyCode(Long idRequest, String verificationCode);
    /**
     * Save a request.
     *
     * @param requestDTO the entity to save.
     * @return the persisted entity.
     */
    RequestDTO save(RequestDTO requestDTO);

    /**
     * Get all the requests.
     *
     * @return the list of entities.
     */
    List<RequestDTO> findAll();
    /**
     * Get all the RequestDTO where PersonalInfo is {@code null}.
     *
     * @return the {@link List} of entities.
     */
    List<RequestDTO> findAllWherePersonalInfoIsNull();
    /**
     * Get all the RequestDTO where RequiredDoc is {@code null}.
     *
     * @return the {@link List} of entities.
     */
    List<RequestDTO> findAllWhereRequiredDocIsNull();
    /**
     * Get all the RequestDTO where Document is {@code null}.
     *
     * @return the {@link List} of entities.
     */
    List<RequestDTO> findAllWhereDocumentIsNull();

    /**
     * Get all the requests with eager load of many-to-many relationships.
     *
     * @return the list of entities.
     */
    Page<RequestDTO> findAllWithEagerRelationships(Pageable pageable);



    /**
     * Get the "id" request.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RequestDTO> findOne(Long id);

    /**
     * Delete the "id" request.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    RequestDTO findByRequestId(Long requestId);
}
