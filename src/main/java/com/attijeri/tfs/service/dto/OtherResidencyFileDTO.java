package com.attijeri.tfs.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.attijeri.tfs.domain.OtherResidencyFile} entity.
 */
public class OtherResidencyFileDTO implements Serializable {
    
    private Long id;

    private String path;


    private Long requiredDocId;

    private Long residencyDocumentId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getRequiredDocId() {
        return requiredDocId;
    }

    public void setRequiredDocId(Long requiredDocId) {
        this.requiredDocId = requiredDocId;
    }

    public Long getResidencyDocumentId() {
        return residencyDocumentId;
    }

    public void setResidencyDocumentId(Long residencyDocumentId) {
        this.residencyDocumentId = residencyDocumentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OtherResidencyFileDTO)) {
            return false;
        }

        return id != null && id.equals(((OtherResidencyFileDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OtherResidencyFileDTO{" +
            "id=" + getId() +
            ", path='" + getPath() + "'" +
            ", requiredDocId=" + getRequiredDocId() +
            ", residencyDocumentId=" + getResidencyDocumentId() +
            "}";
    }
}
