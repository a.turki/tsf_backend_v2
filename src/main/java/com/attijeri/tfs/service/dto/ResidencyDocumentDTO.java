package com.attijeri.tfs.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.attijeri.tfs.domain.ResidencyDocument} entity.
 */
public class ResidencyDocumentDTO implements Serializable {
    
    private Long id;

    private String labelFR;

    private String labelEN;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabelFR() {
        return labelFR;
    }

    public void setLabelFR(String labelFR) {
        this.labelFR = labelFR;
    }

    public String getLabelEN() {
        return labelEN;
    }

    public void setLabelEN(String labelEN) {
        this.labelEN = labelEN;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ResidencyDocumentDTO)) {
            return false;
        }

        return id != null && id.equals(((ResidencyDocumentDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ResidencyDocumentDTO{" +
            "id=" + getId() +
            ", labelFR='" + getLabelFR() + "'" +
            ", labelEN='" + getLabelEN() + "'" +
            "}";
    }
}
