package com.attijeri.tfs.service.dto;

import java.time.LocalDate;
import java.io.Serializable;
import com.attijeri.tfs.domain.enumeration.Civility;

/**
 * A DTO for the {@link com.attijeri.tfs.domain.PersonalInfo} entity.
 */
public class PersonalInfoDTO implements Serializable {

    private Long id;

    private Civility civility;

    private String firstName;

    private String lastName;

    private String email;

    private String nativeCountry;

    private LocalDate birthday;

    private Boolean clientABT;

    private String rib;

    private String nationality;

    private String secondNationality;

    private Integer nbrkids;

    private String maritalStatus;

    private String phone;

    private Boolean americanIndex;


    private Long requestId;

    private Long agencyId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Civility getCivility() {
        return civility;
    }

    public void setCivility(Civility civility) {
        this.civility = civility;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNativeCountry() {
        return nativeCountry;
    }

    public void setNativeCountry(String nativeCountry) {
        this.nativeCountry = nativeCountry;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Boolean isClientABT() {
        return clientABT;
    }

    public void setClientABT(Boolean clientABT) {
        this.clientABT = clientABT;
    }

    public String getRib() {
        return rib;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getSecondNationality() {
        return secondNationality;
    }

    public void setSecondNationality(String secondNationality) {
        this.secondNationality = secondNationality;
    }

    public Integer getNbrkids() {
        return nbrkids;
    }

    public void setNbrkids(Integer nbrkids) {
        this.nbrkids = nbrkids;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean isAmericanIndex() {
        return americanIndex;
    }

    public void setAmericanIndex(Boolean americanIndex) {
        this.americanIndex = americanIndex;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    public Long getAgencyId() {
        return agencyId;
    }

    public void setAgencyId(Long agencyId) {
        this.agencyId = agencyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PersonalInfoDTO)) {
            return false;
        }

        return id != null && id.equals(((PersonalInfoDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PersonalInfoDTO{" +
            "id=" + getId() +
            ", civility='" + getCivility() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", email='" + getEmail() + "'" +
            ", nativeCountry='" + getNativeCountry() + "'" +
            ", birthday='" + getBirthday() + "'" +
            ", clientABT='" + isClientABT() + "'" +
            ", rib='" + getRib() + "'" +
            ", nationality='" + getNationality() + "'" +
            ", secondNationality='" + getSecondNationality() + "'" +
            ", nbrkids=" + getNbrkids() +
            ", maritalStatus='" + getMaritalStatus() + "'" +
            ", phone='" + getPhone() + "'" +
            ", americanIndex='" + isAmericanIndex() + "'" +
            ", requestId=" + getRequestId() +
            ", agencyId=" + getAgencyId() +
            "}";
    }
}
