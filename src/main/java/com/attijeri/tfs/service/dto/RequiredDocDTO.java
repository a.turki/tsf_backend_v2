package com.attijeri.tfs.service.dto;

import java.time.LocalDate;
import java.io.Serializable;

/**
 * A DTO for the {@link com.attijeri.tfs.domain.RequiredDoc} entity.
 */
public class RequiredDocDTO implements Serializable {
    
    private Long id;

    private String label;

    private String type;

    private String numCIN;

    private LocalDate deliveryDateCin;

    private String rectoCin;

    private String versoCin;

    private String fatca;


    private Long requestId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumCIN() {
        return numCIN;
    }

    public void setNumCIN(String numCIN) {
        this.numCIN = numCIN;
    }

    public LocalDate getDeliveryDateCin() {
        return deliveryDateCin;
    }

    public void setDeliveryDateCin(LocalDate deliveryDateCin) {
        this.deliveryDateCin = deliveryDateCin;
    }

    public String getRectoCin() {
        return rectoCin;
    }

    public void setRectoCin(String rectoCin) {
        this.rectoCin = rectoCin;
    }

    public String getVersoCin() {
        return versoCin;
    }

    public void setVersoCin(String versoCin) {
        this.versoCin = versoCin;
    }

    public String getFatca() {
        return fatca;
    }

    public void setFatca(String fatca) {
        this.fatca = fatca;
    }

    public Long getRequestId() {
        return requestId;
    }

    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequiredDocDTO)) {
            return false;
        }

        return id != null && id.equals(((RequiredDocDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RequiredDocDTO{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", type='" + getType() + "'" +
            ", numCIN='" + getNumCIN() + "'" +
            ", deliveryDateCin='" + getDeliveryDateCin() + "'" +
            ", rectoCin='" + getRectoCin() + "'" +
            ", versoCin='" + getVersoCin() + "'" +
            ", fatca='" + getFatca() + "'" +
            ", requestId=" + getRequestId() +
            "}";
    }
}
