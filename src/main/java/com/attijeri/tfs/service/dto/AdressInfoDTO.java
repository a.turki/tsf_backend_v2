package com.attijeri.tfs.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.attijeri.tfs.domain.AdressInfo} entity.
 */
public class AdressInfoDTO implements Serializable {
    
    private Long id;

    private String countryOfResidence;

    private String address;

    private Integer zip;

    private String city;


    private Long personalInfoId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountryOfResidence() {
        return countryOfResidence;
    }

    public void setCountryOfResidence(String countryOfResidence) {
        this.countryOfResidence = countryOfResidence;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getZip() {
        return zip;
    }

    public void setZip(Integer zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Long getPersonalInfoId() {
        return personalInfoId;
    }

    public void setPersonalInfoId(Long personalInfoId) {
        this.personalInfoId = personalInfoId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AdressInfoDTO)) {
            return false;
        }

        return id != null && id.equals(((AdressInfoDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AdressInfoDTO{" +
            "id=" + getId() +
            ", countryOfResidence='" + getCountryOfResidence() + "'" +
            ", address='" + getAddress() + "'" +
            ", zip=" + getZip() +
            ", city='" + getCity() + "'" +
            ", personalInfoId=" + getPersonalInfoId() +
            "}";
    }
}
