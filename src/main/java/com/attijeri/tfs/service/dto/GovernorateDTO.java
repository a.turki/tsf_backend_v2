package com.attijeri.tfs.service.dto;

import com.attijeri.tfs.domain.Municipality;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the {@link com.attijeri.tfs.domain.Governorate} entity.
 */
public class GovernorateDTO implements Serializable {

    private Long id;

    private String name;

    private Set<MunicipalityDTO> municipalities = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<MunicipalityDTO> getMunicipalities() {
        return municipalities;
    }

    public void setMunicipalities(Set<MunicipalityDTO> municipalities) {
        this.municipalities = municipalities;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GovernorateDTO that = (GovernorateDTO) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(municipalities, that.municipalities);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    @Override
    public String toString() {
        return "GovernorateDTO{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", municipalities=" + municipalities +
            '}';
    }
}
