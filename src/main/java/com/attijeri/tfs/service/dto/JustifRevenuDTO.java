package com.attijeri.tfs.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.attijeri.tfs.domain.JustifRevenu} entity.
 */
public class JustifRevenuDTO implements Serializable {
    
    private Long id;

    private String labelEN;

    private String labelFR;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabelEN() {
        return labelEN;
    }

    public void setLabelEN(String labelEN) {
        this.labelEN = labelEN;
    }

    public String getLabelFR() {
        return labelFR;
    }

    public void setLabelFR(String labelFR) {
        this.labelFR = labelFR;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof JustifRevenuDTO)) {
            return false;
        }

        return id != null && id.equals(((JustifRevenuDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "JustifRevenuDTO{" +
            "id=" + getId() +
            ", labelEN='" + getLabelEN() + "'" +
            ", labelFR='" + getLabelFR() + "'" +
            "}";
    }
}
