package com.attijeri.tfs.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.attijeri.tfs.domain.RequiredDocIncome} entity.
 */
public class RequiredDocIncomeDTO implements Serializable {
    
    private Long id;

    private String type;

    private String path;


    private Long requiredDocId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getRequiredDocId() {
        return requiredDocId;
    }

    public void setRequiredDocId(Long requiredDocId) {
        this.requiredDocId = requiredDocId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequiredDocIncomeDTO)) {
            return false;
        }

        return id != null && id.equals(((RequiredDocIncomeDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RequiredDocIncomeDTO{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            ", path='" + getPath() + "'" +
            ", requiredDocId=" + getRequiredDocId() +
            "}";
    }
}
