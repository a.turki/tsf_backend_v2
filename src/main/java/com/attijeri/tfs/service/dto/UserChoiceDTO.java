package com.attijeri.tfs.service.dto;

import com.attijeri.tfs.domain.BankAccount;
import com.attijeri.tfs.domain.enumeration.Civility;

import java.time.LocalDate;
import java.util.Objects;
import java.util.Set;

public class UserChoiceDTO {
    private String firstName;
    private String lastName;
    private Civility civility;
    private String email;
    private String phone ;
    private LocalDate birthday;
    private String langue;
    private String nativeCountry;
    private Set<BankAccountDTO> accounts;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Civility getCivility() {
        return civility;
    }

    public void setCivility(Civility civility) {
        this.civility = civility;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(String langue) {
        this.langue = langue;
    }

    public String getNativeCountry() {
        return nativeCountry;
    }

    public void setNativeCountry(String nativeCountry) {
        this.nativeCountry = nativeCountry;
    }

    public Set<BankAccountDTO> getAccounts() {
        return accounts;
    }

    public void setAccounts(Set<BankAccountDTO> accounts) {
        this.accounts = accounts;
    }

    @Override
    public String toString() {
        return "UserChoiceDTO{" +
            "firstname='" + firstName + '\'' +
            ", lastname='" + lastName + '\'' +
            ", civility=" + civility +
            ", email='" + email + '\'' +
            ", phone='" + phone + '\'' +
            ", birthday=" + birthday +
            ", langue='" + langue + '\'' +
            ", nativeCountry='" + nativeCountry + '\'' +
            ", accounts=" + accounts +
            '}';
    }
}
