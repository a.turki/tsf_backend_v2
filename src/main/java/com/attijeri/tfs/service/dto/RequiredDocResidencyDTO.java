package com.attijeri.tfs.service.dto;

import java.time.LocalDate;
import java.io.Serializable;

/**
 * A DTO for the {@link com.attijeri.tfs.domain.RequiredDocResidency} entity.
 */
public class RequiredDocResidencyDTO implements Serializable {
    
    private Long id;

    private String type;

    private String num;

    private LocalDate deliveryDate;

    private LocalDate experationDate;

    private Boolean illimitedExperationDate;

    private String residencyRecto;

    private String residencyVerso;


    private Long requiredDocId;

    private Long residencyDocumentId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public LocalDate getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(LocalDate deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public LocalDate getExperationDate() {
        return experationDate;
    }

    public void setExperationDate(LocalDate experationDate) {
        this.experationDate = experationDate;
    }

    public Boolean isIllimitedExperationDate() {
        return illimitedExperationDate;
    }

    public void setIllimitedExperationDate(Boolean illimitedExperationDate) {
        this.illimitedExperationDate = illimitedExperationDate;
    }

    public String getResidencyRecto() {
        return residencyRecto;
    }

    public void setResidencyRecto(String residencyRecto) {
        this.residencyRecto = residencyRecto;
    }

    public String getResidencyVerso() {
        return residencyVerso;
    }

    public void setResidencyVerso(String residencyVerso) {
        this.residencyVerso = residencyVerso;
    }

    public Long getRequiredDocId() {
        return requiredDocId;
    }

    public void setRequiredDocId(Long requiredDocId) {
        this.requiredDocId = requiredDocId;
    }

    public Long getResidencyDocumentId() {
        return residencyDocumentId;
    }

    public void setResidencyDocumentId(Long residencyDocumentId) {
        this.residencyDocumentId = residencyDocumentId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequiredDocResidencyDTO)) {
            return false;
        }

        return id != null && id.equals(((RequiredDocResidencyDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RequiredDocResidencyDTO{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            ", num='" + getNum() + "'" +
            ", deliveryDate='" + getDeliveryDate() + "'" +
            ", experationDate='" + getExperationDate() + "'" +
            ", illimitedExperationDate='" + isIllimitedExperationDate() + "'" +
            ", residencyRecto='" + getResidencyRecto() + "'" +
            ", residencyVerso='" + getResidencyVerso() + "'" +
            ", requiredDocId=" + getRequiredDocId() +
            ", residencyDocumentId=" + getResidencyDocumentId() +
            "}";
    }
}
