package com.attijeri.tfs.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.attijeri.tfs.domain.FinancialInfo} entity.
 */
public class FinancialInfoDTO implements Serializable {
    
    private Long id;


    private Long activityId;

    private Long categoryId;

    private Long personalInfoId;

    private Long monthlyNetIncomeId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getActivityId() {
        return activityId;
    }

    public void setActivityId(Long activityId) {
        this.activityId = activityId;
    }

    public Long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Long categoryId) {
        this.categoryId = categoryId;
    }

    public Long getPersonalInfoId() {
        return personalInfoId;
    }

    public void setPersonalInfoId(Long personalInfoId) {
        this.personalInfoId = personalInfoId;
    }

    public Long getMonthlyNetIncomeId() {
        return monthlyNetIncomeId;
    }

    public void setMonthlyNetIncomeId(Long monthlyNetIncomeId) {
        this.monthlyNetIncomeId = monthlyNetIncomeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FinancialInfoDTO)) {
            return false;
        }

        return id != null && id.equals(((FinancialInfoDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FinancialInfoDTO{" +
            "id=" + getId() +
            ", activityId=" + getActivityId() +
            ", categoryId=" + getCategoryId() +
            ", personalInfoId=" + getPersonalInfoId() +
            ", monthlyNetIncomeId=" + getMonthlyNetIncomeId() +
            "}";
    }
}
