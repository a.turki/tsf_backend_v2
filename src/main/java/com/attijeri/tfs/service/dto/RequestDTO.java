package com.attijeri.tfs.service.dto;

import java.time.LocalDate;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A DTO for the {@link com.attijeri.tfs.domain.Request} entity.
 */
public class RequestDTO implements Serializable {
    
    private Long id;

    private LocalDate visioDate;

    private LocalDate sendingMailDate;

    private Boolean state;

    private String step;

    private String codeVerification;


    private Long offerId;
    private Set<BankAccountDTO> bankAccounts = new HashSet<>();
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getVisioDate() {
        return visioDate;
    }

    public void setVisioDate(LocalDate visioDate) {
        this.visioDate = visioDate;
    }

    public LocalDate getSendingMailDate() {
        return sendingMailDate;
    }

    public void setSendingMailDate(LocalDate sendingMailDate) {
        this.sendingMailDate = sendingMailDate;
    }

    public Boolean isState() {
        return state;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public String getStep() {
        return step;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getCodeVerification() {
        return codeVerification;
    }

    public void setCodeVerification(String codeVerification) {
        this.codeVerification = codeVerification;
    }

    public Long getOfferId() {
        return offerId;
    }

    public void setOfferId(Long offerId) {
        this.offerId = offerId;
    }

    public Set<BankAccountDTO> getBankAccounts() {
        return bankAccounts;
    }

    public void setBankAccounts(Set<BankAccountDTO> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequestDTO)) {
            return false;
        }

        return id != null && id.equals(((RequestDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RequestDTO{" +
            "id=" + getId() +
            ", visioDate='" + getVisioDate() + "'" +
            ", sendingMailDate='" + getSendingMailDate() + "'" +
            ", state='" + isState() + "'" +
            ", step='" + getStep() + "'" +
            ", codeVerification='" + getCodeVerification() + "'" +
            ", offerId=" + getOfferId() +
            ", bankAccounts='" + getBankAccounts() + "'" +
            "}";
    }
}
