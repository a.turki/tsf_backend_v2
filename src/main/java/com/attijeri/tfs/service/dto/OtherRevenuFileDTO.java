package com.attijeri.tfs.service.dto;

import java.io.Serializable;

/**
 * A DTO for the {@link com.attijeri.tfs.domain.OtherRevenuFile} entity.
 */
public class OtherRevenuFileDTO implements Serializable {
    
    private Long id;

    private String path;


    private Long requiredDocId;

    private Long justifRevenuId;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public Long getRequiredDocId() {
        return requiredDocId;
    }

    public void setRequiredDocId(Long requiredDocId) {
        this.requiredDocId = requiredDocId;
    }

    public Long getJustifRevenuId() {
        return justifRevenuId;
    }

    public void setJustifRevenuId(Long justifRevenuId) {
        this.justifRevenuId = justifRevenuId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OtherRevenuFileDTO)) {
            return false;
        }

        return id != null && id.equals(((OtherRevenuFileDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OtherRevenuFileDTO{" +
            "id=" + getId() +
            ", path='" + getPath() + "'" +
            ", requiredDocId=" + getRequiredDocId() +
            ", justifRevenuId=" + getJustifRevenuId() +
            "}";
    }
}
