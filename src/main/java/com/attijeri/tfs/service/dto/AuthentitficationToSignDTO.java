package com.attijeri.tfs.service.dto;

import java.time.LocalDate;
import java.io.Serializable;
import java.util.Date;

/**
 * A DTO for the {@link com.attijeri.tfs.domain.AuthentitficationToSign} entity.
 */
public class AuthentitficationToSignDTO implements Serializable {

    private Long id;

    private String email;

    private String token;

    private Date dateCreation;

    private Boolean valide;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Boolean getValide() {
        return valide;
    }

    public Boolean isValide() {
        return valide;
    }

    public void setValide(Boolean valide) {
        this.valide = valide;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AuthentitficationToSignDTO)) {
            return false;
        }

        return id != null && id.equals(((AuthentitficationToSignDTO) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AuthentitficationToSignDTO{" +
            "id=" + getId() +
            ", email='" + getEmail() + "'" +
            ", token='" + getToken() + "'" +
            ", dateCreation='" + getDateCreation() + "'" +
            ", valide='" + isValide() + "'" +
            "}";
    }
}
