package com.attijeri.tfs.service.dto;

public class SubscriberInfoOutputDTO {
   private String email;
    private String first_name;
    private String last_name;
    private String birthday;
    private String cin;
    private String phone;
    private String cin_recto;
    private String cin_verso;

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getCin() {
        return cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCin_recto() {
        return cin_recto;
    }

    public void setCin_recto(String cin_recto) {
        this.cin_recto = cin_recto;
    }

    public String getCin_verso() {
        return cin_verso;
    }

    public void setCin_verso(String cin_verso) {
        this.cin_verso = cin_verso;
    }
}
