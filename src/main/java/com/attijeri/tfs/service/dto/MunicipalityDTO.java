package com.attijeri.tfs.service.dto;

import com.attijeri.tfs.domain.Agency;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * A DTO for the {@link com.attijeri.tfs.domain.Municipality} entity.
 */
public class MunicipalityDTO implements Serializable {

    private Long id;

    private String name;

    private Long governorateId;

    private Set<AgencyDTO> agencies = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getGovernorateId() {
        return governorateId;
    }

    public void setGovernorateId(Long governorateId) {
        this.governorateId = governorateId;
    }


    public Set<AgencyDTO> getAgencies() {
        return agencies;
    }

    public void setAgencies(Set<AgencyDTO> agencies) {
        this.agencies = agencies;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MunicipalityDTO that = (MunicipalityDTO) o;
        return Objects.equals(id, that.id) &&
            Objects.equals(name, that.name) &&
            Objects.equals(governorateId, that.governorateId) &&
            Objects.equals(agencies, that.agencies);
    }

    @Override
    public String toString() {
        return "MunicipalityDTO{" +
            "id=" + id +
            ", name='" + name + '\'' +
            ", governorateId=" + governorateId +
            ", agencies=" + agencies +
            '}';
    }

    @Override
    public int hashCode() {
        return 31;
    }


}
