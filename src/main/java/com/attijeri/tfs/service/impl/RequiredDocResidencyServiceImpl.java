package com.attijeri.tfs.service.impl;

import com.attijeri.tfs.domain.RequiredDoc;
import com.attijeri.tfs.repository.RequiredDocRepository;
import com.attijeri.tfs.service.RequiredDocResidencyService;
import com.attijeri.tfs.domain.RequiredDocResidency;
import com.attijeri.tfs.repository.RequiredDocResidencyRepository;
import com.attijeri.tfs.service.dto.RequiredDocResidencyDTO;
import com.attijeri.tfs.service.mapper.RequiredDocResidencyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link RequiredDocResidency}.
 */
@Service
@Transactional
public class RequiredDocResidencyServiceImpl implements RequiredDocResidencyService {

    private final Logger log = LoggerFactory.getLogger(RequiredDocResidencyServiceImpl.class);

    private final RequiredDocResidencyRepository requiredDocResidencyRepository;

    private final RequiredDocResidencyMapper requiredDocResidencyMapper;
    @Autowired
    public   RequiredDocRepository requiredDocRepository;

    public RequiredDocResidencyServiceImpl(RequiredDocResidencyRepository requiredDocResidencyRepository, RequiredDocResidencyMapper requiredDocResidencyMapper) {
        this.requiredDocResidencyRepository = requiredDocResidencyRepository;
        this.requiredDocResidencyMapper = requiredDocResidencyMapper;
    }

    @Override
    public RequiredDocResidencyDTO save(RequiredDocResidencyDTO requiredDocResidencyDTO) {
        log.debug("Request to save RequiredDocResidency : {}", requiredDocResidencyDTO);
        RequiredDocResidency requiredDocResidency = requiredDocResidencyMapper.toEntity(requiredDocResidencyDTO);
        requiredDocResidency = requiredDocResidencyRepository.save(requiredDocResidency);
        return requiredDocResidencyMapper.toDto(requiredDocResidency);
    }

    @Override
    @Transactional(readOnly = true)
    public List<RequiredDocResidencyDTO> findAll() {
        log.debug("Request to get all RequiredDocResidencies");
        return requiredDocResidencyRepository.findAll().stream()
            .map(requiredDocResidencyMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<RequiredDocResidencyDTO> findOne(Long id) {
        log.debug("Request to get RequiredDocResidency : {}", id);
        return requiredDocResidencyRepository.findById(id)
            .map(requiredDocResidencyMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete RequiredDocResidency : {}", id);
        requiredDocResidencyRepository.deleteById(id);
    }

    @Override
    public Optional<RequiredDocResidencyDTO> getRequiredResidencyByRequestId(Long requestId) {
        RequiredDoc requiredDoc = requiredDocRepository.findByRequestId(requestId);
        Long requiredDocId = requiredDoc.getId();
        RequiredDocResidency requiredDocResidency = requiredDocResidencyRepository.findByRequiredDocId(requiredDocId);
        RequiredDocResidencyDTO requiredDocResidencyDTO = requiredDocResidencyMapper.toDto(requiredDocResidency);
        return Optional.ofNullable(requiredDocResidencyDTO);
    }
}
