package com.attijeri.tfs.service.impl;

import com.attijeri.tfs.service.OtherResidencyFileService;
import com.attijeri.tfs.domain.OtherResidencyFile;
import com.attijeri.tfs.repository.OtherResidencyFileRepository;
import com.attijeri.tfs.service.dto.OtherResidencyFileDTO;
import com.attijeri.tfs.service.dto.OtherRevenuFileDTO;
import com.attijeri.tfs.service.mapper.OtherResidencyFileMapper;
import com.attijeri.tfs.utlis.FtpServeur;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.ftp.session.FtpSession;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link OtherResidencyFile}.
 */
@Service
@Transactional
public class OtherResidencyFileServiceImpl implements OtherResidencyFileService {

    private final Logger log = LoggerFactory.getLogger(OtherResidencyFileServiceImpl.class);

    private final OtherResidencyFileRepository otherResidencyFileRepository;

    private final OtherResidencyFileMapper otherResidencyFileMapper;
    @Autowired
    FtpServeur ftpSeurveur;

    public OtherResidencyFileServiceImpl(OtherResidencyFileRepository otherResidencyFileRepository, OtherResidencyFileMapper otherResidencyFileMapper) {
        this.otherResidencyFileRepository = otherResidencyFileRepository;
        this.otherResidencyFileMapper = otherResidencyFileMapper;
    }

    @Override
    public OtherResidencyFileDTO save(OtherResidencyFileDTO otherResidencyFileDTO) {
        log.debug("Request to save OtherResidencyFile : {}", otherResidencyFileDTO);
        OtherResidencyFile otherResidencyFile = otherResidencyFileMapper.toEntity(otherResidencyFileDTO);
        otherResidencyFile = otherResidencyFileRepository.save(otherResidencyFile);
        return otherResidencyFileMapper.toDto(otherResidencyFile);
    }

    @Override
    @Transactional(readOnly = true)
    public List<OtherResidencyFileDTO> findAll() {
        log.debug("Request to get all OtherResidencyFiles");
        return otherResidencyFileRepository.findAll().stream()
            .map(otherResidencyFileMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<OtherResidencyFileDTO> findOne(Long id) {
        log.debug("Request to get OtherResidencyFile : {}", id);
        return otherResidencyFileRepository.findById(id)
            .map(otherResidencyFileMapper::toDto);
    }

    @Override
    public void delete(Long id) throws IOException {
        try {
            FtpSession session = ftpSeurveur.gimmeFactory().getSession();
            Optional<OtherResidencyFileDTO> otherResidencyFile = this.findOne(id);
            session.remove(otherResidencyFile.get().getPath());
            log.debug("Request to delete OtherResidencyFile : {}", id);
            otherResidencyFileRepository.deleteById(id);
        }
        catch (Exception e){
            log.debug("Erreur de delete file : {}", id);
            otherResidencyFileRepository.deleteById(id);
        }
    }


    @Override
    @Transactional(readOnly = true)
    public List<OtherResidencyFileDTO> getAllResidencySameRequestId(Long id) {
        log.debug("Request to get all OtherRevenuFiles");
        return otherResidencyFileRepository.findAllRevenuByRequestId(id).stream()
            .map(otherResidencyFileMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }
}

