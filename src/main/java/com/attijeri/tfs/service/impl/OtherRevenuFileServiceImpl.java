package com.attijeri.tfs.service.impl;

import com.attijeri.tfs.service.OtherRevenuFileService;
import com.attijeri.tfs.domain.OtherRevenuFile;
import com.attijeri.tfs.repository.OtherRevenuFileRepository;
import com.attijeri.tfs.service.dto.OtherRevenuFileDTO;
import com.attijeri.tfs.service.mapper.OtherRevenuFileMapper;
import com.attijeri.tfs.utlis.FtpServeur;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.integration.ftp.session.FtpSession;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link OtherRevenuFile}.
 */
@Service
@Transactional
public class OtherRevenuFileServiceImpl implements OtherRevenuFileService {

    private final Logger log = LoggerFactory.getLogger(OtherRevenuFileServiceImpl.class);

    private final OtherRevenuFileRepository otherRevenuFileRepository;

    private final OtherRevenuFileMapper otherRevenuFileMapper;
    @Autowired
    FtpServeur ftpSeurveur;

    public OtherRevenuFileServiceImpl(OtherRevenuFileRepository otherRevenuFileRepository, OtherRevenuFileMapper otherRevenuFileMapper) {
        this.otherRevenuFileRepository = otherRevenuFileRepository;
        this.otherRevenuFileMapper = otherRevenuFileMapper;
    }

    @Override
    public OtherRevenuFileDTO save(OtherRevenuFileDTO otherRevenuFileDTO) {
        log.debug("Request to save OtherRevenuFile : {}", otherRevenuFileDTO);
        OtherRevenuFile otherRevenuFile = otherRevenuFileMapper.toEntity(otherRevenuFileDTO);
        otherRevenuFile = otherRevenuFileRepository.saveAndFlush(otherRevenuFile);
        return otherRevenuFileMapper.toDto(otherRevenuFile);
    }

    @Override
    @Transactional(readOnly = true)
    public List<OtherRevenuFileDTO> findAll() {
        log.debug("Request to get all OtherRevenuFiles");
        return otherRevenuFileRepository.findAll().stream()
            .map(otherRevenuFileMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<OtherRevenuFileDTO> findOne(Long id) {
        log.debug("Request to get OtherRevenuFile : {}", id);
        return otherRevenuFileRepository.findById(id)
            .map(otherRevenuFileMapper::toDto);
    }

    @Override
    public void delete(Long id) throws IOException {
        try {
            FtpSession session = ftpSeurveur.gimmeFactory().getSession();
            Optional<OtherRevenuFileDTO> otherRevenuFile = this.findOne(id);
            session.remove(otherRevenuFile.get().getPath());
            log.debug("Request to delete OtherRevenuFile : {}", id);
            otherRevenuFileRepository.deleteById(id);
        }
        catch (Exception e){
            log.debug("problem delete file  OtherRevenuFile : {}", id);
            otherRevenuFileRepository.deleteById(id);

        }
    }


    @Override
    @Transactional(readOnly = true)
    public List<OtherRevenuFileDTO> getAllRevenuSameRequestId(Long id) {
        log.debug("Request to get all OtherRevenuFiles");
        return otherRevenuFileRepository.findAllRevenuByRequestId(id).stream()
            .map(otherRevenuFileMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }
}
