package com.attijeri.tfs.service.impl;

import com.attijeri.tfs.service.AdressInfoService;
import com.attijeri.tfs.domain.AdressInfo;
import com.attijeri.tfs.repository.AdressInfoRepository;
import com.attijeri.tfs.service.dto.AdressInfoDTO;
import com.attijeri.tfs.service.dto.PersonalInfoDTO;
import com.attijeri.tfs.service.mapper.AdressInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link AdressInfo}.
 */
@Service
@Transactional
public class AdressInfoServiceImpl implements AdressInfoService {

    private final Logger log = LoggerFactory.getLogger(AdressInfoServiceImpl.class);

    private final AdressInfoRepository adressInfoRepository;

    private final AdressInfoMapper adressInfoMapper;

    public AdressInfoServiceImpl(AdressInfoRepository adressInfoRepository, AdressInfoMapper adressInfoMapper) {
        this.adressInfoRepository = adressInfoRepository;
        this.adressInfoMapper = adressInfoMapper;
    }

    @Override
    public AdressInfoDTO save(AdressInfoDTO adressInfoDTO) {
        log.debug("Request to save AdressInfo : {}", adressInfoDTO);
        AdressInfo adressInfo = adressInfoMapper.toEntity(adressInfoDTO);
        adressInfo = adressInfoRepository.save(adressInfo);
        return adressInfoMapper.toDto(adressInfo);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AdressInfoDTO> findAll() {
        log.debug("Request to get all AdressInfos");
        return adressInfoRepository.findAll().stream()
            .map(adressInfoMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<AdressInfoDTO> findOne(Long id) {
        log.debug("Request to get AdressInfo : {}", id);
        return adressInfoRepository.findById(id)
            .map(adressInfoMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete AdressInfo : {}", id);
        adressInfoRepository.deleteById(id);
    }

    @Override
    public AdressInfoDTO findAddress(Long personalId) {
        AdressInfoDTO adressInfoDTO1 = new AdressInfoDTO();
        for (AdressInfoDTO adressInfoDTO : findAll() ){
            if(adressInfoDTO.getPersonalInfoId().equals(personalId)){
                adressInfoDTO1 = adressInfoDTO;
            }
        }
        return adressInfoDTO1;
    }
}
