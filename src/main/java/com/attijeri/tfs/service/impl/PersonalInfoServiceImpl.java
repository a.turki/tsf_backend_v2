package com.attijeri.tfs.service.impl;

import com.attijeri.tfs.service.PersonalInfoService;
import com.attijeri.tfs.domain.PersonalInfo;
import com.attijeri.tfs.repository.PersonalInfoRepository;
import com.attijeri.tfs.service.RequestService;
import com.attijeri.tfs.service.dto.PersonalInfoDTO;
import com.attijeri.tfs.service.mapper.PersonalInfoMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing {@link PersonalInfo}.
 */
@Service
@Transactional
public class PersonalInfoServiceImpl implements PersonalInfoService {

    private final Logger log = LoggerFactory.getLogger(PersonalInfoServiceImpl.class);
    private final static int mailLastTime = 2 ;
    private final PersonalInfoRepository personalInfoRepository;
    private final RequestService requestService;
    private final PersonalInfoMapper personalInfoMapper;

    public PersonalInfoServiceImpl(PersonalInfoRepository personalInfoRepository, PersonalInfoMapper personalInfoMapper, RequestService requestService) {
        this.personalInfoRepository = personalInfoRepository;
        this.personalInfoMapper = personalInfoMapper;
        this.requestService = requestService;
    }


    @Override
    public PersonalInfoDTO save(PersonalInfoDTO personalInfoDTO) {
        log.debug("Request to save PersonalInfo : {}", personalInfoDTO);
        PersonalInfo personalInfo = personalInfoMapper.toEntity(personalInfoDTO);
        personalInfo = personalInfoRepository.save(personalInfo);
        return personalInfoMapper.toDto(personalInfo);
    }

    @Override
    @Transactional(readOnly = true)
    public List<PersonalInfoDTO> findAll() {
        log.debug("Request to get all PersonalInfos");
        return personalInfoRepository.findAll().stream()
            .map(personalInfoMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }



    /**
     *  Get all the personalInfos where AdressInfo is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PersonalInfoDTO> findAllWhereAdressInfoIsNull() {
        log.debug("Request to get all personalInfos where AdressInfo is null");
        return StreamSupport
            .stream(personalInfoRepository.findAll().spliterator(), false)
            .filter(personalInfo -> personalInfo.getAdressInfo() == null)
            .map(personalInfoMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     *  Get all the personalInfos where FinancialInfo is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<PersonalInfoDTO> findAllWhereFinancialInfoIsNull() {
        log.debug("Request to get all personalInfos where FinancialInfo is null");
        return StreamSupport
            .stream(personalInfoRepository.findAll().spliterator(), false)
            .filter(personalInfo -> personalInfo.getFinancialInfo() == null)
            .map(personalInfoMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<PersonalInfoDTO> findOne(Long id) {
        log.debug("Request to get PersonalInfo : {}", id);
        return personalInfoRepository.findById(id)
            .map(personalInfoMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete PersonalInfo : {}", id);
        personalInfoRepository.deleteById(id);
    }
    @Override
    public PersonalInfoDTO update(PersonalInfoDTO personalInfoDTO) {
        log.debug("Request to update PersonalInfo : {}", personalInfoDTO);
        Optional<PersonalInfoDTO> result = findOne(personalInfoDTO.getId());
        if (result.isPresent()) {
            PersonalInfo personalInfo = personalInfoMapper.toEntity(personalInfoDTO);
            personalInfo = personalInfoRepository.save(personalInfo);
            return personalInfoMapper.toDto(personalInfo);
        }
        return null;
    }

    @Override
    public boolean validEmail(String email){
        boolean validMail = false;
        for(PersonalInfoDTO personalInfoDTO : findAll()){
            if(personalInfoDTO.getEmail().trim().equalsIgnoreCase(email)){
                validMail = true;
            }
        }
        return validMail;
    }

    @Override
    public PersonalInfoDTO findByRequestId(Long requestId) {
        PersonalInfoDTO personalInfoDTO1 = new PersonalInfoDTO();
        for (PersonalInfoDTO personalInfoDTO : findAll() ){
            if(personalInfoDTO.getRequestId().equals(requestId)){
                personalInfoDTO1 = personalInfoDTO;
            }
        }
        return personalInfoDTO1;
    }

    //@Scheduled( fixedRate = 5000)
    @Scheduled(cron = "* * 1 * * *")
    public String deleteRequestNotUpdated(){
        requestService.findAll().forEach(requestDTO -> {
            if ((LocalDate.now().getDayOfYear() - requestDTO.getSendingMailDate().getDayOfYear() > mailLastTime) && (requestDTO.isState()==false)){
                delete(findPersonalInfoForRequestNotUpdated(requestDTO.getId()));
                requestService.delete(requestDTO.getId());
            }
        });
        return "delete request" ;
    }

    public Long findPersonalInfoForRequestNotUpdated(Long id){
        PersonalInfoDTO personalInfo = new PersonalInfoDTO();
        for (PersonalInfoDTO personal :findAll() ){
            if(personal.getRequestId().equals(id)){
                personalInfo = personal ;
            }
        }
        return personalInfo.getId();
    }
}
