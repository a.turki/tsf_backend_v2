package com.attijeri.tfs.service.impl;

import com.attijeri.tfs.service.JustifRevenuService;
import com.attijeri.tfs.domain.JustifRevenu;
import com.attijeri.tfs.repository.JustifRevenuRepository;
import com.attijeri.tfs.service.dto.JustifRevenuDTO;
import com.attijeri.tfs.service.mapper.JustifRevenuMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link JustifRevenu}.
 */
@Service
@Transactional
public class JustifRevenuServiceImpl implements JustifRevenuService {

    private final Logger log = LoggerFactory.getLogger(JustifRevenuServiceImpl.class);

    private final JustifRevenuRepository justifRevenuRepository;

    private final JustifRevenuMapper justifRevenuMapper;

    public JustifRevenuServiceImpl(JustifRevenuRepository justifRevenuRepository, JustifRevenuMapper justifRevenuMapper) {
        this.justifRevenuRepository = justifRevenuRepository;
        this.justifRevenuMapper = justifRevenuMapper;
    }

    @Override
    public JustifRevenuDTO save(JustifRevenuDTO justifRevenuDTO) {
        log.debug("Request to save JustifRevenu : {}", justifRevenuDTO);
        JustifRevenu justifRevenu = justifRevenuMapper.toEntity(justifRevenuDTO);
        justifRevenu = justifRevenuRepository.save(justifRevenu);
        return justifRevenuMapper.toDto(justifRevenu);
    }

    @Override
    @Transactional(readOnly = true)
    public List<JustifRevenuDTO> findAll() {
        log.debug("Request to get all JustifRevenus");
        return justifRevenuRepository.findAll().stream()
            .map(justifRevenuMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<JustifRevenuDTO> findOne(Long id) {
        log.debug("Request to get JustifRevenu : {}", id);
        return justifRevenuRepository.findById(id)
            .map(justifRevenuMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete JustifRevenu : {}", id);
        justifRevenuRepository.deleteById(id);
    }
}
