package com.attijeri.tfs.service.impl;

import com.attijeri.tfs.service.ResidencyDocumentService;
import com.attijeri.tfs.domain.ResidencyDocument;
import com.attijeri.tfs.repository.ResidencyDocumentRepository;
import com.attijeri.tfs.service.dto.ResidencyDocumentDTO;
import com.attijeri.tfs.service.mapper.ResidencyDocumentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing {@link ResidencyDocument}.
 */
@Service
@Transactional
public class ResidencyDocumentServiceImpl implements ResidencyDocumentService {

    private final Logger log = LoggerFactory.getLogger(ResidencyDocumentServiceImpl.class);

    private final ResidencyDocumentRepository residencyDocumentRepository;

    private final ResidencyDocumentMapper residencyDocumentMapper;

    public ResidencyDocumentServiceImpl(ResidencyDocumentRepository residencyDocumentRepository, ResidencyDocumentMapper residencyDocumentMapper) {
        this.residencyDocumentRepository = residencyDocumentRepository;
        this.residencyDocumentMapper = residencyDocumentMapper;
    }

    @Override
    public ResidencyDocumentDTO save(ResidencyDocumentDTO residencyDocumentDTO) {
        log.debug("Request to save ResidencyDocument : {}", residencyDocumentDTO);
        ResidencyDocument residencyDocument = residencyDocumentMapper.toEntity(residencyDocumentDTO);
        residencyDocument = residencyDocumentRepository.save(residencyDocument);
        return residencyDocumentMapper.toDto(residencyDocument);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ResidencyDocumentDTO> findAll() {
        log.debug("Request to get all ResidencyDocuments");
        return residencyDocumentRepository.findAll().stream()
            .map(residencyDocumentMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }



    /**
     *  Get all the residencyDocuments where RequiredDocResidency is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true) 
    public List<ResidencyDocumentDTO> findAllWhereRequiredDocResidencyIsNull() {
        log.debug("Request to get all residencyDocuments where RequiredDocResidency is null");
        return StreamSupport
            .stream(residencyDocumentRepository.findAll().spliterator(), false)
            .filter(residencyDocument -> residencyDocument.getRequiredDocResidency() == null)
            .map(residencyDocumentMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ResidencyDocumentDTO> findOne(Long id) {
        log.debug("Request to get ResidencyDocument : {}", id);
        return residencyDocumentRepository.findById(id)
            .map(residencyDocumentMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ResidencyDocument : {}", id);
        residencyDocumentRepository.deleteById(id);
    }
}
