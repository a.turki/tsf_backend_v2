package com.attijeri.tfs.service.impl;

import com.attijeri.tfs.domain.Request;
import com.attijeri.tfs.service.ReqDocUploadService;
import com.attijeri.tfs.domain.ReqDocUpload;
import com.attijeri.tfs.repository.ReqDocUploadRepository;
import com.attijeri.tfs.service.RequestService;
import com.attijeri.tfs.utlis.FtpServeur;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.integration.ftp.session.FtpSession;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.time.Instant;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing {@link ReqDocUpload}.
 */
@Service
@Transactional
public class ReqDocUploadServiceImpl implements ReqDocUploadService {

    private final Logger log = LoggerFactory.getLogger(ReqDocUploadServiceImpl.class);

    private final ReqDocUploadRepository reqDocUploadRepository;
    private final RequestService requestService;
    private final FtpServeur ftpSever;
    public ReqDocUploadServiceImpl(ReqDocUploadRepository reqDocUploadRepository, RequestService requestService, FtpServeur ftpSever) {
        this.reqDocUploadRepository = reqDocUploadRepository;
        this.requestService = requestService;
        this.ftpSever = ftpSever;
    }

    @Override
    public ReqDocUpload save(ReqDocUpload reqDocUpload) {
        log.debug("Request to save ReqDocUpload : {}", reqDocUpload);
        return reqDocUploadRepository.save(reqDocUpload);
    }

    @Override
    @Transactional(readOnly = true)
    public List<ReqDocUpload> findAll() {
        log.debug("Request to get all ReqDocUploads");
        return reqDocUploadRepository.findAll();
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<ReqDocUpload> findOne(Long id) {
        log.debug("Request to get ReqDocUpload : {}", id);
        return reqDocUploadRepository.findById(id);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete ReqDocUpload : {}", id);
        reqDocUploadRepository.deleteById(id);
    }

    @Override
    public ReqDocUpload uploadFile(MultipartFile file, Long requestId, String typeDoc) throws Exception {
        FtpSession session = ftpSever.gimmeFactory().getSession();
        String requestDocId=String.valueOf(requestId);
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter
                                                            .ofPattern("yyyy-MM-dd,HH.mm.ss")
                                                            .withZone(ZoneId.systemDefault());
        Instant updatedAt = Instant.now();
        String updatedAtString = dateTimeFormatter.format( updatedAt );

        String fileExtension;
        if (getFileExtension(file.getOriginalFilename()).isPresent())
            fileExtension = getFileExtension(file.getOriginalFilename()).get();
        else
            throw new Exception("file extension not present");
        System.out.println("here");
        String fileName=
            typeDoc
            +"_"
            +updatedAtString
            +"."
            +fileExtension;
        System.out.println(fileName);
        Request request = requestService.getRequestById(requestId);
        ReqDocUpload reqDocUpload = new ReqDocUpload();
        if (session != null) {
            if (!file.isEmpty()) {
                try {
                    if (!session.exists(requestDocId)) {
                        session.mkdir(requestDocId);
                    }
                    session.write(file.getInputStream(), requestDocId + "/" + fileName);
                    reqDocUpload.setPathDoc(requestDocId + "/" + fileName);
                    reqDocUpload.setTypeDoc(typeDoc);
                    reqDocUpload.setUpdatedAt(updatedAt);
                    reqDocUpload.setRequest(request);
                    System.out.println(reqDocUpload);
                    reqDocUpload = reqDocUploadRepository.save(reqDocUpload);
                    System.out.println(reqDocUpload);

                }
                catch(Exception e){
                    e.printStackTrace();
                }
                session.close();
            }
        }
        return reqDocUpload;
    }

    @Override
    public boolean deleteFile(Long reqDocId) throws Exception{

        FtpSession session = ftpSever.gimmeFactory().getSession();
        ReqDocUpload reqDocUpload = reqDocUploadRepository.getOne(reqDocId);
        if (session.remove(reqDocUpload.getPathDoc())) {
            reqDocUploadRepository.deleteById(reqDocId);
            return true;
        }
        return false;
    }

        public Optional<String> getFileExtension(String filename) {
        return Optional.ofNullable(filename)
            .filter(f -> f.contains("."))
            .map(f -> f.substring(filename.lastIndexOf(".") + 1));
    }
}
