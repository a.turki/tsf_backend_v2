package com.attijeri.tfs.service.impl;

import com.attijeri.tfs.config.optConfig.MiddleWareService;
import com.attijeri.tfs.config.optConfig.Response;
import com.attijeri.tfs.service.PersonalInfoService;
import com.attijeri.tfs.config.optConfig.MiddleWareService;
import com.attijeri.tfs.service.PersonalInfoService;
import com.attijeri.tfs.service.RequestService;
import com.attijeri.tfs.domain.Request;
import com.attijeri.tfs.repository.RequestRepository;
import com.attijeri.tfs.service.dto.PersonalInfoDTO;
import com.attijeri.tfs.service.dto.PersonalInfoDTO;
import com.attijeri.tfs.service.dto.RequestDTO;
import com.attijeri.tfs.service.mapper.RequestMapper;
import org.apache.commons.lang3.RandomStringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing {@link Request}.
 */
@Service
@Transactional
public class RequestServiceImpl implements RequestService {

    private final Logger log = LoggerFactory.getLogger(RequestServiceImpl.class);

    private final RequestRepository requestRepository;
    private final MiddleWareService middleWareService;

    private final RequestMapper requestMapper;

    private final PersonalInfoService personalInfoService;


    public RequestServiceImpl(RequestRepository requestRepository, MiddleWareService middleWareService, RequestMapper requestMapper,@Lazy PersonalInfoService personalInfoService) {
        this.requestRepository = requestRepository;
        this.middleWareService = middleWareService;
        this.requestMapper = requestMapper;
        this.personalInfoService = personalInfoService;
    }

    @Override
    public Request getRequestById(Long refCode){
        return requestRepository.getOne(refCode);
    }
    @Override
    public boolean followRequestMail(Long refCode) {
        PersonalInfoDTO personalInfoDTO = personalInfoService.findByRequestId(refCode);
        System.out.println(personalInfoDTO);
        String verificationCode = RandomStringUtils.random(6, false, true);
        Optional<RequestDTO> optionalRequestDTO = findOne(refCode);
        if (optionalRequestDTO.isPresent()){
            RequestDTO requestDTO = optionalRequestDTO.get();
            requestDTO.setCodeVerification(verificationCode);
            save(requestDTO);
            ResponseEntity<Response> response = middleWareService.followRequest(personalInfoDTO,"Code d'accés",verificationCode);
            System.out.println(response.getBody());
            System.out.println(response.getStatusCode());
            System.out.println(response.getBody().getDocumentHeader().getResultMessage());
            return response.getBody() != null && response.getBody().getDocumentHeader().getResultMessage().equals("SUCCESS");

        }
        return false;
    }

    @Override
    public RequestDTO verifyCode(Long idRequest, String verificationCode) {
        Optional<RequestDTO> optionalRequestDTO = findOne(idRequest);
        if (optionalRequestDTO.isPresent()) {
            RequestDTO requestDTO = optionalRequestDTO.get();
            if (verificationCode.equals(requestDTO.getCodeVerification()))
                return requestDTO;
        }


        return null;
    }

    @Override
    public RequestDTO save(RequestDTO requestDTO) {
        log.debug("Request to save Request : {}", requestDTO);
        Request request = requestMapper.toEntity(requestDTO);
        request = requestRepository.save(request);
        return requestMapper.toDto(request);
    }

    @Override
    @Transactional(readOnly = true)
    public List<RequestDTO> findAll() {
        log.debug("Request to get all Requests");
        return requestRepository.findAllWithEagerRelationships().stream()
            .map(requestMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    public Page<RequestDTO> findAllWithEagerRelationships(Pageable pageable) {
        return requestRepository.findAllWithEagerRelationships(pageable).map(requestMapper::toDto);
    }


    /**
     *  Get all the requests where PersonalInfo is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true)
    public List<RequestDTO> findAllWherePersonalInfoIsNull() {
        log.debug("Request to get all requests where PersonalInfo is null");
        return StreamSupport
            .stream(requestRepository.findAll().spliterator(), false)
            .filter(request -> request.getPersonalInfo() == null)
            .map(requestMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    /**
     *  Get all the requests where RequiredDoc is {@code null}.
     *  @return the list of entities.
     */
    @Override
    @Transactional(readOnly = true)
    public List<RequestDTO> findAllWhereRequiredDocIsNull() {
        log.debug("Request to get all requests where RequiredDoc is null");
        return StreamSupport
            .stream(requestRepository.findAll().spliterator(), false)
            .filter(request -> request.getRequiredDoc() == null)
            .map(requestMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    public List<RequestDTO> findAllWhereDocumentIsNull() {
        return null;
    }



    @Override
    @Transactional(readOnly = true)
    public Optional<RequestDTO> findOne(Long id) {
        log.debug("Request to get Request : {}", id);
        return requestRepository.findOneWithEagerRelationships(id)
            .map(requestMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Request : {}", id);
        requestRepository.deleteById(id);
    }


    @Override
    public RequestDTO findByRequestId(Long requestId) {
        RequestDTO requestDTO1 = new RequestDTO();
        for (RequestDTO requestDTO : findAll() ){
            if(requestDTO.getId().equals(requestId)){
                requestDTO1 = requestDTO;
            }
        }
        return requestDTO1;
    }
}
