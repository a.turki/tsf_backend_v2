package com.attijeri.tfs.service.impl;

import com.attijeri.tfs.domain.*;
import com.attijeri.tfs.repository.*;
import com.attijeri.tfs.service.OtherResidencyFileService;
import com.attijeri.tfs.service.OtherRevenuFileService;
import com.attijeri.tfs.service.RequiredDocIncomeService;
import com.attijeri.tfs.service.RequiredDocService;
import com.attijeri.tfs.service.dto.*;
import com.attijeri.tfs.service.mapper.*;
import com.attijeri.tfs.utlis.Dowanload;
import com.attijeri.tfs.utlis.FtpServeur;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.integration.ftp.session.FtpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link RequiredDoc}.
 */
@Service
@Transactional
public class RequiredDocServiceImpl implements RequiredDocService {
    private final Logger log = LoggerFactory.getLogger(RequiredDocServiceImpl.class);
    private final RequiredDocRepository requiredDocRepository;
    private final RequiredDocMapper requiredDocMapper;
    private static final String UPLOAD_DIR = "REQUIRED_DOC_DATA";
    private static final String DOC= "DOC";
    private static final String DOCRESIDENCY= "DOCRESIDENCY";
    private static final String DOCINCOME= "DOCINCOME";
    private static final String RECTOCIN = "rectoFile";
    private static final String FITCA= "fitcaFile";
    private static final String VERSOCIN= "versoFile";
    private static final String NATIOALITE= "nationaliteFile";
    private static final String PIECEREVENU= "pieceRevenuFile";
    private static final String RESIDENCYRECTO= "residencyRecto";
    private static final String RESIDENCYVERSO= "residencyVerso";
    private static final String AUTHERFILERESIDENCY= "autherFileRisdency";
    private static final String AUTHERFILEREVENUE= "autherFileRevenue";
    private static final String TYPERISDENCY= "typeRisdency";
    private static final String TYPEREVENUE= "typeRevenue";



    @Autowired
    FtpServeur ftpSeurveur;
    @Autowired
    RequestRepository requestRepository ;
    @Autowired
    RequiredDocIncomeRepository requiredDocIncomeRepository;
    @Autowired
    RequiredDocResidencyRepository requiredDocResidencyRepository;
    @Autowired
    RequiredDocIncomeMapper requiredDocIncomeMapper;
    @Autowired
    RequiredDocIncomeService requiredDocIncomeService;
    @Autowired
    RequiredDocResidencyMapper requiredDocResidencyMapper;
    @Autowired
    OtherResidencyFileRepository otherResidencyFileRepository;
    @Autowired
    RequiredDocResidencyServiceImpl requiredDocResidencyServiceImpl;
    @Autowired
    Dowanload dowanload;
    @Autowired
    OtherRevenuFileService otherRevenuFileService;
    @Autowired
    OtherRevenuFileMapper otherRevenuFileMapper;
    @Autowired
    OtherResidencyFileMapper otherResidencyFileMapper;
    @Autowired
    OtherResidencyFileService otherResidencyFileService;
    @Autowired
    OtherRevenuFileRepository otherRevenuFileRepository;
    public RequiredDocServiceImpl(RequiredDocRepository requiredDocRepository, RequiredDocMapper requiredDocMapper) {
        this.requiredDocRepository = requiredDocRepository;
        this.requiredDocMapper = requiredDocMapper;
    }
    @Override
    @Transactional(readOnly = true)
    public List<RequiredDocDTO> findAll() {
        log.debug("Request to get all RequiredDocs");
        return requiredDocRepository.findAll().stream()
            .map(requiredDocMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<RequiredDocDTO> findOne(Long id) {
        log.debug("Request to get RequiredDoc : {}", id);
        return requiredDocRepository.findById(id)
            .map(requiredDocMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete RequiredDoc : {}", id);
        requiredDocRepository.deleteById(id);
    }



    @Override
    public RequiredDocDTO save(RequiredDocDTO requiredDocDTO) {
        log.debug("Request to save RequiredDoc : {}", requiredDocDTO);
        RequiredDoc requiredDoc = requiredDocMapper.toEntity(requiredDocDTO);
        requiredDoc = requiredDocRepository.saveAndFlush(requiredDoc);
        return requiredDocMapper.toDto(requiredDoc);
    }

    public void saveRequiredDoc(RequiredDoc requiredDoc){
        requiredDocRepository.saveAndFlush(requiredDoc);

    }
    @Override
    public String deleteImportantDoc(Long requestId, String typeDoc){
        try {
            FtpSession session = ftpSeurveur.gimmeFactory().getSession();
            RequiredDoc requiredDoc = requiredDocRepository.findByRequestId(requestId);
            RequiredDocDTO requiredDocDTO = requiredDocMapper.toDto(requiredDoc);
            Long requiredDocId = requiredDoc.getId();
            if (typeDoc.equals(RECTOCIN) || typeDoc.equals(VERSOCIN) || typeDoc.equals(FITCA)) {
                if (typeDoc.equals(RECTOCIN)) {
                    session.remove(requiredDocDTO.getRectoCin());
                    requiredDocDTO.setRectoCin("");
                } else if (typeDoc.equals(VERSOCIN)) {
                    session.remove(requiredDocDTO.getVersoCin());
                    requiredDocDTO.setVersoCin("");
                } else if (typeDoc.equals(FITCA)) {
                    session.remove(requiredDocDTO.getFatca());
                    requiredDocDTO.setFatca("");
                }
                this.save(requiredDocDTO);
                requiredDocMapper.toDto(requiredDoc);
            }
            if (typeDoc.equals(RESIDENCYRECTO) || typeDoc.equals(RESIDENCYVERSO)) {

                RequiredDocResidency requiredDocResidency = requiredDocResidencyRepository.findByRequiredDocId(requiredDocId);
                RequiredDocResidencyDTO requiredDocResidencyDTO = requiredDocResidencyMapper.toDto(requiredDocResidency);
                if (typeDoc.equals(RESIDENCYRECTO)) {
                    session.remove(requiredDocResidencyDTO.getResidencyRecto());
                    requiredDocResidencyDTO.setResidencyRecto("");
                } else if (typeDoc.equals(RESIDENCYVERSO)) {
                    session.remove(requiredDocResidencyDTO.getResidencyVerso());
                    requiredDocResidencyDTO.setResidencyVerso("");
                }
                requiredDocResidencyServiceImpl.save(requiredDocResidencyDTO);
                requiredDocResidencyMapper.toDto(requiredDocResidency);
            }
            if (typeDoc.equals(NATIOALITE)){
                RequiredDocIncome  requiredDocIncome=requiredDocIncomeRepository.findByRequiredDocId(requiredDocId);
                RequiredDocIncomeDTO requiredDocIncomeDTO=requiredDocIncomeMapper.toDto(requiredDocIncome);
                session.remove(requiredDocIncomeDTO.getPath());
                requiredDocIncomeRepository.delete(requiredDocIncome);
            }
        }catch(Exception e){
            e.printStackTrace();
        }


        return  null;
    }

    @Override
    public FileDataDao uploadFile(MultipartFile file, Long requestId, String typeDoc) throws IOException {
        FtpSession session = ftpSeurveur.gimmeFactory().getSession();
        String requestDocId=String.valueOf(requestId);
        String pathDoc=requestDocId+"/"+DOC;
        String pathDocResidency=requestDocId+"/"+DOCRESIDENCY;
        String pathDocIncome=requestDocId+"/"+DOCINCOME;
        FileDataDao fileDataDao=new FileDataDao();
        if (session != null) {
            if (!file.isEmpty()) {
                RequiredDoc requiredDoc =  requiredDocRepository.findByRequestId(requestId);
                if(requiredDoc==null){
                    requiredDoc = new RequiredDoc();
                }
                RequiredDocDTO requiredDocDTO =requiredDocMapper.toDto(requiredDoc);
                requiredDocDTO.setRequestId(requestId);
                try {
                    if (!session.exists(requestDocId)) {
                        session.mkdir(requestDocId);
                    }
                    if (!session.exists(pathDoc)) {
                        session.mkdir(pathDoc);
                    }
                    if (!session.exists(pathDocResidency)) {
                        session.mkdir(pathDocResidency);
                    }
                    if (!session.exists(pathDocIncome)) {
                        session.mkdir(pathDocIncome);
                    }
                    if (typeDoc.equals(RECTOCIN)||typeDoc.equals(VERSOCIN) ||typeDoc.equals(FITCA)) {
                        session.write(file.getInputStream(), pathDoc + "/" + file.getOriginalFilename());
                        if (typeDoc.equals(RECTOCIN)) {
                            requiredDocDTO.setRectoCin(pathDoc + "/" + file.getOriginalFilename());
                        }else if (typeDoc.equals(VERSOCIN)) {
                            requiredDocDTO.setVersoCin(pathDoc + "/" + file.getOriginalFilename());
                        } else if (typeDoc.equals(FITCA)) {
                            requiredDocDTO.setFatca(pathDoc + "/" + file.getOriginalFilename());
                        }
                        requiredDocDTO.setLabel(file.getOriginalFilename());
                        this.save(requiredDocDTO);
                        requiredDocMapper.toDto(requiredDoc);
                    }
                    else
                    if (typeDoc.equals(NATIOALITE)){
                        session.write(file.getInputStream(), pathDocIncome + "/" + file.getOriginalFilename());
                        Long requiredDocId=requiredDoc.getId();
                        RequiredDocIncome requiredDocIncome = new RequiredDocIncome();
                        RequiredDocIncomeDTO requiredDocIncomeDTO=requiredDocIncomeMapper.toDto(requiredDocIncome);
                        requiredDocIncomeDTO.setRequiredDocId(requiredDocId);
                        requiredDocIncomeDTO.setPath(pathDocIncome + "/" + file.getOriginalFilename());
                        requiredDocIncomeService.save(requiredDocIncomeDTO);
                        requiredDocIncomeMapper.toDto(requiredDocIncome);
                    }
                    if (typeDoc.equals (RESIDENCYRECTO) ||typeDoc.equals(RESIDENCYVERSO)){
                        session.write(file.getInputStream(), pathDocResidency + "/" + file.getOriginalFilename());
                        Long requiredDocId=requiredDoc.getId();
                        RequiredDocResidency requiredDocResidency =requiredDocResidencyRepository.findByRequiredDocId(requiredDocId);
                        if (requiredDocResidency==null){
                            requiredDocResidency= new RequiredDocResidency();
                        }
                        RequiredDocResidencyDTO  requiredDocResidencyDTO=requiredDocResidencyMapper.toDto(requiredDocResidency);
                        requiredDocResidencyDTO.setRequiredDocId(requiredDocId);
                        if (typeDoc.equals (RESIDENCYRECTO)) {
                            requiredDocResidencyDTO.setResidencyRecto(pathDocResidency + "/" + file.getOriginalFilename());
                        }
                        else if (typeDoc.equals (RESIDENCYVERSO)){
                            requiredDocResidencyDTO.setResidencyVerso(pathDocResidency + "/" + file.getOriginalFilename());
                        }
                      //  requiredDocResidencyDTO.setResidencyDocumentId(residencyId);
                        requiredDocResidencyServiceImpl.save(requiredDocResidencyDTO);
                        requiredDocResidencyMapper.toDto(requiredDocResidency);
                    }
                    fileDataDao.setEnable(false);
                    fileDataDao.setNameFile(file.getOriginalFilename());
                    fileDataDao.setPath(file.getOriginalFilename());
                }
                catch(Exception e){
                    e.printStackTrace();
                }
                session.close();
            }
        }
        return fileDataDao;
    }
    @Override
    public FileDataDao uploadFileRevenu(MultipartFile file, Long requestId) throws IOException {
        FtpSession session = ftpSeurveur.gimmeFactory().getSession();
        String requestDocId=String.valueOf(requestId);
        String pathDocIncome=requestDocId+"/"+DOCINCOME;
        FileDataDao fileDataDao=new FileDataDao();
        if (session != null) {
            if (!file.isEmpty()) {
                RequiredDoc requiredDoc =  requiredDocRepository.findByRequestId(requestId);
                if(requiredDoc==null){
                    requiredDoc = new RequiredDoc();
                }
                RequiredDocDTO requiredDocDTO =requiredDocMapper.toDto(requiredDoc);
                requiredDocDTO.setRequestId(requestId);
                String dirName=Long.toString(requestId) ;
                try {
                    if (!session.exists(requestDocId)) {
                        session.mkdir(requestDocId);
                    }

                    if (!session.exists(pathDocIncome)) {
                        session.mkdir(pathDocIncome);
                    }
                    session.write(file.getInputStream(), pathDocIncome + "/" + file.getOriginalFilename());
                    Long requiredDocId = requiredDoc.getId();
                    fileDataDao.setEnable(false);
                    fileDataDao.setNameFile(file.getOriginalFilename());
                    fileDataDao.setPath(file.getOriginalFilename());
                    OtherRevenuFile otherRevenuFile = new OtherRevenuFile();
                    OtherRevenuFileDTO otherRevenuFileDTO = otherRevenuFileMapper.toDto(otherRevenuFile);
                    otherRevenuFileDTO.setRequiredDocId(requiredDocId);


                    otherRevenuFileDTO.setPath(pathDocIncome + "/" + file.getOriginalFilename());
                    otherRevenuFileDTO= otherRevenuFileService.save(otherRevenuFileDTO);
                    fileDataDao.setId(otherRevenuFileDTO.getId());
                    otherRevenuFileMapper.toDto(otherRevenuFile);
                }
                catch(Exception e){
                    e.printStackTrace();
                }
                session.close();
            }
        }

        return fileDataDao;
    }




    @Override
    public FileDataDao uploadFileResidency(MultipartFile file, Long requestId) throws IOException {
        FtpSession session = ftpSeurveur.gimmeFactory().getSession();
        String requestDocId=String.valueOf(requestId);
        String pathDocIncome=requestDocId+"/"+DOCINCOME;
        FileDataDao fileDataDao=new FileDataDao();
        if (session != null) {
            if (!file.isEmpty()) {
                RequiredDoc requiredDoc =  requiredDocRepository.findByRequestId(requestId);
                if(requiredDoc==null){
                    requiredDoc = new RequiredDoc();
                }
                RequiredDocDTO requiredDocDTO =requiredDocMapper.toDto(requiredDoc);
                requiredDocDTO.setRequestId(requestId);
                String dirName=Long.toString(requestId) ;
                try {
                    if (!session.exists(requestDocId)) {
                        session.mkdir(requestDocId);
                    }

                    if (!session.exists(pathDocIncome)) {
                        session.mkdir(pathDocIncome);
                    }
                    session.write(file.getInputStream(), pathDocIncome + "/" + file.getOriginalFilename());
                    Long requiredDocId = requiredDoc.getId();
                    fileDataDao.setEnable(false);
                    fileDataDao.setNameFile(file.getOriginalFilename());
                    OtherResidencyFile otherResidencyFile = new OtherResidencyFile();
                    OtherResidencyFileDTO otherResidencyFileDTO = otherResidencyFileMapper.toDto(otherResidencyFile);
                    otherResidencyFileDTO.setRequiredDocId(requiredDocId);

                    fileDataDao.setPath(file.getOriginalFilename());
                    otherResidencyFileDTO.setPath(pathDocIncome + "/" + file.getOriginalFilename());
                    otherResidencyFileDTO= otherResidencyFileService.save(otherResidencyFileDTO);
                    fileDataDao.setId(otherResidencyFileDTO.getId());
                    otherResidencyFileMapper.toDto(otherResidencyFile);
                }
                catch(Exception e){
                    e.printStackTrace();
                }
                session.close();
            }
        }

        return fileDataDao;
    }


//    @Override
//    public FileDataDao upload(MultipartFile file, Long requestId, String typeDoc) throws IOException {
//        FtpSession session = ftpSeurveur.gimmeFactory().getSession();
//        String requestDocId=String.valueOf(requestId);
//        String pathDoc=requestDocId+"/"+DOC;
//        String pathDocResidency=requestDocId+"/"+DOCRESIDENCY;
//        String pathDocIncome=requestDocId+"/"+DOCINCOME;
//        FileDataDao fileData=new FileDataDao();
//        if (session != null) {
//            if (!file.isEmpty()) {
//                RequiredDoc requiredDoc =  requiredDocRepository.findByRequestId(requestId);
//                if(requiredDoc==null){
//                    requiredDoc = new RequiredDoc();
//                }
//                RequiredDocDTO requiredDocDTO =requiredDocMapper.toDto(requiredDoc);
//                requiredDocDTO.setRequestId(requestId);
//                String dirName=Long.toString(requestId) ;
//
//
//                try {
//                    if (!session.exists(requestDocId)) {
//                        session.mkdir(requestDocId);
//                    }
//                    if (!session.exists(pathDoc)) {
//                        session.mkdir(pathDoc);
//                    }
//                    if (!session.exists(pathDocResidency)) {
//                        session.mkdir(pathDocResidency);
//                    }
//                    if (!session.exists(pathDocIncome)) {
//                        session.mkdir(pathDocIncome);
//                    }
//                    if (typeDoc.equals(RECTOCIN)||typeDoc.equals(VERSOCIN) ||typeDoc.equals(FITCA)) {
//                        session.write(file.getInputStream(), pathDoc + "/" + file.getOriginalFilename());
//                        if (typeDoc.equals(RECTOCIN)) {
//                            requiredDocDTO.setRectoCin(pathDoc + "/" + file.getOriginalFilename());
//                        }else if (typeDoc.equals(VERSOCIN)) {
//                            requiredDocDTO.setVersoCin(pathDoc + "/" + file.getOriginalFilename());
//                        } else if (typeDoc.equals(FITCA)) {
//                            requiredDocDTO.setFatca(pathDoc + "/" + file.getOriginalFilename());
//                        }
//                        fileData.setNameFile(file.getOriginalFilename());
//                        fileData.setEnable(false);
//                        requiredDocDTO.setLabel(file.getOriginalFilename());
//                        this.save(requiredDocDTO);
//                        requiredDocMapper.toDto(requiredDoc);
//                    }
//                    else if(typeDoc.equals(NATIOALITE)){
//                        session.write(file.getInputStream(), pathDocIncome + "/" + file.getOriginalFilename());
//                        Long requiredDocId=requiredDoc.getId();
//                        RequiredDocIncome requiredDocIncome = new RequiredDocIncome();
//                        RequiredDocIncomeDTO requiredDocIncomeDTO=requiredDocIncomeMapper.toDto(requiredDocIncome);
//                        fileData.setNameFile(file.getOriginalFilename());
//                        fileData.setEnable(false);
//                        requiredDocIncomeDTO.setRequiredDocId(requiredDocId);
//                        requiredDocIncomeDTO.setPath(pathDocIncome + "/" + file.getOriginalFilename());
//                        requiredDocIncomeService.save(requiredDocIncomeDTO);
//                        requiredDocIncomeMapper.toDto(requiredDocIncome);
//
//                    }
//                    if (typeDoc.equals (RESIDENCYRECTO) ||typeDoc.equals(RESIDENCYVERSO)){
//                        session.write(file.getInputStream(), pathDocResidency + "/" + file.getOriginalFilename());
//                        Long requiredDocId=requiredDoc.getId();
//                        RequiredDocResidency requiredDocResidency =requiredDocResidencyRepository.findByRequiredDocId(requiredDocId);
//                        if (requiredDocResidency==null){
//                            requiredDocResidency= new RequiredDocResidency();
//                        }
//                        RequiredDocResidencyDTO  requiredDocResidencyDTO=requiredDocResidencyMapper.toDto(requiredDocResidency);
//                        requiredDocResidencyDTO.setRequiredDocId(requiredDocId);
//                      //  requiredDocResidencyDTO.setResidencyDocumentId(residencyId);
//                        if (typeDoc.equals (RESIDENCYRECTO)) {
//                            requiredDocResidencyDTO.setResidencyRecto(pathDocResidency + "/" + file.getOriginalFilename());
//                        }
//                        else if (typeDoc.equals (RESIDENCYVERSO)){
//                            requiredDocResidencyDTO.setResidencyVerso(pathDocResidency + "/" + file.getOriginalFilename());
//                        }
//                        fileData.setNameFile(file.getOriginalFilename());
//                        fileData.setEnable(false);
//                    //    requiredDocResidencyDTO.setResidencyDocumentId(residencyId);
//                        requiredDocResidencyServiceImpl.save(requiredDocResidencyDTO);
//                        requiredDocResidencyMapper.toDto(requiredDocResidency);
//                    }
//                }
//                catch(Exception e){
//                    e.printStackTrace();
//                }
//                session.close();
//            }
//        }
//        return fileData;
//    }




    public void saveAllDataOfDoc (AllDataOfDocDto allDataOfDocDto, Long requestId) {
        try {
            RequiredDoc requiredDoc = requiredDocRepository.findByRequestId(requestId);
            Long requiredDocId = requiredDoc.getId();
            RequiredDocResidency requiredDocResidency = requiredDocResidencyRepository.findByRequiredDocId(requiredDocId);
            RequiredDocDTO requiredDocDTO = requiredDocMapper.toDto(requiredDoc);
            RequiredDocResidencyDTO requiredDocResidencyDTO = requiredDocResidencyMapper.toDto(requiredDocResidency);
            requiredDocDTO.setNumCIN(allDataOfDocDto.getNumCarteIdentite());
            requiredDocDTO.setDeliveryDateCin(allDataOfDocDto.getDateDelivranceCarteIdentite());
            requiredDocResidencyDTO.setDeliveryDate(allDataOfDocDto.getDeliveryDate());
            requiredDocResidencyDTO.setExperationDate(allDataOfDocDto.getExperationDate());
            requiredDocResidencyDTO.setIllimitedExperationDate(allDataOfDocDto.getIllimitedExperationDate());
            requiredDocResidencyDTO.setNum(allDataOfDocDto.getNumDocumentJustif());
            this.save(requiredDocDTO);
            requiredDocMapper.toDto(requiredDoc);
            requiredDocResidencyServiceImpl.save(requiredDocResidencyDTO);
            requiredDocResidencyMapper.toDto(requiredDocResidency);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    @Override
    public AllDataOfDocDto getDataJustif(Long requestId) {
        try {

            AllDataOfDocDto allDataOfDocDto = new AllDataOfDocDto();
            RequiredDoc requiredDoc = requiredDocRepository.findByRequestId(requestId);

            if(requiredDoc!=null) {
                Long requiredDocId = requiredDoc.getId();
                RequiredDocResidency requiredDocResidency = requiredDocResidencyRepository.findByRequiredDocId(requiredDocId);

                //  RequiredDocIncome  requiredDocIncome=requiredDocIncomeRepository.findByRequiredDocId(requiredDocId);
                allDataOfDocDto.setDateDelivranceCarteIdentite(requiredDoc.getDeliveryDateCin());
                allDataOfDocDto.setNumCarteIdentite(requiredDoc.getNumCIN());
                allDataOfDocDto.setRectoCin(requiredDoc.getRectoCin());
                allDataOfDocDto.setVersoCin(requiredDoc.getVersoCin());
                allDataOfDocDto.setFatca(requiredDoc.getFatca());
                allDataOfDocDto.setDeliveryDate(requiredDocResidency.getDeliveryDate());
                allDataOfDocDto.setResidencyVerso(requiredDocResidency.getResidencyVerso());
                allDataOfDocDto.setResidencyRecto(requiredDocResidency.getResidencyVerso());
                allDataOfDocDto.setExperationDate(requiredDocResidency.getExperationDate());
                allDataOfDocDto.setIllimitedExperationDate(requiredDocResidency.isIllimitedExperationDate());
                return allDataOfDocDto;
            }
            else {
                AllDataOfDocDto allDataOfDocDtoNull = new AllDataOfDocDto();
                return allDataOfDocDtoNull;
            }
        }
        catch (Exception e){
            AllDataOfDocDto allDataOfDocDto = new AllDataOfDocDto();
            return allDataOfDocDto;
        }


    }
    public byte[] downloadfile(Long requestId,String typeDoc, HttpServletResponse response) throws IOException {

        try {
            RequiredDoc requiredDoc = requiredDocRepository.findByRequestId(requestId);
            Long requiredDocId = requiredDoc.getId();
            RequiredDocResidency requiredDocResidency = requiredDocResidencyRepository.findByRequiredDocId(requiredDocId);
            RequiredDocIncome requiredDocIncome=requiredDocIncomeRepository.findByRequiredDocId(requiredDocId);
            byte[] blob = new byte[1024];

            if (typeDoc.equals(RECTOCIN)) {
                blob = dowanload.dowanloadFile(requiredDoc.getRectoCin(), response);

            }
            if (typeDoc.equals(VERSOCIN)) {
                blob = dowanload.dowanloadFile(requiredDoc.getVersoCin(), response);

            }
            if (typeDoc.equals(FITCA)) {
                blob = dowanload.dowanloadFile(requiredDoc.getFatca(), response);

            }
            if (typeDoc.equals(RESIDENCYRECTO)) {
                blob = dowanload.dowanloadFile(requiredDocResidency.getResidencyRecto(), response);
            }
            if (typeDoc.equals(RESIDENCYVERSO)) {
                blob = dowanload.dowanloadFile(requiredDocResidency.getResidencyVerso(), response);
            }

            if (typeDoc.equals(NATIOALITE)) {
                blob = dowanload.dowanloadFile(requiredDocIncome.getPath(), response);
            }
            return blob;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public byte[] downloadfileRevenu(Long id, HttpServletResponse response) throws IOException {
        try {
            byte[] blob = new byte[1024];
            Optional<OtherRevenuFile> otherRevenuFile=otherRevenuFileRepository.findById(id);
            blob = dowanload.dowanloadFile(otherRevenuFile.get().getPath(), response);
            return blob;
        }
        catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }

    @Override
    public byte[] downloadfileResidency(Long id, HttpServletResponse response) throws IOException {
        try {
            byte[] blob = new byte[1024];
            Optional<OtherResidencyFile> otherResidencyFile=otherResidencyFileRepository.findById(id);
            blob = dowanload.dowanloadFile(otherResidencyFile.get().getPath(), response);
            return blob;
        }
        catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }

    @Override
    public byte[] downloadfilefatca( HttpServletResponse response) throws IOException {
        try {
            byte[] blob = new byte[1024];

            blob = dowanload.dowanloadFatca(response);
            return blob;
        }
        catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }


    @Override
    public Optional<RequiredDocDTO> getRequiredDocByRequestId(Long requestId) {
        RequiredDoc requiredDoc = requiredDocRepository.findByRequestId(requestId);
        RequiredDocDTO requiredDocDTO = requiredDocMapper.toDto(requiredDoc);
        return Optional.ofNullable(requiredDocDTO);
    }

    @Override
    public RequiredDocDTO findByRequestId(Long requestId) {
        RequiredDocDTO requiredDocDTO = new RequiredDocDTO();
        for (RequiredDocDTO requiredDocDTO1 : findAll() ){
            if(requiredDocDTO1.getRequestId().equals(requestId)){
                requiredDocDTO = requiredDocDTO1;
            }
        }
        return requiredDocDTO;
    }
}

