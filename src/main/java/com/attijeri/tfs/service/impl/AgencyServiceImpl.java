package com.attijeri.tfs.service.impl;

import com.attijeri.tfs.service.AgencyService;
import com.attijeri.tfs.service.PersonalInfoService;
import com.attijeri.tfs.domain.Agency;
import com.attijeri.tfs.repository.AgencyRepository;
import com.attijeri.tfs.service.dto.AgencyDTO;
import com.attijeri.tfs.service.dto.PersonalInfoDTO;
import com.attijeri.tfs.service.mapper.AgencyMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Agency}.
 */
@Service
@Transactional
public class AgencyServiceImpl implements AgencyService {

    private final Logger log = LoggerFactory.getLogger(AgencyServiceImpl.class);

    private final AgencyRepository agencyRepository;

    private final AgencyMapper agencyMapper;

    private final PersonalInfoService personalInfoService;

    public AgencyServiceImpl(AgencyRepository agencyRepository, AgencyMapper agencyMapper,
                             PersonalInfoService personalInfoService) {
        this.agencyRepository = agencyRepository;
        this.agencyMapper = agencyMapper;
        this.personalInfoService = personalInfoService;
    }

    @Override
    public AgencyDTO save(AgencyDTO agencyDTO) {
        log.debug("Request to save Agency : {}", agencyDTO);
        Agency agency = agencyMapper.toEntity(agencyDTO);
        agency = agencyRepository.save(agency);

        Optional<PersonalInfoDTO> result = personalInfoService.findOne(agencyDTO.getPersonalInfoId());
        if (result.isPresent()) {
            PersonalInfoDTO personalInfoDTO = result.get();
            personalInfoDTO.setAgencyId(agency.getId());
            personalInfoService.update(personalInfoDTO);
        }
        return agencyMapper.toDto(agency);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AgencyDTO> findAll() {
        log.debug("Request to get all Agencies");
        return agencyRepository.findAll().stream()
            .map(agencyMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<AgencyDTO> findOne(Long id) {
        log.debug("Request to get Agency : {}", id);
        return agencyRepository.findById(id)
            .map(agencyMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Agency : {}", id);
        agencyRepository.deleteById(id);
    }
}
