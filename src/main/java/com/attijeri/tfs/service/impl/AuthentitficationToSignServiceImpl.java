package com.attijeri.tfs.service.impl;

import com.attijeri.tfs.service.AuthentitficationToSignService;
import com.attijeri.tfs.domain.AuthentitficationToSign;
import com.attijeri.tfs.repository.AuthentitficationToSignRepository;
import com.attijeri.tfs.service.dto.AuthentitficationToSignDTO;
import com.attijeri.tfs.service.mapper.AuthentitficationToSignMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link AuthentitficationToSign}.
 */
@Service
@Transactional
public class AuthentitficationToSignServiceImpl implements AuthentitficationToSignService {

    private final Logger log = LoggerFactory.getLogger(AuthentitficationToSignServiceImpl.class);

    private final AuthentitficationToSignRepository authentitficationToSignRepository;

    private final AuthentitficationToSignMapper authentitficationToSignMapper;

    public AuthentitficationToSignServiceImpl(AuthentitficationToSignRepository authentitficationToSignRepository, AuthentitficationToSignMapper authentitficationToSignMapper) {
        this.authentitficationToSignRepository = authentitficationToSignRepository;
        this.authentitficationToSignMapper = authentitficationToSignMapper;
    }

    @Override
    public AuthentitficationToSignDTO save(AuthentitficationToSignDTO authentitficationToSignDTO) {
        log.debug("Request to save AuthentitficationToSign : {}", authentitficationToSignDTO);
        AuthentitficationToSign authentitficationToSign = authentitficationToSignMapper.toEntity(authentitficationToSignDTO);
        authentitficationToSign = authentitficationToSignRepository.save(authentitficationToSign);
        return authentitficationToSignMapper.toDto(authentitficationToSign);
    }

    @Override
    @Transactional(readOnly = true)
    public List<AuthentitficationToSignDTO> findAll() {
        log.debug("Request to get all AuthentitficationToSigns");
        return authentitficationToSignRepository.findAll().stream()
            .map(authentitficationToSignMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<AuthentitficationToSignDTO> findOne(Long id) {
        log.debug("Request to get AuthentitficationToSign : {}", id);
        return authentitficationToSignRepository.findById(id)
            .map(authentitficationToSignMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete AuthentitficationToSign : {}", id);
        authentitficationToSignRepository.deleteById(id);
    }
}
