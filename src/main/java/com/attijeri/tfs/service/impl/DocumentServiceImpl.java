package com.attijeri.tfs.service.impl;

import com.attijeri.tfs.service.DocumentService;
import com.attijeri.tfs.domain.Document;
import com.attijeri.tfs.repository.DocumentRepository;
import com.attijeri.tfs.service.dto.DocumentDTO;
import com.attijeri.tfs.service.dto.PersonalInfoDTO;
import com.attijeri.tfs.service.mapper.DocumentMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Service Implementation for managing {@link Document}.
 */
@Service
@Transactional
public class DocumentServiceImpl implements DocumentService {

    private final Logger log = LoggerFactory.getLogger(DocumentServiceImpl.class);

    private final DocumentRepository documentRepository;

    private final DocumentMapper documentMapper;

    public DocumentServiceImpl(DocumentRepository documentRepository, DocumentMapper documentMapper) {
        this.documentRepository = documentRepository;
        this.documentMapper = documentMapper;
    }

    @Override
    public DocumentDTO save(DocumentDTO documentDTO) {
        log.debug("Request to save Document : {}", documentDTO);
        Document document = documentMapper.toEntity(documentDTO);
        document = documentRepository.save(document);
        return documentMapper.toDto(document);
    }

    @Override
    @Transactional(readOnly = true)
    public List<DocumentDTO> findAll() {
        log.debug("Request to get all Documents");
        return documentRepository.findAll().stream()
            .map(documentMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }


    @Override
    @Transactional(readOnly = true)
    public Optional<DocumentDTO> findOne(Long id) {
        log.debug("Request to get Document : {}", id);
        return documentRepository.findById(id)
            .map(documentMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Document : {}", id);
        documentRepository.deleteById(id);
    }

    @Override
    public DocumentDTO findDocumentByRequestId(Long requestId) {
        DocumentDTO documentDTO1 = new DocumentDTO();
        for (DocumentDTO documentDTO : findAll() ){
            if(documentDTO.getRequestId().equals(requestId)){
                documentDTO1 = documentDTO;
            }
        }
        return documentDTO1;
    }

    @Override
    public DocumentDTO findDocumentByDossierId(String DossierId) {
        DocumentDTO documentDTO1 = new DocumentDTO();
        for (DocumentDTO documentDTO : findAll() ){
            if(documentDTO.getIdDossierSigned().equals(DossierId)){
                documentDTO1 = documentDTO;
            }
        }
        System.out.println(documentDTO1);
        return documentDTO1;
    }
}
