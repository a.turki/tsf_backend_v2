package com.attijeri.tfs.service.impl;

import com.attijeri.tfs.service.MonthlyNetIncomeService;
import com.attijeri.tfs.domain.MonthlyNetIncome;
import com.attijeri.tfs.repository.MonthlyNetIncomeRepository;
import com.attijeri.tfs.service.dto.MonthlyNetIncomeDTO;
import com.attijeri.tfs.service.mapper.MonthlyNetIncomeMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * Service Implementation for managing {@link MonthlyNetIncome}.
 */
@Service
@Transactional
public class MonthlyNetIncomeServiceImpl implements MonthlyNetIncomeService {

    private final Logger log = LoggerFactory.getLogger(MonthlyNetIncomeServiceImpl.class);

    private final MonthlyNetIncomeRepository monthlyNetIncomeRepository;

    private final MonthlyNetIncomeMapper monthlyNetIncomeMapper;

    public MonthlyNetIncomeServiceImpl(MonthlyNetIncomeRepository monthlyNetIncomeRepository, MonthlyNetIncomeMapper monthlyNetIncomeMapper) {
        this.monthlyNetIncomeRepository = monthlyNetIncomeRepository;
        this.monthlyNetIncomeMapper = monthlyNetIncomeMapper;
    }

    @Override
    public MonthlyNetIncomeDTO save(MonthlyNetIncomeDTO monthlyNetIncomeDTO) {
        log.debug("Request to save MonthlyNetIncome : {}", monthlyNetIncomeDTO);
        MonthlyNetIncome monthlyNetIncome = monthlyNetIncomeMapper.toEntity(monthlyNetIncomeDTO);
        monthlyNetIncome = monthlyNetIncomeRepository.save(monthlyNetIncome);
        return monthlyNetIncomeMapper.toDto(monthlyNetIncome);
    }

    @Override
    @Transactional(readOnly = true)
    public List<MonthlyNetIncomeDTO> findAll() {
        log.debug("Request to get all MonthlyNetIncomes");
        return monthlyNetIncomeRepository.findAll().stream()
            .map(monthlyNetIncomeMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }



    /**
     *  Get all the monthlyNetIncomes where FinancialInfo is {@code null}.
     *  @return the list of entities.
     */
    @Transactional(readOnly = true) 
    public List<MonthlyNetIncomeDTO> findAllWhereFinancialInfoIsNull() {
        log.debug("Request to get all monthlyNetIncomes where FinancialInfo is null");
        return StreamSupport
            .stream(monthlyNetIncomeRepository.findAll().spliterator(), false)
            .filter(monthlyNetIncome -> monthlyNetIncome.getFinancialInfo() == null)
            .map(monthlyNetIncomeMapper::toDto)
            .collect(Collectors.toCollection(LinkedList::new));
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<MonthlyNetIncomeDTO> findOne(Long id) {
        log.debug("Request to get MonthlyNetIncome : {}", id);
        return monthlyNetIncomeRepository.findById(id)
            .map(monthlyNetIncomeMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete MonthlyNetIncome : {}", id);
        monthlyNetIncomeRepository.deleteById(id);
    }
}
