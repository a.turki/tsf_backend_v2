package com.attijeri.tfs.service;

import com.attijeri.tfs.domain.ReqDocUpload;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link ReqDocUpload}.
 */
public interface ReqDocUploadService {

    /**
     * Save a reqDocUpload.
     *
     * @param reqDocUpload the entity to save.
     * @return the persisted entity.
     */
    ReqDocUpload save(ReqDocUpload reqDocUpload);

    /**
     * Get all the reqDocUploads.
     *
     * @return the list of entities.
     */
    List<ReqDocUpload> findAll();


    /**
     * Get the "id" reqDocUpload.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ReqDocUpload> findOne(Long id);

    /**
     * Delete the "id" reqDocUpload.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    ReqDocUpload uploadFile(MultipartFile file, Long requestId, String typeDoc) throws Exception;

    boolean deleteFile(Long reqDocId) throws Exception;

    }
