package com.attijeri.tfs.service;

import com.attijeri.tfs.service.dto.FinancialInfoDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.attijeri.tfs.domain.FinancialInfo}.
 */
public interface FinancialInfoService {

    /**
     * Save a financialInfo.
     *
     * @param financialInfoDTO the entity to save.
     * @return the persisted entity.
     */
    FinancialInfoDTO save(FinancialInfoDTO financialInfoDTO);

    /**
     * Get all the financialInfos.
     *
     * @return the list of entities.
     */
    List<FinancialInfoDTO> findAll();


    /**
     * Get the "id" financialInfo.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<FinancialInfoDTO> findOne(Long id);

    /**
     * Delete the "id" financialInfo.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    FinancialInfoDTO findFinancialInfo(Long personalId);
}
