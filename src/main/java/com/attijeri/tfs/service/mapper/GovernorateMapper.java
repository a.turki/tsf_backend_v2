package com.attijeri.tfs.service.mapper;


import com.attijeri.tfs.domain.*;
import com.attijeri.tfs.service.dto.GovernorateDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link Governorate} and its DTO {@link GovernorateDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface GovernorateMapper extends EntityMapper<GovernorateDTO, Governorate> {


    @Mapping(target = "municipalities", ignore = true)
    @Mapping(target = "removeMunicipality", ignore = true)
    Governorate toEntity(GovernorateDTO governorateDTO);

    default Governorate fromId(Long id) {
        if (id == null) {
            return null;
        }
        Governorate governorate = new Governorate();
        governorate.setId(id);
        return governorate;
    }
}
