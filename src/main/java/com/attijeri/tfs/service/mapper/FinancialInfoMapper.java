package com.attijeri.tfs.service.mapper;


import com.attijeri.tfs.domain.*;
import com.attijeri.tfs.service.dto.FinancialInfoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link FinancialInfo} and its DTO {@link FinancialInfoDTO}.
 */
@Mapper(componentModel = "spring", uses = {ActivityMapper.class, CategoryMapper.class, PersonalInfoMapper.class, MonthlyNetIncomeMapper.class})
public interface FinancialInfoMapper extends EntityMapper<FinancialInfoDTO, FinancialInfo> {

    @Mapping(source = "activity.id", target = "activityId")
    @Mapping(source = "category.id", target = "categoryId")
    @Mapping(source = "personalInfo.id", target = "personalInfoId")
    @Mapping(source = "monthlyNetIncome.id", target = "monthlyNetIncomeId")
    FinancialInfoDTO toDto(FinancialInfo financialInfo);

    @Mapping(source = "activityId", target = "activity")
    @Mapping(source = "categoryId", target = "category")
    @Mapping(source = "personalInfoId", target = "personalInfo")
    @Mapping(source = "monthlyNetIncomeId", target = "monthlyNetIncome")
    FinancialInfo toEntity(FinancialInfoDTO financialInfoDTO);

    default FinancialInfo fromId(Long id) {
        if (id == null) {
            return null;
        }
        FinancialInfo financialInfo = new FinancialInfo();
        financialInfo.setId(id);
        return financialInfo;
    }
}
