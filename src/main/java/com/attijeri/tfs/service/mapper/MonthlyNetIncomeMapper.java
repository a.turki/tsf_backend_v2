package com.attijeri.tfs.service.mapper;


import com.attijeri.tfs.domain.*;
import com.attijeri.tfs.service.dto.MonthlyNetIncomeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link MonthlyNetIncome} and its DTO {@link MonthlyNetIncomeDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface MonthlyNetIncomeMapper extends EntityMapper<MonthlyNetIncomeDTO, MonthlyNetIncome> {


    @Mapping(target = "financialInfo", ignore = true)
    MonthlyNetIncome toEntity(MonthlyNetIncomeDTO monthlyNetIncomeDTO);

    default MonthlyNetIncome fromId(Long id) {
        if (id == null) {
            return null;
        }
        MonthlyNetIncome monthlyNetIncome = new MonthlyNetIncome();
        monthlyNetIncome.setId(id);
        return monthlyNetIncome;
    }
}
