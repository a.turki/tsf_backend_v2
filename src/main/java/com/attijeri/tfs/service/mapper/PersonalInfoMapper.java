package com.attijeri.tfs.service.mapper;


import com.attijeri.tfs.domain.*;
import com.attijeri.tfs.service.dto.PersonalInfoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link PersonalInfo} and its DTO {@link PersonalInfoDTO}.
 */
@Mapper(componentModel = "spring", uses = {RequestMapper.class, AgencyMapper.class})
public interface PersonalInfoMapper extends EntityMapper<PersonalInfoDTO, PersonalInfo> {

    @Mapping(source = "request.id", target = "requestId")
    @Mapping(source = "agency.id", target = "agencyId")
    PersonalInfoDTO toDto(PersonalInfo personalInfo);

    @Mapping(source = "requestId", target = "request")
    @Mapping(target = "adressInfo", ignore = true)
    @Mapping(source = "agencyId", target = "agency")
    @Mapping(target = "financialInfo", ignore = true)
    PersonalInfo toEntity(PersonalInfoDTO personalInfoDTO);

    default PersonalInfo fromId(Long id) {
        if (id == null) {
            return null;
        }
        PersonalInfo personalInfo = new PersonalInfo();
        personalInfo.setId(id);
        return personalInfo;
    }
}
