package com.attijeri.tfs.service.mapper;


import com.attijeri.tfs.domain.*;
import com.attijeri.tfs.service.dto.ResidencyDocumentDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link ResidencyDocument} and its DTO {@link ResidencyDocumentDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface ResidencyDocumentMapper extends EntityMapper<ResidencyDocumentDTO, ResidencyDocument> {


    @Mapping(target = "requiredDocResidency", ignore = true)
    @Mapping(target = "otherResidencyFiles", ignore = true)
    @Mapping(target = "removeOtherResidencyFile", ignore = true)
    ResidencyDocument toEntity(ResidencyDocumentDTO residencyDocumentDTO);

    default ResidencyDocument fromId(Long id) {
        if (id == null) {
            return null;
        }
        ResidencyDocument residencyDocument = new ResidencyDocument();
        residencyDocument.setId(id);
        return residencyDocument;
    }
}
