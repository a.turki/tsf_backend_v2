package com.attijeri.tfs.service.mapper;


import com.attijeri.tfs.domain.*;
import com.attijeri.tfs.service.dto.RequiredDocResidencyDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link RequiredDocResidency} and its DTO {@link RequiredDocResidencyDTO}.
 */
@Mapper(componentModel = "spring", uses = {RequiredDocMapper.class, ResidencyDocumentMapper.class})
public interface RequiredDocResidencyMapper extends EntityMapper<RequiredDocResidencyDTO, RequiredDocResidency> {

    @Mapping(source = "requiredDoc.id", target = "requiredDocId")
    @Mapping(source = "residencyDocument.id", target = "residencyDocumentId")
    RequiredDocResidencyDTO toDto(RequiredDocResidency requiredDocResidency);

    @Mapping(source = "requiredDocId", target = "requiredDoc")
    @Mapping(source = "residencyDocumentId", target = "residencyDocument")
    RequiredDocResidency toEntity(RequiredDocResidencyDTO requiredDocResidencyDTO);

    default RequiredDocResidency fromId(Long id) {
        if (id == null) {
            return null;
        }
        RequiredDocResidency requiredDocResidency = new RequiredDocResidency();
        requiredDocResidency.setId(id);
        return requiredDocResidency;
    }
}
