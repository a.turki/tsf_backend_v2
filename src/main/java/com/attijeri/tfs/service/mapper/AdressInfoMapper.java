package com.attijeri.tfs.service.mapper;


import com.attijeri.tfs.domain.*;
import com.attijeri.tfs.service.dto.AdressInfoDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link AdressInfo} and its DTO {@link AdressInfoDTO}.
 */
@Mapper(componentModel = "spring", uses = {PersonalInfoMapper.class})
public interface AdressInfoMapper extends EntityMapper<AdressInfoDTO, AdressInfo> {

    @Mapping(source = "personalInfo.id", target = "personalInfoId")
    AdressInfoDTO toDto(AdressInfo adressInfo);

    @Mapping(source = "personalInfoId", target = "personalInfo")
    AdressInfo toEntity(AdressInfoDTO adressInfoDTO);

    default AdressInfo fromId(Long id) {
        if (id == null) {
            return null;
        }
        AdressInfo adressInfo = new AdressInfo();
        adressInfo.setId(id);
        return adressInfo;
    }
}
