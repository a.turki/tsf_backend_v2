package com.attijeri.tfs.service.mapper;


import com.attijeri.tfs.domain.*;
import com.attijeri.tfs.service.dto.AuthentitficationToSignDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link AuthentitficationToSign} and its DTO {@link AuthentitficationToSignDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface AuthentitficationToSignMapper extends EntityMapper<AuthentitficationToSignDTO, AuthentitficationToSign> {



    default AuthentitficationToSign fromId(Long id) {
        if (id == null) {
            return null;
        }
        AuthentitficationToSign authentitficationToSign = new AuthentitficationToSign();
        authentitficationToSign.setId(id);
        return authentitficationToSign;
    }
}
