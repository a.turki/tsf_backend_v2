package com.attijeri.tfs.service.mapper;


import com.attijeri.tfs.domain.*;
import com.attijeri.tfs.service.dto.OtherResidencyFileDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link OtherResidencyFile} and its DTO {@link OtherResidencyFileDTO}.
 */
@Mapper(componentModel = "spring", uses = {RequiredDocMapper.class, ResidencyDocumentMapper.class})
public interface OtherResidencyFileMapper extends EntityMapper<OtherResidencyFileDTO, OtherResidencyFile> {

    @Mapping(source = "requiredDoc.id", target = "requiredDocId")
    @Mapping(source = "residencyDocument.id", target = "residencyDocumentId")
    OtherResidencyFileDTO toDto(OtherResidencyFile otherResidencyFile);

    @Mapping(source = "requiredDocId", target = "requiredDoc")
    @Mapping(source = "residencyDocumentId", target = "residencyDocument")
    OtherResidencyFile toEntity(OtherResidencyFileDTO otherResidencyFileDTO);

    default OtherResidencyFile fromId(Long id) {
        if (id == null) {
            return null;
        }
        OtherResidencyFile otherResidencyFile = new OtherResidencyFile();
        otherResidencyFile.setId(id);
        return otherResidencyFile;
    }
}
