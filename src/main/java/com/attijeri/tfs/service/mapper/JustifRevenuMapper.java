package com.attijeri.tfs.service.mapper;


import com.attijeri.tfs.domain.*;
import com.attijeri.tfs.service.dto.JustifRevenuDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link JustifRevenu} and its DTO {@link JustifRevenuDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface JustifRevenuMapper extends EntityMapper<JustifRevenuDTO, JustifRevenu> {


    @Mapping(target = "otherRevenuFiles", ignore = true)
    @Mapping(target = "removeOtherRevenuFile", ignore = true)
    JustifRevenu toEntity(JustifRevenuDTO justifRevenuDTO);

    default JustifRevenu fromId(Long id) {
        if (id == null) {
            return null;
        }
        JustifRevenu justifRevenu = new JustifRevenu();
        justifRevenu.setId(id);
        return justifRevenu;
    }
}
