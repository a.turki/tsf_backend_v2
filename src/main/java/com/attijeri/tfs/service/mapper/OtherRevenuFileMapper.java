package com.attijeri.tfs.service.mapper;


import com.attijeri.tfs.domain.*;
import com.attijeri.tfs.service.dto.OtherRevenuFileDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link OtherRevenuFile} and its DTO {@link OtherRevenuFileDTO}.
 */
@Mapper(componentModel = "spring", uses = {RequiredDocMapper.class, JustifRevenuMapper.class})
public interface OtherRevenuFileMapper extends EntityMapper<OtherRevenuFileDTO, OtherRevenuFile> {

    @Mapping(source = "requiredDoc.id", target = "requiredDocId")
    @Mapping(source = "justifRevenu.id", target = "justifRevenuId")
    OtherRevenuFileDTO toDto(OtherRevenuFile otherRevenuFile);

    @Mapping(source = "requiredDocId", target = "requiredDoc")
    @Mapping(source = "justifRevenuId", target = "justifRevenu")
    OtherRevenuFile toEntity(OtherRevenuFileDTO otherRevenuFileDTO);

    default OtherRevenuFile fromId(Long id) {
        if (id == null) {
            return null;
        }
        OtherRevenuFile otherRevenuFile = new OtherRevenuFile();
        otherRevenuFile.setId(id);
        return otherRevenuFile;
    }
}
