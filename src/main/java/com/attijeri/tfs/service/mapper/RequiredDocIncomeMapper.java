package com.attijeri.tfs.service.mapper;


import com.attijeri.tfs.domain.*;
import com.attijeri.tfs.service.dto.RequiredDocIncomeDTO;

import org.mapstruct.*;

/**
 * Mapper for the entity {@link RequiredDocIncome} and its DTO {@link RequiredDocIncomeDTO}.
 */
@Mapper(componentModel = "spring", uses = {RequiredDocMapper.class})
public interface RequiredDocIncomeMapper extends EntityMapper<RequiredDocIncomeDTO, RequiredDocIncome> {

    @Mapping(source = "requiredDoc.id", target = "requiredDocId")
    RequiredDocIncomeDTO toDto(RequiredDocIncome requiredDocIncome);

    @Mapping(source = "requiredDocId", target = "requiredDoc")
    RequiredDocIncome toEntity(RequiredDocIncomeDTO requiredDocIncomeDTO);

    default RequiredDocIncome fromId(Long id) {
        if (id == null) {
            return null;
        }
        RequiredDocIncome requiredDocIncome = new RequiredDocIncome();
        requiredDocIncome.setId(id);
        return requiredDocIncome;
    }
}
