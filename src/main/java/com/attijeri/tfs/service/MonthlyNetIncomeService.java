package com.attijeri.tfs.service;

import com.attijeri.tfs.service.dto.MonthlyNetIncomeDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.attijeri.tfs.domain.MonthlyNetIncome}.
 */
public interface MonthlyNetIncomeService {

    /**
     * Save a monthlyNetIncome.
     *
     * @param monthlyNetIncomeDTO the entity to save.
     * @return the persisted entity.
     */
    MonthlyNetIncomeDTO save(MonthlyNetIncomeDTO monthlyNetIncomeDTO);

    /**
     * Get all the monthlyNetIncomes.
     *
     * @return the list of entities.
     */
    List<MonthlyNetIncomeDTO> findAll();
    /**
     * Get all the MonthlyNetIncomeDTO where FinancialInfo is {@code null}.
     *
     * @return the {@link List} of entities.
     */
    List<MonthlyNetIncomeDTO> findAllWhereFinancialInfoIsNull();


    /**
     * Get the "id" monthlyNetIncome.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<MonthlyNetIncomeDTO> findOne(Long id);

    /**
     * Delete the "id" monthlyNetIncome.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
