package com.attijeri.tfs.service;

import com.attijeri.tfs.service.dto.PersonalInfoDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.attijeri.tfs.domain.PersonalInfo}.
 */
public interface PersonalInfoService {

    /**
     * Save a personalInfo.
     *
     * @param personalInfoDTO the entity to save.
     * @return the persisted entity.
     */
    PersonalInfoDTO save(PersonalInfoDTO personalInfoDTO);

    /**
     * Get all the personalInfos.
     *
     * @return the list of entities.
     */
    List<PersonalInfoDTO> findAll();
    /**
     * Get all the PersonalInfoDTO where AdressInfo is {@code null}.
     *
     * @return the {@link List} of entities.
     */
    List<PersonalInfoDTO> findAllWhereAdressInfoIsNull();
    /**
     * Get all the PersonalInfoDTO where FinancialInfo is {@code null}.
     *
     * @return the {@link List} of entities.
     */
    List<PersonalInfoDTO> findAllWhereFinancialInfoIsNull();


    /**
     * Get the "id" personalInfo.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<PersonalInfoDTO> findOne(Long id);

    /**
     * Delete the "id" personalInfo.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    PersonalInfoDTO update(PersonalInfoDTO PersonalInfo);


    public boolean validEmail(String email);

    public PersonalInfoDTO findByRequestId (Long requestId);
}
