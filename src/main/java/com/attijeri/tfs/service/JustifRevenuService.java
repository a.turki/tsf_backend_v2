package com.attijeri.tfs.service;

import com.attijeri.tfs.service.dto.JustifRevenuDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.attijeri.tfs.domain.JustifRevenu}.
 */
public interface JustifRevenuService {

    /**
     * Save a justifRevenu.
     *
     * @param justifRevenuDTO the entity to save.
     * @return the persisted entity.
     */
    JustifRevenuDTO save(JustifRevenuDTO justifRevenuDTO);

    /**
     * Get all the justifRevenus.
     *
     * @return the list of entities.
     */
    List<JustifRevenuDTO> findAll();


    /**
     * Get the "id" justifRevenu.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<JustifRevenuDTO> findOne(Long id);

    /**
     * Delete the "id" justifRevenu.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
