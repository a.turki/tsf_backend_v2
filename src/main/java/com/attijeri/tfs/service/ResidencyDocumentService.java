package com.attijeri.tfs.service;

import com.attijeri.tfs.service.dto.ResidencyDocumentDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.attijeri.tfs.domain.ResidencyDocument}.
 */
public interface ResidencyDocumentService {

    /**
     * Save a residencyDocument.
     *
     * @param residencyDocumentDTO the entity to save.
     * @return the persisted entity.
     */
    ResidencyDocumentDTO save(ResidencyDocumentDTO residencyDocumentDTO);

    /**
     * Get all the residencyDocuments.
     *
     * @return the list of entities.
     */
    List<ResidencyDocumentDTO> findAll();


    /**
     * Get the "id" residencyDocument.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<ResidencyDocumentDTO> findOne(Long id);

    /**
     * Delete the "id" residencyDocument.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
