package com.attijeri.tfs.service;

import com.attijeri.tfs.service.dto.AdressInfoDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.attijeri.tfs.domain.AdressInfo}.
 */
public interface AdressInfoService {

    /**
     * Save a adressInfo.
     *
     * @param adressInfoDTO the entity to save.
     * @return the persisted entity.
     */
    AdressInfoDTO save(AdressInfoDTO adressInfoDTO);

    /**
     * Get all the adressInfos.
     *
     * @return the list of entities.
     */
    List<AdressInfoDTO> findAll();


    /**
     * Get the "id" adressInfo.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AdressInfoDTO> findOne(Long id);

    /**
     * Delete the "id" adressInfo.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

    AdressInfoDTO findAddress(Long personalId);
}
