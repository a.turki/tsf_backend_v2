package com.attijeri.tfs.service;

import com.attijeri.tfs.service.dto.DetailOfferDTO;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.attijeri.tfs.domain.DetailOffer}.
 */
public interface DetailOfferService {

    /**
     * Save a detailOffer.
     *
     * @param detailOfferDTO the entity to save.
     * @return the persisted entity.
     */
    DetailOfferDTO save(DetailOfferDTO detailOfferDTO);

    /**
     * Get all the detailOffers.
     *
     * @return the list of entities.
     */
    List<DetailOfferDTO> findAll();


    /**
     * Get the "id" detailOffer.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<DetailOfferDTO> findOne(Long id);

    /**
     * Delete the "id" detailOffer.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
