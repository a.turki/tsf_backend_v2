package com.attijeri.tfs.service;

import com.attijeri.tfs.service.dto.OtherRevenuFileDTO;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.attijeri.tfs.domain.OtherRevenuFile}.
 */
public interface OtherRevenuFileService {

    /**
     * Save a otherRevenuFile.
     *
     * @param otherRevenuFileDTO the entity to save.
     * @return the persisted entity.
     */
    OtherRevenuFileDTO save(OtherRevenuFileDTO otherRevenuFileDTO);

    /**
     * Get all the otherRevenuFiles.
     *
     * @return the list of entities.
     */
    List<OtherRevenuFileDTO> findAll();


    /**
     * Get the "id" otherRevenuFile.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OtherRevenuFileDTO> findOne(Long id);

    /**
     * Delete the "id" otherRevenuFile.
     *
     * @param id the id of the entity.
     */
    void delete(Long id) throws IOException;
    List<OtherRevenuFileDTO> getAllRevenuSameRequestId(Long id);
}
