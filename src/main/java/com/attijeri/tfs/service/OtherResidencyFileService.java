package com.attijeri.tfs.service;

import com.attijeri.tfs.service.dto.OtherResidencyFileDTO;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.attijeri.tfs.domain.OtherResidencyFile}.
 */
public interface OtherResidencyFileService {

    /**
     * Save a otherResidencyFile.
     *
     * @param otherResidencyFileDTO the entity to save.
     * @return the persisted entity.
     */
    OtherResidencyFileDTO save(OtherResidencyFileDTO otherResidencyFileDTO);

    /**
     * Get all the otherResidencyFiles.
     *
     * @return the list of entities.
     */
    List<OtherResidencyFileDTO> findAll();


    /**
     * Get the "id" otherResidencyFile.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<OtherResidencyFileDTO> findOne(Long id);

    /**
     * Delete the "id" otherResidencyFile.
     *
     * @param id the id of the entity.
     */
    void delete(Long id) throws IOException;
    List<OtherResidencyFileDTO> getAllResidencySameRequestId(Long id);
}
