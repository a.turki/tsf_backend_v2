package com.attijeri.tfs.service;


import com.attijeri.tfs.service.dto.AllDataOfDocDto;
import com.attijeri.tfs.service.dto.FileDataDao;
import com.attijeri.tfs.service.dto.RequiredDocDTO;
import io.github.jhipster.web.util.ResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing {@link com.attijeri.tfs.domain.RequiredDoc}.
 */
public interface RequiredDocService {

    /**
     * Save a requiredDoc.
     *
     * @param requiredDocDTO the entity to save.
     * @return the persisted entity.
     */
    RequiredDocDTO save(RequiredDocDTO requiredDocDTO);

    /**
     * Get all the requiredDocs.
     *
     * @return the list of entities.
     */
    List<RequiredDocDTO> findAll();


    /**
     * Get the "id" requiredDoc.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RequiredDocDTO> findOne(Long id);

    /**
     * Delete the "id" requiredDoc.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);

//    public FileDataDao upload(MultipartFile file, Long requestId, String typeDoc)throws IOException;
    public FileDataDao uploadFile( MultipartFile file,Long requestId,String typeDoc)throws IOException;
    public FileDataDao uploadFileRevenu( MultipartFile file,Long requestId)throws IOException;
    public FileDataDao uploadFileResidency( MultipartFile file,Long requestId)throws IOException;
    public void saveAllDataOfDoc (AllDataOfDocDto allDataOfDocDto, Long requestId);
    public String deleteImportantDoc(Long requestId, String typeDoc);
    public byte[] downloadfile(Long requestId,String typeDoc, HttpServletResponse response) throws IOException;
    public byte[] downloadfileRevenu(Long requestId, HttpServletResponse response) throws IOException;
    public byte[] downloadfileResidency(Long requestId, HttpServletResponse response) throws IOException;
    public byte[] downloadfilefatca(HttpServletResponse response) throws IOException;
    public Optional<RequiredDocDTO> getRequiredDocByRequestId( Long id);
    public AllDataOfDocDto getDataJustif(Long id);
    RequiredDocDTO findByRequestId(Long requestId);
}
