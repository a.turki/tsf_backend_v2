package com.attijeri.tfs.config.optConfig;

import com.attijeri.tfs.domain.Request;
import com.attijeri.tfs.service.dto.PersonalInfoDTO;
import org.springframework.http.ResponseEntity;

public interface MiddleWareService {

    ResponseEntity<Response> sendEmail(String to, String cc, String cci, String subject, String content);

    ResponseEntity<Response> sendConfirmationEmail(PersonalInfoDTO personalInfoDTO,String subject,String language);

    ResponseEntity<Response> sendRequestRegistrationMail(PersonalInfoDTO personalInfoDTO,String subject,Long idRequest);

    ResponseEntity<Response> followRequest(PersonalInfoDTO personalInfoDTO,String subject,String verificationCode);

}
