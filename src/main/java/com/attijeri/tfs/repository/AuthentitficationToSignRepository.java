package com.attijeri.tfs.repository;

import com.attijeri.tfs.domain.AuthentitficationToSign;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Optional;

/**
 * Spring Data  repository for the AuthentitficationToSign entity.
 */
@SuppressWarnings("unused")
@Repository
public interface AuthentitficationToSignRepository extends JpaRepository<AuthentitficationToSign, Long> {
    Optional<AuthentitficationToSign>  findByEmail(String email);
}
