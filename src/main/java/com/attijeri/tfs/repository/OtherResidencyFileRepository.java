package com.attijeri.tfs.repository;

import com.attijeri.tfs.domain.OtherResidencyFile;

import com.attijeri.tfs.domain.OtherRevenuFile;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the OtherResidencyFile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OtherResidencyFileRepository extends JpaRepository<OtherResidencyFile, Long> {
    @Query(value = "SELECT * FROM OTHER_RES_FILE WHERE  REQUIRED_DOC_ID = :requiredDocId",nativeQuery = true)
    OtherResidencyFile findByRequestId(@Param("requiredDocId") Long requiredDocId);
    @Query(value = "SELECT * FROM OTHER_RES_FILE WHERE  REQUIRED_DOC_ID = :requiredDocId",nativeQuery = true)
    List<OtherResidencyFile> findAllRevenuByRequestId(@Param("requiredDocId") Long requiredDocId);
}
