package com.attijeri.tfs.repository;

import com.attijeri.tfs.domain.JustifRevenu;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the JustifRevenu entity.
 */
@SuppressWarnings("unused")
@Repository
public interface JustifRevenuRepository extends JpaRepository<JustifRevenu, Long> {
}
