package com.attijeri.tfs.repository;

import com.attijeri.tfs.domain.RequiredDocIncome;
import com.attijeri.tfs.domain.RequiredDocResidency;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the RequiredDocResidency entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RequiredDocResidencyRepository extends JpaRepository<RequiredDocResidency, Long> {
    @Query(value = "SELECT * FROM REQ_RESIDENCY WHERE REQUIRED_DOC_ID = :requiredDocId",nativeQuery = true)
    RequiredDocResidency findByRequiredDocId(@Param("requiredDocId") Long requiredDocId);
}
