package com.attijeri.tfs.repository;

import com.attijeri.tfs.domain.RequiredDoc;
import com.attijeri.tfs.domain.RequiredDocIncome;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the RequiredDocIncome entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RequiredDocIncomeRepository extends JpaRepository<RequiredDocIncome, Long> {
    @Query(value = "SELECT * FROM REQ_DOC_INCOME WHERE REQUIRED_DOC_ID = :requiredDocId",nativeQuery = true)
    RequiredDocIncome findByRequiredDocId(@Param("requiredDocId") Long requiredDocId);

}
