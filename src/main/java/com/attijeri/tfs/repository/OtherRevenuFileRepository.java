package com.attijeri.tfs.repository;

import com.attijeri.tfs.domain.OtherRevenuFile;

import com.attijeri.tfs.domain.RequiredDoc;
import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Spring Data  repository for the OtherRevenuFile entity.
 */
@SuppressWarnings("unused")
@Repository
public interface OtherRevenuFileRepository extends JpaRepository<OtherRevenuFile, Long> {
    @Query(value = "SELECT * FROM OTHER_REV_FILE WHERE REQUIRED_DOC_ID = :requiredDocId",nativeQuery = true)
    OtherRevenuFile findByRequiredDocId(@Param("requiredDocId") Long requiredDocId);
    @Query(value = "SELECT * FROM OTHER_REV_FILE WHERE REQUIRED_DOC_ID = :requiredDocId",nativeQuery = true)
    List<OtherRevenuFile> findAllRevenuByRequestId(@Param("requiredDocId") Long requiredDocId);
}

