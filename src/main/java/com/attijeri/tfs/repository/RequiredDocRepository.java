package com.attijeri.tfs.repository;

import com.attijeri.tfs.domain.RequiredDoc;

import org.springframework.data.jpa.repository.*;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the RequiredDoc entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RequiredDocRepository extends JpaRepository<RequiredDoc, Long> {
    @Query(value = "SELECT * FROM REQUIRED_DOC WHERE REQUEST_ID = :requestId",nativeQuery = true)
    RequiredDoc findByRequestId(@Param("requestId") Long requestId);
}
