package com.attijeri.tfs.repository;

import com.attijeri.tfs.domain.ReqDocUpload;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ReqDocUpload entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ReqDocUploadRepository extends JpaRepository<ReqDocUpload, Long> {
}
