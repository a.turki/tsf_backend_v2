package com.attijeri.tfs.repository;

import com.attijeri.tfs.domain.ResidencyDocument;

import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data  repository for the ResidencyDocument entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ResidencyDocumentRepository extends JpaRepository<ResidencyDocument, Long> {
}
