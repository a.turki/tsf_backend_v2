package com.attijeri.tfs.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.micrometer.core.lang.Nullable;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A Request.
 */
@Entity
@Table(name = "request")
public class Request implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "visio_date")
    private LocalDate visioDate;

    @Column(name = "sending_mail_date")
    private LocalDate sendingMailDate;

    @Column(name = "state")
    private Boolean state;

    @Column(name = "step")
    private String step;

    @Column(name = "code_verification")
    private String codeVerification;

    @ManyToOne
    @JsonIgnoreProperties(value = "requests", allowSetters = true)
    private Offer offer;

    @OneToOne(mappedBy = "request")
    @JsonIgnore
    private PersonalInfo personalInfo;

    @OneToOne(mappedBy = "request")
    @JsonIgnore
    private RequiredDoc requiredDoc;

    @ManyToMany
    @JoinTable(name = "request_bank_account",
               joinColumns = @JoinColumn(name = "request_id", referencedColumnName = "id"),
               inverseJoinColumns = @JoinColumn(name = "bank_account_id", referencedColumnName = "id"))
    private Set<BankAccount> bankAccounts = new HashSet<>();
    @Nullable
    @OneToOne(mappedBy = "request")
    @JsonIgnore
    private Document document;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LocalDate getVisioDate() {
        return visioDate;
    }

    public Request visioDate(LocalDate visioDate) {
        this.visioDate = visioDate;
        return this;
    }

    public void setVisioDate(LocalDate visioDate) {
        this.visioDate = visioDate;
    }

    public LocalDate getSendingMailDate() {
        return sendingMailDate;
    }

    public Request sendingMailDate(LocalDate sendingMailDate) {
        this.sendingMailDate = sendingMailDate;
        return this;
    }

    public void setSendingMailDate(LocalDate sendingMailDate) {
        this.sendingMailDate = sendingMailDate;
    }

    public Boolean isState() {
        return state;
    }

    public Request state(Boolean state) {
        this.state = state;
        return this;
    }

    public void setState(Boolean state) {
        this.state = state;
    }

    public String getStep() {
        return step;
    }

    public Request step(String step) {
        this.step = step;
        return this;
    }

    public void setStep(String step) {
        this.step = step;
    }

    public String getCodeVerification() {
        return codeVerification;
    }

    public Request codeVerification(String codeVerification) {
        this.codeVerification = codeVerification;
        return this;
    }

    public void setCodeVerification(String codeVerification) {
        this.codeVerification = codeVerification;
    }

    public Offer getOffer() {
        return offer;
    }

    public Request offer(Offer offer) {
        this.offer = offer;
        return this;
    }

    public void setOffer(Offer offer) {
        this.offer = offer;
    }

    public PersonalInfo getPersonalInfo() {
        return personalInfo;
    }

    public Request personalInfo(PersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
        return this;
    }

    public void setPersonalInfo(PersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
    }

    public RequiredDoc getRequiredDoc() {
        return requiredDoc;
    }

    public Request requiredDoc(RequiredDoc requiredDoc) {
        this.requiredDoc = requiredDoc;
        return this;
    }

    public void setRequiredDoc(RequiredDoc requiredDoc) {
        this.requiredDoc = requiredDoc;
    }

    public Set<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    public Request bankAccounts(Set<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
        return this;
    }

    public Request addBankAccount(BankAccount bankAccount) {
        this.bankAccounts.add(bankAccount);
        bankAccount.getRequests().add(this);
        return this;
    }

    public Request removeBankAccount(BankAccount bankAccount) {
        this.bankAccounts.remove(bankAccount);
        bankAccount.getRequests().remove(this);
        return this;
    }

    public void setBankAccounts(Set<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    public Document getDocument() {
        return document;
    }

    public Request document(Document document) {
        this.document = document;
        return this;
    }

    public void setDocument(Document document) {
        this.document = document;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Request)) {
            return false;
        }
        return id != null && id.equals(((Request) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Request{" +
            "id=" + getId() +
            ", visioDate='" + getVisioDate() + "'" +
            ", sendingMailDate='" + getSendingMailDate() + "'" +
            ", state='" + isState() + "'" +
            ", step='" + getStep() + "'" +
            ", codeVerification='" + getCodeVerification() + "'" +
            "}";
    }
}
