package com.attijeri.tfs.domain.enumeration;

/**
 * The Civility enumeration.
 */
public enum Civility {
    MADAME, MONSIEUR
}
