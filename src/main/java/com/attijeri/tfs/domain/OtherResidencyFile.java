package com.attijeri.tfs.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A OtherResidencyFile.
 */
@Entity
@Table(name = "other_res_file")
public class OtherResidencyFile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "path")
    private String path;

    @ManyToOne
    @JsonIgnoreProperties(value = "otherResidencyFiles", allowSetters = true)
    private RequiredDoc requiredDoc;

    @ManyToOne
    @JsonIgnoreProperties(value = "otherResidencyFiles", allowSetters = true)
    private ResidencyDocument residencyDocument;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public OtherResidencyFile path(String path) {
        this.path = path;
        return this;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public RequiredDoc getRequiredDoc() {
        return requiredDoc;
    }

    public OtherResidencyFile requiredDoc(RequiredDoc requiredDoc) {
        this.requiredDoc = requiredDoc;
        return this;
    }

    public void setRequiredDoc(RequiredDoc requiredDoc) {
        this.requiredDoc = requiredDoc;
    }

    public ResidencyDocument getResidencyDocument() {
        return residencyDocument;
    }

    public OtherResidencyFile residencyDocument(ResidencyDocument residencyDocument) {
        this.residencyDocument = residencyDocument;
        return this;
    }

    public void setResidencyDocument(ResidencyDocument residencyDocument) {
        this.residencyDocument = residencyDocument;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OtherResidencyFile)) {
            return false;
        }
        return id != null && id.equals(((OtherResidencyFile) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OtherResidencyFile{" +
            "id=" + getId() +
            ", path='" + getPath() + "'" +
            "}";
    }
}
