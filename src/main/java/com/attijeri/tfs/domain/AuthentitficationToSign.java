package com.attijeri.tfs.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

/**
 * A AuthentitficationToSign.
 */
@Entity
@Table(name = "authentitfication_to_sign")
public class AuthentitficationToSign implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "email")
    private String email;

    @Column(name = "token")
    private String token;

    @Column(name = "date_creation")
    private Date dateCreation;

    @Column(name = "valide")
    private Boolean valide;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public AuthentitficationToSign email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getToken() {
        return token;
    }

    public AuthentitficationToSign token(String token) {
        this.token = token;
        return this;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public Boolean getValide() {
        return valide;
    }

    public Boolean isValide() {
        return valide;
    }

    public AuthentitficationToSign valide(Boolean valide) {
        this.valide = valide;
        return this;
    }

    public void setValide(Boolean valide) {
        this.valide = valide;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AuthentitficationToSign)) {
            return false;
        }
        return id != null && id.equals(((AuthentitficationToSign) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AuthentitficationToSign{" +
            "id=" + getId() +
            ", email='" + getEmail() + "'" +
            ", token='" + getToken() + "'" +
            ", dateCreation='" + getDateCreation() + "'" +
            ", valide='" + isValide() + "'" +
            "}";
    }
}
