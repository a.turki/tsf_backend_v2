package com.attijeri.tfs.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDate;

/**
 * A ReqDocUpload.
 */
@Entity
@Table(name = "req_doc_upload")
public class ReqDocUpload implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "path_doc")
    private String pathDoc;

    @Column(name = "type_doc")
    private String typeDoc;



    @Column(name = "updated_at")
    private Instant updatedAt;

    @ManyToOne
    @JsonIgnoreProperties(value = "reqDocUploads", allowSetters = true)
    private Request request;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPathDoc() {
        return pathDoc;
    }

    public ReqDocUpload pathDoc(String pathDoc) {
        this.pathDoc = pathDoc;
        return this;
    }

    public void setPathDoc(String pathDoc) {
        this.pathDoc = pathDoc;
    }

    public String getTypeDoc() {
        return typeDoc;
    }

    public ReqDocUpload typeDoc(String typeDoc) {
        this.typeDoc = typeDoc;
        return this;
    }

    public void setTypeDoc(String typeDoc) {
        this.typeDoc = typeDoc;
    }



    public Instant getUpdatedAt() {
        return updatedAt;
    }

    public ReqDocUpload updatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
        return this;
    }

    public void setUpdatedAt(Instant updatedAt) {
        this.updatedAt = updatedAt;
    }

    public Request getRequest() {
        return request;
    }

    public ReqDocUpload request(Request request) {
        this.request = request;
        return this;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ReqDocUpload)) {
            return false;
        }
        return id != null && id.equals(((ReqDocUpload) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ReqDocUpload{" +
            "id=" + getId() +
            ", pathDoc='" + getPathDoc() + "'" +
            ", typeDoc='" + getTypeDoc() + "'" +
            ", updatedAt='" + getUpdatedAt() + "'" +
            "}";
    }
}
