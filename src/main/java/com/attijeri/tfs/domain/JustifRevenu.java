package com.attijeri.tfs.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A JustifRevenu.
 */
@Entity
@Table(name = "justif_revenu")
public class JustifRevenu implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "label_en")
    private String labelEN;

    @Column(name = "label_fr")
    private String labelFR;

    @OneToMany(mappedBy = "justifRevenu")
    private Set<OtherRevenuFile> otherRevenuFiles = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabelEN() {
        return labelEN;
    }

    public JustifRevenu labelEN(String labelEN) {
        this.labelEN = labelEN;
        return this;
    }

    public void setLabelEN(String labelEN) {
        this.labelEN = labelEN;
    }

    public String getLabelFR() {
        return labelFR;
    }

    public JustifRevenu labelFR(String labelFR) {
        this.labelFR = labelFR;
        return this;
    }

    public void setLabelFR(String labelFR) {
        this.labelFR = labelFR;
    }

    public Set<OtherRevenuFile> getOtherRevenuFiles() {
        return otherRevenuFiles;
    }

    public JustifRevenu otherRevenuFiles(Set<OtherRevenuFile> otherRevenuFiles) {
        this.otherRevenuFiles = otherRevenuFiles;
        return this;
    }

    public JustifRevenu addOtherRevenuFile(OtherRevenuFile otherRevenuFile) {
        this.otherRevenuFiles.add(otherRevenuFile);
        otherRevenuFile.setJustifRevenu(this);
        return this;
    }

    public JustifRevenu removeOtherRevenuFile(OtherRevenuFile otherRevenuFile) {
        this.otherRevenuFiles.remove(otherRevenuFile);
        otherRevenuFile.setJustifRevenu(null);
        return this;
    }

    public void setOtherRevenuFiles(Set<OtherRevenuFile> otherRevenuFiles) {
        this.otherRevenuFiles = otherRevenuFiles;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof JustifRevenu)) {
            return false;
        }
        return id != null && id.equals(((JustifRevenu) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "JustifRevenu{" +
            "id=" + getId() +
            ", labelEN='" + getLabelEN() + "'" +
            ", labelFR='" + getLabelFR() + "'" +
            "}";
    }
}
