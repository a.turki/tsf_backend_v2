package com.attijeri.tfs.domain;


import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * A RequiredDoc.
 */
@Entity
@Table(name = "required_doc")
public class RequiredDoc implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "jhi_label")
    private String label;

    @Column(name = "jhi_type")
    private String type;

    @Column(name = "num_cin")
    private String numCIN;

    @Column(name = "delivery_date_cin")
    private LocalDate deliveryDateCin;

    @Column(name = "recto_cin")
    private String rectoCin;

    @Column(name = "verso_cin")
    private String versoCin;

    @Column(name = "fatca")
    private String fatca;

    @OneToOne
    @JoinColumn(unique = true)
    private Request request;

    @OneToMany(mappedBy = "requiredDoc")
    private Set<RequiredDocIncome> requiredDocIncomes = new HashSet<>();

    @OneToMany(mappedBy = "requiredDoc")
    private Set<RequiredDocResidency> requiredDocResidencies = new HashSet<>();

    @OneToMany(mappedBy = "requiredDoc")
    private Set<OtherResidencyFile> otherResidencyFiles = new HashSet<>();

    @OneToMany(mappedBy = "requiredDoc")
    private Set<OtherRevenuFile> otherRevenuFiles = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public RequiredDoc label(String label) {
        this.label = label;
        return this;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getType() {
        return type;
    }

    public RequiredDoc type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumCIN() {
        return numCIN;
    }

    public RequiredDoc numCIN(String numCIN) {
        this.numCIN = numCIN;
        return this;
    }

    public void setNumCIN(String numCIN) {
        this.numCIN = numCIN;
    }

    public LocalDate getDeliveryDateCin() {
        return deliveryDateCin;
    }

    public RequiredDoc deliveryDateCin(LocalDate deliveryDateCin) {
        this.deliveryDateCin = deliveryDateCin;
        return this;
    }

    public void setDeliveryDateCin(LocalDate deliveryDateCin) {
        this.deliveryDateCin = deliveryDateCin;
    }

    public String getRectoCin() {
        return rectoCin;
    }

    public RequiredDoc rectoCin(String rectoCin) {
        this.rectoCin = rectoCin;
        return this;
    }

    public void setRectoCin(String rectoCin) {
        this.rectoCin = rectoCin;
    }

    public String getVersoCin() {
        return versoCin;
    }

    public RequiredDoc versoCin(String versoCin) {
        this.versoCin = versoCin;
        return this;
    }

    public void setVersoCin(String versoCin) {
        this.versoCin = versoCin;
    }

    public String getFatca() {
        return fatca;
    }

    public RequiredDoc fatca(String fatca) {
        this.fatca = fatca;
        return this;
    }

    public void setFatca(String fatca) {
        this.fatca = fatca;
    }

    public Request getRequest() {
        return request;
    }

    public RequiredDoc request(Request request) {
        this.request = request;
        return this;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public Set<RequiredDocIncome> getRequiredDocIncomes() {
        return requiredDocIncomes;
    }

    public RequiredDoc requiredDocIncomes(Set<RequiredDocIncome> requiredDocIncomes) {
        this.requiredDocIncomes = requiredDocIncomes;
        return this;
    }

    public RequiredDoc addRequiredDocIncome(RequiredDocIncome requiredDocIncome) {
        this.requiredDocIncomes.add(requiredDocIncome);
        requiredDocIncome.setRequiredDoc(this);
        return this;
    }

    public RequiredDoc removeRequiredDocIncome(RequiredDocIncome requiredDocIncome) {
        this.requiredDocIncomes.remove(requiredDocIncome);
        requiredDocIncome.setRequiredDoc(null);
        return this;
    }

    public void setRequiredDocIncomes(Set<RequiredDocIncome> requiredDocIncomes) {
        this.requiredDocIncomes = requiredDocIncomes;
    }

    public Set<RequiredDocResidency> getRequiredDocResidencies() {
        return requiredDocResidencies;
    }

    public RequiredDoc requiredDocResidencies(Set<RequiredDocResidency> requiredDocResidencies) {
        this.requiredDocResidencies = requiredDocResidencies;
        return this;
    }

    public RequiredDoc addRequiredDocResidency(RequiredDocResidency requiredDocResidency) {
        this.requiredDocResidencies.add(requiredDocResidency);
        requiredDocResidency.setRequiredDoc(this);
        return this;
    }

    public RequiredDoc removeRequiredDocResidency(RequiredDocResidency requiredDocResidency) {
        this.requiredDocResidencies.remove(requiredDocResidency);
        requiredDocResidency.setRequiredDoc(null);
        return this;
    }

    public void setRequiredDocResidencies(Set<RequiredDocResidency> requiredDocResidencies) {
        this.requiredDocResidencies = requiredDocResidencies;
    }

    public Set<OtherResidencyFile> getOtherResidencyFiles() {
        return otherResidencyFiles;
    }

    public RequiredDoc otherResidencyFiles(Set<OtherResidencyFile> otherResidencyFiles) {
        this.otherResidencyFiles = otherResidencyFiles;
        return this;
    }

    public RequiredDoc addOtherResidencyFile(OtherResidencyFile otherResidencyFile) {
        this.otherResidencyFiles.add(otherResidencyFile);
        otherResidencyFile.setRequiredDoc(this);
        return this;
    }

    public RequiredDoc removeOtherResidencyFile(OtherResidencyFile otherResidencyFile) {
        this.otherResidencyFiles.remove(otherResidencyFile);
        otherResidencyFile.setRequiredDoc(null);
        return this;
    }

    public void setOtherResidencyFiles(Set<OtherResidencyFile> otherResidencyFiles) {
        this.otherResidencyFiles = otherResidencyFiles;
    }

    public Set<OtherRevenuFile> getOtherRevenuFiles() {
        return otherRevenuFiles;
    }

    public RequiredDoc otherRevenuFiles(Set<OtherRevenuFile> otherRevenuFiles) {
        this.otherRevenuFiles = otherRevenuFiles;
        return this;
    }

    public RequiredDoc addOtherRevenuFile(OtherRevenuFile otherRevenuFile) {
        this.otherRevenuFiles.add(otherRevenuFile);
        otherRevenuFile.setRequiredDoc(this);
        return this;
    }

    public RequiredDoc removeOtherRevenuFile(OtherRevenuFile otherRevenuFile) {
        this.otherRevenuFiles.remove(otherRevenuFile);
        otherRevenuFile.setRequiredDoc(null);
        return this;
    }

    public void setOtherRevenuFiles(Set<OtherRevenuFile> otherRevenuFiles) {
        this.otherRevenuFiles = otherRevenuFiles;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequiredDoc)) {
            return false;
        }
        return id != null && id.equals(((RequiredDoc) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RequiredDoc{" +
            "id=" + getId() +
            ", label='" + getLabel() + "'" +
            ", type='" + getType() + "'" +
            ", numCIN='" + getNumCIN() + "'" +
            ", deliveryDateCin='" + getDeliveryDateCin() + "'" +
            ", rectoCin='" + getRectoCin() + "'" +
            ", versoCin='" + getVersoCin() + "'" +
            ", fatca='" + getFatca() + "'" +
            "}";
    }
}
