package com.attijeri.tfs.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A RequiredDocIncome.
 */
@Entity
@Table(name = "req_doc_income")
public class RequiredDocIncome implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "jhi_type")
    private String type;

    @Column(name = "path")
    private String path;

    @ManyToOne
    @JsonIgnoreProperties(value = "requiredDocIncomes", allowSetters = true)
    private RequiredDoc requiredDoc;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public RequiredDocIncome type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public RequiredDocIncome path(String path) {
        this.path = path;
        return this;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public RequiredDoc getRequiredDoc() {
        return requiredDoc;
    }

    public RequiredDocIncome requiredDoc(RequiredDoc requiredDoc) {
        this.requiredDoc = requiredDoc;
        return this;
    }

    public void setRequiredDoc(RequiredDoc requiredDoc) {
        this.requiredDoc = requiredDoc;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequiredDocIncome)) {
            return false;
        }
        return id != null && id.equals(((RequiredDocIncome) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RequiredDocIncome{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            ", path='" + getPath() + "'" +
            "}";
    }
}
