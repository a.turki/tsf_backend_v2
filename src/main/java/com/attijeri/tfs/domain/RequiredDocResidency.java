package com.attijeri.tfs.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * A RequiredDocResidency.
 */
@Entity
@Table(name = "req_residency")
public class RequiredDocResidency implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "jhi_type")
    private String type;

    @Column(name = "num")
    private String num;

    @Column(name = "delivery_date")
    private LocalDate deliveryDate;

    @Column(name = "experation_date")
    private LocalDate experationDate;

    @Column(name = "illimited_experation_date")
    private Boolean illimitedExperationDate;

    @Column(name = "residency_recto")
    private String residencyRecto;

    @Column(name = "residency_verso")
    private String residencyVerso;

    @ManyToOne
    @JsonIgnoreProperties(value = "requiredDocResidencies", allowSetters = true)
    private RequiredDoc requiredDoc;

    @OneToOne
    @JoinColumn(unique = true)
    private ResidencyDocument residencyDocument;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public RequiredDocResidency type(String type) {
        this.type = type;
        return this;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNum() {
        return num;
    }

    public RequiredDocResidency num(String num) {
        this.num = num;
        return this;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public LocalDate getDeliveryDate() {
        return deliveryDate;
    }

    public RequiredDocResidency deliveryDate(LocalDate deliveryDate) {
        this.deliveryDate = deliveryDate;
        return this;
    }

    public void setDeliveryDate(LocalDate deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public LocalDate getExperationDate() {
        return experationDate;
    }

    public RequiredDocResidency experationDate(LocalDate experationDate) {
        this.experationDate = experationDate;
        return this;
    }

    public void setExperationDate(LocalDate experationDate) {
        this.experationDate = experationDate;
    }

    public Boolean isIllimitedExperationDate() {
        return illimitedExperationDate;
    }

    public RequiredDocResidency illimitedExperationDate(Boolean illimitedExperationDate) {
        this.illimitedExperationDate = illimitedExperationDate;
        return this;
    }

    public void setIllimitedExperationDate(Boolean illimitedExperationDate) {
        this.illimitedExperationDate = illimitedExperationDate;
    }

    public String getResidencyRecto() {
        return residencyRecto;
    }

    public RequiredDocResidency residencyRecto(String residencyRecto) {
        this.residencyRecto = residencyRecto;
        return this;
    }

    public void setResidencyRecto(String residencyRecto) {
        this.residencyRecto = residencyRecto;
    }

    public String getResidencyVerso() {
        return residencyVerso;
    }

    public RequiredDocResidency residencyVerso(String residencyVerso) {
        this.residencyVerso = residencyVerso;
        return this;
    }

    public void setResidencyVerso(String residencyVerso) {
        this.residencyVerso = residencyVerso;
    }

    public RequiredDoc getRequiredDoc() {
        return requiredDoc;
    }

    public RequiredDocResidency requiredDoc(RequiredDoc requiredDoc) {
        this.requiredDoc = requiredDoc;
        return this;
    }

    public void setRequiredDoc(RequiredDoc requiredDoc) {
        this.requiredDoc = requiredDoc;
    }

    public ResidencyDocument getResidencyDocument() {
        return residencyDocument;
    }

    public RequiredDocResidency residencyDocument(ResidencyDocument residencyDocument) {
        this.residencyDocument = residencyDocument;
        return this;
    }

    public void setResidencyDocument(ResidencyDocument residencyDocument) {
        this.residencyDocument = residencyDocument;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RequiredDocResidency)) {
            return false;
        }
        return id != null && id.equals(((RequiredDocResidency) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RequiredDocResidency{" +
            "id=" + getId() +
            ", type='" + getType() + "'" +
            ", num='" + getNum() + "'" +
            ", deliveryDate='" + getDeliveryDate() + "'" +
            ", experationDate='" + getExperationDate() + "'" +
            ", illimitedExperationDate='" + isIllimitedExperationDate() + "'" +
            ", residencyRecto='" + getResidencyRecto() + "'" +
            ", residencyVerso='" + getResidencyVerso() + "'" +
            "}";
    }
}
