package com.attijeri.tfs.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A ResidencyDocument.
 */
@Entity
@Table(name = "residency_document")
public class ResidencyDocument implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "label_fr")
    private String labelFR;

    @Column(name = "label_en")
    private String labelEN;

    @OneToOne(mappedBy = "residencyDocument")
    @JsonIgnore
    private RequiredDocResidency requiredDocResidency;

    @OneToMany(mappedBy = "residencyDocument")
    private Set<OtherResidencyFile> otherResidencyFiles = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLabelFR() {
        return labelFR;
    }

    public ResidencyDocument labelFR(String labelFR) {
        this.labelFR = labelFR;
        return this;
    }

    public void setLabelFR(String labelFR) {
        this.labelFR = labelFR;
    }

    public String getLabelEN() {
        return labelEN;
    }

    public ResidencyDocument labelEN(String labelEN) {
        this.labelEN = labelEN;
        return this;
    }

    public void setLabelEN(String labelEN) {
        this.labelEN = labelEN;
    }

    public RequiredDocResidency getRequiredDocResidency() {
        return requiredDocResidency;
    }

    public ResidencyDocument requiredDocResidency(RequiredDocResidency requiredDocResidency) {
        this.requiredDocResidency = requiredDocResidency;
        return this;
    }

    public void setRequiredDocResidency(RequiredDocResidency requiredDocResidency) {
        this.requiredDocResidency = requiredDocResidency;
    }

    public Set<OtherResidencyFile> getOtherResidencyFiles() {
        return otherResidencyFiles;
    }

    public ResidencyDocument otherResidencyFiles(Set<OtherResidencyFile> otherResidencyFiles) {
        this.otherResidencyFiles = otherResidencyFiles;
        return this;
    }

    public ResidencyDocument addOtherResidencyFile(OtherResidencyFile otherResidencyFile) {
        this.otherResidencyFiles.add(otherResidencyFile);
        otherResidencyFile.setResidencyDocument(this);
        return this;
    }

    public ResidencyDocument removeOtherResidencyFile(OtherResidencyFile otherResidencyFile) {
        this.otherResidencyFiles.remove(otherResidencyFile);
        otherResidencyFile.setResidencyDocument(null);
        return this;
    }

    public void setOtherResidencyFiles(Set<OtherResidencyFile> otherResidencyFiles) {
        this.otherResidencyFiles = otherResidencyFiles;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof ResidencyDocument)) {
            return false;
        }
        return id != null && id.equals(((ResidencyDocument) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "ResidencyDocument{" +
            "id=" + getId() +
            ", labelFR='" + getLabelFR() + "'" +
            ", labelEN='" + getLabelEN() + "'" +
            "}";
    }
}
