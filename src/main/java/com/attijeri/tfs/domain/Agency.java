package com.attijeri.tfs.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.annotation.Nullable;
import javax.persistence.*;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * A Agency.
 */
@Entity
@Table(name = "agency")
public class Agency implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;
    @Nullable
    @Column(name = "name")
    private String name;
    @Nullable
    @Column(name = "address")
    private String address;
    @Nullable
    @Column(name = "zip")
    private Integer zip;
    @Nullable
    @Column(name = "city")
    private String city;
    @Nullable
    @ManyToOne
    @JsonIgnoreProperties(value = "agencies", allowSetters = true)
    private Municipality municipality;
    @Nullable
    @OneToMany(mappedBy = "agency")
    private Set<PersonalInfo> personalInfos = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public Agency name(String name) {
        this.name = name;
        return this;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public Agency address(String address) {
        this.address = address;
        return this;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getZip() {
        return zip;
    }

    public Agency zip(Integer zip) {
        this.zip = zip;
        return this;
    }

    public void setZip(Integer zip) {
        this.zip = zip;
    }

    public String getCity() {
        return city;
    }

    public Agency city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Municipality getMunicipality() {
        return municipality;
    }

    public Agency municipality(Municipality municipality) {
        this.municipality = municipality;
        return this;
    }

    public void setMunicipality(Municipality municipality) {
        this.municipality = municipality;
    }

    public Set<PersonalInfo> getPersonalInfos() {
        return personalInfos;
    }

    public Agency personalInfos(Set<PersonalInfo> personalInfos) {
        this.personalInfos = personalInfos;
        return this;
    }

    public Agency addPersonalInfo(PersonalInfo personalInfo) {
        this.personalInfos.add(personalInfo);
        personalInfo.setAgency(this);
        return this;
    }

    public Agency removePersonalInfo(PersonalInfo personalInfo) {
        this.personalInfos.remove(personalInfo);
        personalInfo.setAgency(null);
        return this;
    }

    public void setPersonalInfos(Set<PersonalInfo> personalInfos) {
        this.personalInfos = personalInfos;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Agency)) {
            return false;
        }
        return id != null && id.equals(((Agency) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Agency{" +
            "id=" + getId() +
            ", name='" + getName() + "'" +
            ", address='" + getAddress() + "'" +
            ", zip=" + getZip() +
            ", city='" + getCity() + "'" +
            "}";
    }
}
