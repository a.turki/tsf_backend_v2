package com.attijeri.tfs.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;

/**
 * A OtherRevenuFile.
 */
@Entity
@Table(name = "other_rev_file")
public class OtherRevenuFile implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "path")
    private String path;

    @ManyToOne
    @JsonIgnoreProperties(value = "otherRevenuFiles", allowSetters = true)
    private RequiredDoc requiredDoc;

    @ManyToOne
    @JsonIgnoreProperties(value = "otherRevenuFiles", allowSetters = true)
    private JustifRevenu justifRevenu;

    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getPath() {
        return path;
    }

    public OtherRevenuFile path(String path) {
        this.path = path;
        return this;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public RequiredDoc getRequiredDoc() {
        return requiredDoc;
    }

    public OtherRevenuFile requiredDoc(RequiredDoc requiredDoc) {
        this.requiredDoc = requiredDoc;
        return this;
    }

    public void setRequiredDoc(RequiredDoc requiredDoc) {
        this.requiredDoc = requiredDoc;
    }

    public JustifRevenu getJustifRevenu() {
        return justifRevenu;
    }

    public OtherRevenuFile justifRevenu(JustifRevenu justifRevenu) {
        this.justifRevenu = justifRevenu;
        return this;
    }

    public void setJustifRevenu(JustifRevenu justifRevenu) {
        this.justifRevenu = justifRevenu;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof OtherRevenuFile)) {
            return false;
        }
        return id != null && id.equals(((OtherRevenuFile) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "OtherRevenuFile{" +
            "id=" + getId() +
            ", path='" + getPath() + "'" +
            "}";
    }
}
