package com.attijeri.tfs.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;

import java.io.Serializable;
import java.time.LocalDate;

import com.attijeri.tfs.domain.enumeration.Civility;
import org.springframework.lang.Nullable;

/**
 * A PersonalInfo.
 */
@Entity
@Table(name = "personal_info")
public class PersonalInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;
    @Nullable
    @Enumerated(EnumType.STRING)
    @Column(name = "civility")
    private Civility civility;
    @Nullable
    @Column(name = "first_name")
    private String firstName;
    @Nullable
    @Column(name = "last_name")
    private String lastName;
    @Nullable
    @Column(name = "email")
    private String email;
    @Nullable
    @Column(name = "native_country")
    private String nativeCountry;
    @Nullable
    @Column(name = "birthday")
    private LocalDate birthday;
    @Nullable
    @Column(name = "client_abt")
    private Boolean clientABT;
    @Nullable
    @Column(name = "rib")
    private String rib;
    @Nullable
    @Column(name = "nationality")
    private String nationality;
    @Nullable
    @Column(name = "second_nationality")
    private String secondNationality;
    @Nullable
    @Column(name = "nbrkids")
    private Integer nbrkids;
    @Nullable
    @Column(name = "marital_status")
    private String maritalStatus;
    @Nullable
    @Column(name = "phone")
    private String phone;
    @Nullable
    @Column(name = "american_index")
    private Boolean americanIndex;
    @Nullable
    @OneToOne
    @JoinColumn(unique = true)
    private Request request;
    @Nullable
    @OneToOne(mappedBy = "personalInfo")
    @JsonIgnore
    private AdressInfo adressInfo;
    @Nullable
    @ManyToOne
    @JsonIgnoreProperties(value = "personalInfos", allowSetters = true)
    private Agency agency;
    @Nullable
    @OneToOne(mappedBy = "personalInfo")
    @JsonIgnore
    private FinancialInfo financialInfo;


    // jhipster-needle-entity-add-field - JHipster will add fields here
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Civility getCivility() {
        return civility;
    }

    public PersonalInfo civility(Civility civility) {
        this.civility = civility;
        return this;
    }

    public void setCivility(Civility civility) {
        this.civility = civility;
    }

    public String getFirstName() {
        return firstName;
    }

    public PersonalInfo firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public PersonalInfo lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public PersonalInfo email(String email) {
        this.email = email;
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNativeCountry() {
        return nativeCountry;
    }

    public PersonalInfo nativeCountry(String nativeCountry) {
        this.nativeCountry = nativeCountry;
        return this;
    }

    public void setNativeCountry(String nativeCountry) {
        this.nativeCountry = nativeCountry;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public PersonalInfo birthday(LocalDate birthday) {
        this.birthday = birthday;
        return this;
    }

    public void setBirthday(LocalDate birthday) {
        this.birthday = birthday;
    }

    public Boolean isClientABT() {
        return clientABT;
    }

    public PersonalInfo clientABT(Boolean clientABT) {
        this.clientABT = clientABT;
        return this;
    }

    public void setClientABT(Boolean clientABT) {
        this.clientABT = clientABT;
    }

    public String getRib() {
        return rib;
    }

    public PersonalInfo rib(String rib) {
        this.rib = rib;
        return this;
    }

    public void setRib(String rib) {
        this.rib = rib;
    }

    public String getNationality() {
        return nationality;
    }

    public PersonalInfo nationality(String nationality) {
        this.nationality = nationality;
        return this;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getSecondNationality() {
        return secondNationality;
    }

    public PersonalInfo secondNationality(String secondNationality) {
        this.secondNationality = secondNationality;
        return this;
    }

    public void setSecondNationality(String secondNationality) {
        this.secondNationality = secondNationality;
    }

    public Integer getNbrkids() {
        return nbrkids;
    }

    public PersonalInfo nbrkids(Integer nbrkids) {
        this.nbrkids = nbrkids;
        return this;
    }

    public void setNbrkids(Integer nbrkids) {
        this.nbrkids = nbrkids;
    }

    public String getMaritalStatus() {
        return maritalStatus;
    }

    public PersonalInfo maritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
        return this;
    }

    public void setMaritalStatus(String maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public String getPhone() {
        return phone;
    }

    public PersonalInfo phone(String phone) {
        this.phone = phone;
        return this;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Boolean isAmericanIndex() {
        return americanIndex;
    }

    public PersonalInfo americanIndex(Boolean americanIndex) {
        this.americanIndex = americanIndex;
        return this;
    }

    public void setAmericanIndex(Boolean americanIndex) {
        this.americanIndex = americanIndex;
    }

    public Request getRequest() {
        return request;
    }

    public PersonalInfo request(Request request) {
        this.request = request;
        return this;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

    public AdressInfo getAdressInfo() {
        return adressInfo;
    }

    public PersonalInfo adressInfo(AdressInfo adressInfo) {
        this.adressInfo = adressInfo;
        return this;
    }

    public void setAdressInfo(AdressInfo adressInfo) {
        this.adressInfo = adressInfo;
    }

    public Agency getAgency() {
        return agency;
    }

    public PersonalInfo agency(Agency agency) {
        this.agency = agency;
        return this;
    }

    public void setAgency(Agency agency) {
        this.agency = agency;
    }

    public FinancialInfo getFinancialInfo() {
        return financialInfo;
    }

    public PersonalInfo financialInfo(FinancialInfo financialInfo) {
        this.financialInfo = financialInfo;
        return this;
    }

    public void setFinancialInfo(FinancialInfo financialInfo) {
        this.financialInfo = financialInfo;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof PersonalInfo)) {
            return false;
        }
        return id != null && id.equals(((PersonalInfo) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "PersonalInfo{" +
            "id=" + getId() +
            ", civility='" + getCivility() + "'" +
            ", firstName='" + getFirstName() + "'" +
            ", lastName='" + getLastName() + "'" +
            ", email='" + getEmail() + "'" +
            ", nativeCountry='" + getNativeCountry() + "'" +
            ", birthday='" + getBirthday() + "'" +
            ", clientABT='" + isClientABT() + "'" +
            ", rib='" + getRib() + "'" +
            ", nationality='" + getNationality() + "'" +
            ", secondNationality='" + getSecondNationality() + "'" +
            ", nbrkids=" + getNbrkids() +
            ", maritalStatus='" + getMaritalStatus() + "'" +
            ", phone='" + getPhone() + "'" +
            ", americanIndex='" + isAmericanIndex() + "'" +
            "}";
    }
}
