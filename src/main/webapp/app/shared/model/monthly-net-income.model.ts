export interface IMonthlyNetIncome {
  id?: number;
  incomesFR?: string;
  incomesEN?: string;
  financialInfoId?: number;
}

export class MonthlyNetIncome implements IMonthlyNetIncome {
  constructor(public id?: number, public incomesFR?: string, public incomesEN?: string, public financialInfoId?: number) {}
}
