import { IRequest } from 'app/shared/model/request.model';

export interface IBankAccount {
  id?: number;
  libelleFR?: string;
  libelleEN?: string;
  descriptionFR?: string;
  descriptionEN?: string;
  requests?: IRequest[];
}

export class BankAccount implements IBankAccount {
  constructor(
    public id?: number,
    public libelleFR?: string,
    public libelleEN?: string,
    public descriptionFR?: string,
    public descriptionEN?: string,
    public requests?: IRequest[]
  ) {}
}
