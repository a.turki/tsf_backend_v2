import { Moment } from 'moment';
import { IBankAccount } from 'app/shared/model/bank-account.model';

export interface IRequest {
  id?: number;
  visioDate?: Moment;
  sendingMailDate?: Moment;
  state?: boolean;
  step?: string;
  codeVerification?: string;
  offerId?: number;
  personalInfoId?: number;
  requiredDocId?: number;
  bankAccounts?: IBankAccount[];
  documentId?: number;
}

export class Request implements IRequest {
  constructor(
    public id?: number,
    public visioDate?: Moment,
    public sendingMailDate?: Moment,
    public state?: boolean,
    public step?: string,
    public codeVerification?: string,
    public offerId?: number,
    public personalInfoId?: number,
    public requiredDocId?: number,
    public bankAccounts?: IBankAccount[],
    public documentId?: number
  ) {
    this.state = this.state || false;
  }
}
