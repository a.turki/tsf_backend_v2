import { Moment } from 'moment';
import { IRequest } from 'app/shared/model/request.model';

export interface IReqDocUpload {
  id?: number;
  pathDoc?: string;
  typeDoc?: string;
  uploadedAt?: Moment;
  updatedAt?: Moment;
  request?: IRequest;
}

export class ReqDocUpload implements IReqDocUpload {
  constructor(
    public id?: number,
    public pathDoc?: string,
    public typeDoc?: string,
    public uploadedAt?: Moment,
    public updatedAt?: Moment,
    public request?: IRequest
  ) {}
}
