import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IDetailOffer } from 'app/shared/model/detail-offer.model';
import { DetailOfferService } from './detail-offer.service';
import { DetailOfferDeleteDialogComponent } from './detail-offer-delete-dialog.component';

@Component({
  selector: 'jhi-detail-offer',
  templateUrl: './detail-offer.component.html',
})
export class DetailOfferComponent implements OnInit, OnDestroy {
  detailOffers?: IDetailOffer[];
  eventSubscriber?: Subscription;

  constructor(
    protected detailOfferService: DetailOfferService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.detailOfferService.query().subscribe((res: HttpResponse<IDetailOffer[]>) => (this.detailOffers = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInDetailOffers();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IDetailOffer): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInDetailOffers(): void {
    this.eventSubscriber = this.eventManager.subscribe('detailOfferListModification', () => this.loadAll());
  }

  delete(detailOffer: IDetailOffer): void {
    const modalRef = this.modalService.open(DetailOfferDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.detailOffer = detailOffer;
  }
}
