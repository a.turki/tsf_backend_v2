import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IFinancialInfo } from 'app/shared/model/financial-info.model';
import { FinancialInfoService } from './financial-info.service';
import { FinancialInfoDeleteDialogComponent } from './financial-info-delete-dialog.component';

@Component({
  selector: 'jhi-financial-info',
  templateUrl: './financial-info.component.html',
})
export class FinancialInfoComponent implements OnInit, OnDestroy {
  financialInfos?: IFinancialInfo[];
  eventSubscriber?: Subscription;

  constructor(
    protected financialInfoService: FinancialInfoService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.financialInfoService.query().subscribe((res: HttpResponse<IFinancialInfo[]>) => (this.financialInfos = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInFinancialInfos();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IFinancialInfo): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInFinancialInfos(): void {
    this.eventSubscriber = this.eventManager.subscribe('financialInfoListModification', () => this.loadAll());
  }

  delete(financialInfo: IFinancialInfo): void {
    const modalRef = this.modalService.open(FinancialInfoDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.financialInfo = financialInfo;
  }
}
