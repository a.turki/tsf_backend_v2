import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAuthentitficationToSign } from 'app/shared/model/authentitfication-to-sign.model';
import { AuthentitficationToSignService } from './authentitfication-to-sign.service';
import { AuthentitficationToSignDeleteDialogComponent } from './authentitfication-to-sign-delete-dialog.component';

@Component({
  selector: 'jhi-authentitfication-to-sign',
  templateUrl: './authentitfication-to-sign.component.html',
})
export class AuthentitficationToSignComponent implements OnInit, OnDestroy {
  authentitficationToSigns?: IAuthentitficationToSign[];
  eventSubscriber?: Subscription;

  constructor(
    protected authentitficationToSignService: AuthentitficationToSignService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.authentitficationToSignService
      .query()
      .subscribe((res: HttpResponse<IAuthentitficationToSign[]>) => (this.authentitficationToSigns = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAuthentitficationToSigns();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAuthentitficationToSign): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAuthentitficationToSigns(): void {
    this.eventSubscriber = this.eventManager.subscribe('authentitficationToSignListModification', () => this.loadAll());
  }

  delete(authentitficationToSign: IAuthentitficationToSign): void {
    const modalRef = this.modalService.open(AuthentitficationToSignDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.authentitficationToSign = authentitficationToSign;
  }
}
