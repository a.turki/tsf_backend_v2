import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IMonthlyNetIncome } from 'app/shared/model/monthly-net-income.model';
import { MonthlyNetIncomeService } from './monthly-net-income.service';
import { MonthlyNetIncomeDeleteDialogComponent } from './monthly-net-income-delete-dialog.component';

@Component({
  selector: 'jhi-monthly-net-income',
  templateUrl: './monthly-net-income.component.html',
})
export class MonthlyNetIncomeComponent implements OnInit, OnDestroy {
  monthlyNetIncomes?: IMonthlyNetIncome[];
  eventSubscriber?: Subscription;

  constructor(
    protected monthlyNetIncomeService: MonthlyNetIncomeService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.monthlyNetIncomeService.query().subscribe((res: HttpResponse<IMonthlyNetIncome[]>) => (this.monthlyNetIncomes = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInMonthlyNetIncomes();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IMonthlyNetIncome): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInMonthlyNetIncomes(): void {
    this.eventSubscriber = this.eventManager.subscribe('monthlyNetIncomeListModification', () => this.loadAll());
  }

  delete(monthlyNetIncome: IMonthlyNetIncome): void {
    const modalRef = this.modalService.open(MonthlyNetIncomeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.monthlyNetIncome = monthlyNetIncome;
  }
}
