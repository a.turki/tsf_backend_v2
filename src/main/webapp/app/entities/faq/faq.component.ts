import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IFAQ } from 'app/shared/model/faq.model';
import { FAQService } from './faq.service';
import { FAQDeleteDialogComponent } from './faq-delete-dialog.component';

@Component({
  selector: 'jhi-faq',
  templateUrl: './faq.component.html',
})
export class FAQComponent implements OnInit, OnDestroy {
  fAQS?: IFAQ[];
  eventSubscriber?: Subscription;

  constructor(protected fAQService: FAQService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.fAQService.query().subscribe((res: HttpResponse<IFAQ[]>) => (this.fAQS = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInFAQS();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IFAQ): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInFAQS(): void {
    this.eventSubscriber = this.eventManager.subscribe('fAQListModification', () => this.loadAll());
  }

  delete(fAQ: IFAQ): void {
    const modalRef = this.modalService.open(FAQDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.fAQ = fAQ;
  }
}
