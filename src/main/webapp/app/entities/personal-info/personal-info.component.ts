import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IPersonalInfo } from 'app/shared/model/personal-info.model';
import { PersonalInfoService } from './personal-info.service';
import { PersonalInfoDeleteDialogComponent } from './personal-info-delete-dialog.component';

@Component({
  selector: 'jhi-personal-info',
  templateUrl: './personal-info.component.html',
})
export class PersonalInfoComponent implements OnInit, OnDestroy {
  personalInfos?: IPersonalInfo[];
  eventSubscriber?: Subscription;

  constructor(
    protected personalInfoService: PersonalInfoService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.personalInfoService.query().subscribe((res: HttpResponse<IPersonalInfo[]>) => (this.personalInfos = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInPersonalInfos();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IPersonalInfo): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInPersonalInfos(): void {
    this.eventSubscriber = this.eventManager.subscribe('personalInfoListModification', () => this.loadAll());
  }

  delete(personalInfo: IPersonalInfo): void {
    const modalRef = this.modalService.open(PersonalInfoDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.personalInfo = personalInfo;
  }
}
