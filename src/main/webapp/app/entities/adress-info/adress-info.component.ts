import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IAdressInfo } from 'app/shared/model/adress-info.model';
import { AdressInfoService } from './adress-info.service';
import { AdressInfoDeleteDialogComponent } from './adress-info-delete-dialog.component';

@Component({
  selector: 'jhi-adress-info',
  templateUrl: './adress-info.component.html',
})
export class AdressInfoComponent implements OnInit, OnDestroy {
  adressInfos?: IAdressInfo[];
  eventSubscriber?: Subscription;

  constructor(protected adressInfoService: AdressInfoService, protected eventManager: JhiEventManager, protected modalService: NgbModal) {}

  loadAll(): void {
    this.adressInfoService.query().subscribe((res: HttpResponse<IAdressInfo[]>) => (this.adressInfos = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInAdressInfos();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IAdressInfo): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInAdressInfos(): void {
    this.eventSubscriber = this.eventManager.subscribe('adressInfoListModification', () => this.loadAll());
  }

  delete(adressInfo: IAdressInfo): void {
    const modalRef = this.modalService.open(AdressInfoDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.adressInfo = adressInfo;
  }
}
