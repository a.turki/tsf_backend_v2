import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IJustifRevenu } from 'app/shared/model/justif-revenu.model';
import { JustifRevenuService } from './justif-revenu.service';
import { JustifRevenuDeleteDialogComponent } from './justif-revenu-delete-dialog.component';

@Component({
  selector: 'jhi-justif-revenu',
  templateUrl: './justif-revenu.component.html',
})
export class JustifRevenuComponent implements OnInit, OnDestroy {
  justifRevenus?: IJustifRevenu[];
  eventSubscriber?: Subscription;

  constructor(
    protected justifRevenuService: JustifRevenuService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.justifRevenuService.query().subscribe((res: HttpResponse<IJustifRevenu[]>) => (this.justifRevenus = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInJustifRevenus();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IJustifRevenu): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInJustifRevenus(): void {
    this.eventSubscriber = this.eventManager.subscribe('justifRevenuListModification', () => this.loadAll());
  }

  delete(justifRevenu: IJustifRevenu): void {
    const modalRef = this.modalService.open(JustifRevenuDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.justifRevenu = justifRevenu;
  }
}
