import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager } from 'ng-jhipster';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IReqDocUpload } from 'app/shared/model/req-doc-upload.model';
import { ReqDocUploadService } from './req-doc-upload.service';
import { ReqDocUploadDeleteDialogComponent } from './req-doc-upload-delete-dialog.component';

@Component({
  selector: 'jhi-req-doc-upload',
  templateUrl: './req-doc-upload.component.html',
})
export class ReqDocUploadComponent implements OnInit, OnDestroy {
  reqDocUploads?: IReqDocUpload[];
  eventSubscriber?: Subscription;

  constructor(
    protected reqDocUploadService: ReqDocUploadService,
    protected eventManager: JhiEventManager,
    protected modalService: NgbModal
  ) {}

  loadAll(): void {
    this.reqDocUploadService.query().subscribe((res: HttpResponse<IReqDocUpload[]>) => (this.reqDocUploads = res.body || []));
  }

  ngOnInit(): void {
    this.loadAll();
    this.registerChangeInReqDocUploads();
  }

  ngOnDestroy(): void {
    if (this.eventSubscriber) {
      this.eventManager.destroy(this.eventSubscriber);
    }
  }

  trackId(index: number, item: IReqDocUpload): number {
    // eslint-disable-next-line @typescript-eslint/no-unnecessary-type-assertion
    return item.id!;
  }

  registerChangeInReqDocUploads(): void {
    this.eventSubscriber = this.eventManager.subscribe('reqDocUploadListModification', () => this.loadAll());
  }

  delete(reqDocUpload: IReqDocUpload): void {
    const modalRef = this.modalService.open(ReqDocUploadDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.reqDocUpload = reqDocUpload;
  }
}
