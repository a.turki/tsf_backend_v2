package com.attijeri.tfs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.attijeri.tfs.web.rest.TestUtil;

public class ResidencyDocumentDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(ResidencyDocumentDTO.class);
        ResidencyDocumentDTO residencyDocumentDTO1 = new ResidencyDocumentDTO();
        residencyDocumentDTO1.setId(1L);
        ResidencyDocumentDTO residencyDocumentDTO2 = new ResidencyDocumentDTO();
        assertThat(residencyDocumentDTO1).isNotEqualTo(residencyDocumentDTO2);
        residencyDocumentDTO2.setId(residencyDocumentDTO1.getId());
        assertThat(residencyDocumentDTO1).isEqualTo(residencyDocumentDTO2);
        residencyDocumentDTO2.setId(2L);
        assertThat(residencyDocumentDTO1).isNotEqualTo(residencyDocumentDTO2);
        residencyDocumentDTO1.setId(null);
        assertThat(residencyDocumentDTO1).isNotEqualTo(residencyDocumentDTO2);
    }
}
