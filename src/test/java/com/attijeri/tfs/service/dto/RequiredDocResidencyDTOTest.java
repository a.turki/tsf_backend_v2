package com.attijeri.tfs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.attijeri.tfs.web.rest.TestUtil;

public class RequiredDocResidencyDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RequiredDocResidencyDTO.class);
        RequiredDocResidencyDTO requiredDocResidencyDTO1 = new RequiredDocResidencyDTO();
        requiredDocResidencyDTO1.setId(1L);
        RequiredDocResidencyDTO requiredDocResidencyDTO2 = new RequiredDocResidencyDTO();
        assertThat(requiredDocResidencyDTO1).isNotEqualTo(requiredDocResidencyDTO2);
        requiredDocResidencyDTO2.setId(requiredDocResidencyDTO1.getId());
        assertThat(requiredDocResidencyDTO1).isEqualTo(requiredDocResidencyDTO2);
        requiredDocResidencyDTO2.setId(2L);
        assertThat(requiredDocResidencyDTO1).isNotEqualTo(requiredDocResidencyDTO2);
        requiredDocResidencyDTO1.setId(null);
        assertThat(requiredDocResidencyDTO1).isNotEqualTo(requiredDocResidencyDTO2);
    }
}
