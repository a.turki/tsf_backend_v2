package com.attijeri.tfs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.attijeri.tfs.web.rest.TestUtil;

public class JustifRevenuDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(JustifRevenuDTO.class);
        JustifRevenuDTO justifRevenuDTO1 = new JustifRevenuDTO();
        justifRevenuDTO1.setId(1L);
        JustifRevenuDTO justifRevenuDTO2 = new JustifRevenuDTO();
        assertThat(justifRevenuDTO1).isNotEqualTo(justifRevenuDTO2);
        justifRevenuDTO2.setId(justifRevenuDTO1.getId());
        assertThat(justifRevenuDTO1).isEqualTo(justifRevenuDTO2);
        justifRevenuDTO2.setId(2L);
        assertThat(justifRevenuDTO1).isNotEqualTo(justifRevenuDTO2);
        justifRevenuDTO1.setId(null);
        assertThat(justifRevenuDTO1).isNotEqualTo(justifRevenuDTO2);
    }
}
