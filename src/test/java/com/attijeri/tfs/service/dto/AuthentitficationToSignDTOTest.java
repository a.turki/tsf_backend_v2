package com.attijeri.tfs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.attijeri.tfs.web.rest.TestUtil;

public class AuthentitficationToSignDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AuthentitficationToSignDTO.class);
        AuthentitficationToSignDTO authentitficationToSignDTO1 = new AuthentitficationToSignDTO();
        authentitficationToSignDTO1.setId(1L);
        AuthentitficationToSignDTO authentitficationToSignDTO2 = new AuthentitficationToSignDTO();
        assertThat(authentitficationToSignDTO1).isNotEqualTo(authentitficationToSignDTO2);
        authentitficationToSignDTO2.setId(authentitficationToSignDTO1.getId());
        assertThat(authentitficationToSignDTO1).isEqualTo(authentitficationToSignDTO2);
        authentitficationToSignDTO2.setId(2L);
        assertThat(authentitficationToSignDTO1).isNotEqualTo(authentitficationToSignDTO2);
        authentitficationToSignDTO1.setId(null);
        assertThat(authentitficationToSignDTO1).isNotEqualTo(authentitficationToSignDTO2);
    }
}
