package com.attijeri.tfs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.attijeri.tfs.web.rest.TestUtil;

public class OtherResidencyFileDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OtherResidencyFileDTO.class);
        OtherResidencyFileDTO otherResidencyFileDTO1 = new OtherResidencyFileDTO();
        otherResidencyFileDTO1.setId(1L);
        OtherResidencyFileDTO otherResidencyFileDTO2 = new OtherResidencyFileDTO();
        assertThat(otherResidencyFileDTO1).isNotEqualTo(otherResidencyFileDTO2);
        otherResidencyFileDTO2.setId(otherResidencyFileDTO1.getId());
        assertThat(otherResidencyFileDTO1).isEqualTo(otherResidencyFileDTO2);
        otherResidencyFileDTO2.setId(2L);
        assertThat(otherResidencyFileDTO1).isNotEqualTo(otherResidencyFileDTO2);
        otherResidencyFileDTO1.setId(null);
        assertThat(otherResidencyFileDTO1).isNotEqualTo(otherResidencyFileDTO2);
    }
}
