package com.attijeri.tfs.service.dto;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.attijeri.tfs.web.rest.TestUtil;

public class OtherRevenuFileDTOTest {

    @Test
    public void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(OtherRevenuFileDTO.class);
        OtherRevenuFileDTO otherRevenuFileDTO1 = new OtherRevenuFileDTO();
        otherRevenuFileDTO1.setId(1L);
        OtherRevenuFileDTO otherRevenuFileDTO2 = new OtherRevenuFileDTO();
        assertThat(otherRevenuFileDTO1).isNotEqualTo(otherRevenuFileDTO2);
        otherRevenuFileDTO2.setId(otherRevenuFileDTO1.getId());
        assertThat(otherRevenuFileDTO1).isEqualTo(otherRevenuFileDTO2);
        otherRevenuFileDTO2.setId(2L);
        assertThat(otherRevenuFileDTO1).isNotEqualTo(otherRevenuFileDTO2);
        otherRevenuFileDTO1.setId(null);
        assertThat(otherRevenuFileDTO1).isNotEqualTo(otherRevenuFileDTO2);
    }
}
