package com.attijeri.tfs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class OtherResidencyFileMapperTest {

    private OtherResidencyFileMapper otherResidencyFileMapper;

    @BeforeEach
    public void setUp() {
        otherResidencyFileMapper = new OtherResidencyFileMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(otherResidencyFileMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(otherResidencyFileMapper.fromId(null)).isNull();
    }
}
