package com.attijeri.tfs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class JustifRevenuMapperTest {

    private JustifRevenuMapper justifRevenuMapper;

    @BeforeEach
    public void setUp() {
        justifRevenuMapper = new JustifRevenuMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(justifRevenuMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(justifRevenuMapper.fromId(null)).isNull();
    }
}
