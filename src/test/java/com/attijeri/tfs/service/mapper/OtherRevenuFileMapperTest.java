package com.attijeri.tfs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class OtherRevenuFileMapperTest {

    private OtherRevenuFileMapper otherRevenuFileMapper;

    @BeforeEach
    public void setUp() {
        otherRevenuFileMapper = new OtherRevenuFileMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(otherRevenuFileMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(otherRevenuFileMapper.fromId(null)).isNull();
    }
}
