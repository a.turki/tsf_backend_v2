package com.attijeri.tfs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class ResidencyDocumentMapperTest {

    private ResidencyDocumentMapper residencyDocumentMapper;

    @BeforeEach
    public void setUp() {
        residencyDocumentMapper = new ResidencyDocumentMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(residencyDocumentMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(residencyDocumentMapper.fromId(null)).isNull();
    }
}
