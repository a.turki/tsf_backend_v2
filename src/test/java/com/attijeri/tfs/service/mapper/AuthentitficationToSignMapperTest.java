package com.attijeri.tfs.service.mapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class AuthentitficationToSignMapperTest {

    private AuthentitficationToSignMapper authentitficationToSignMapper;

    @BeforeEach
    public void setUp() {
        authentitficationToSignMapper = new AuthentitficationToSignMapperImpl();
    }

    @Test
    public void testEntityFromId() {
        Long id = 1L;
        assertThat(authentitficationToSignMapper.fromId(id).getId()).isEqualTo(id);
        assertThat(authentitficationToSignMapper.fromId(null)).isNull();
    }
}
