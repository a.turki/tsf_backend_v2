package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsBackendApp;
import com.attijeri.tfs.domain.ReqDocUpload;
import com.attijeri.tfs.repository.ReqDocUploadRepository;
import com.attijeri.tfs.service.ReqDocUploadService;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.Instant;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ReqDocUploadResource} REST controller.
 */
@SpringBootTest(classes = TfsBackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ReqDocUploadResourceIT {

    private static final String DEFAULT_PATH_DOC = "AAAAAAAAAA";
    private static final String UPDATED_PATH_DOC = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE_DOC = "AAAAAAAAAA";
    private static final String UPDATED_TYPE_DOC = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_UPLOADED_AT = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_UPLOADED_AT = LocalDate.now(ZoneId.systemDefault());

    private static final Instant DEFAULT_UPDATED_AT = Instant.ofEpochMilli(0L);
    private static final Instant UPDATED_UPDATED_AT = Instant.now().truncatedTo(ChronoUnit.MILLIS);

    @Autowired
    private ReqDocUploadRepository reqDocUploadRepository;

    @Autowired
    private ReqDocUploadService reqDocUploadService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restReqDocUploadMockMvc;

    private ReqDocUpload reqDocUpload;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ReqDocUpload createEntity(EntityManager em) {
        ReqDocUpload reqDocUpload = new ReqDocUpload()
            .pathDoc(DEFAULT_PATH_DOC)
            .typeDoc(DEFAULT_TYPE_DOC)
            .updatedAt(DEFAULT_UPDATED_AT);
        return reqDocUpload;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ReqDocUpload createUpdatedEntity(EntityManager em) {
        ReqDocUpload reqDocUpload = new ReqDocUpload()
            .pathDoc(UPDATED_PATH_DOC)
            .typeDoc(UPDATED_TYPE_DOC)
            .updatedAt(UPDATED_UPDATED_AT);
        return reqDocUpload;
    }

    @BeforeEach
    public void initTest() {
        reqDocUpload = createEntity(em);
    }

    @Test
    @Transactional
    public void createReqDocUpload() throws Exception {
        int databaseSizeBeforeCreate = reqDocUploadRepository.findAll().size();
        // Create the ReqDocUpload
        restReqDocUploadMockMvc.perform(post("/api/req-doc-uploads")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(reqDocUpload)))
            .andExpect(status().isCreated());

        // Validate the ReqDocUpload in the database
        List<ReqDocUpload> reqDocUploadList = reqDocUploadRepository.findAll();
        assertThat(reqDocUploadList).hasSize(databaseSizeBeforeCreate + 1);
        ReqDocUpload testReqDocUpload = reqDocUploadList.get(reqDocUploadList.size() - 1);
        assertThat(testReqDocUpload.getPathDoc()).isEqualTo(DEFAULT_PATH_DOC);
        assertThat(testReqDocUpload.getTypeDoc()).isEqualTo(DEFAULT_TYPE_DOC);
        assertThat(testReqDocUpload.getUpdatedAt()).isEqualTo(DEFAULT_UPDATED_AT);
    }

    @Test
    @Transactional
    public void createReqDocUploadWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = reqDocUploadRepository.findAll().size();

        // Create the ReqDocUpload with an existing ID
        reqDocUpload.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restReqDocUploadMockMvc.perform(post("/api/req-doc-uploads")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(reqDocUpload)))
            .andExpect(status().isBadRequest());

        // Validate the ReqDocUpload in the database
        List<ReqDocUpload> reqDocUploadList = reqDocUploadRepository.findAll();
        assertThat(reqDocUploadList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllReqDocUploads() throws Exception {
        // Initialize the database
        reqDocUploadRepository.saveAndFlush(reqDocUpload);

        // Get all the reqDocUploadList
        restReqDocUploadMockMvc.perform(get("/api/req-doc-uploads?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(reqDocUpload.getId().intValue())))
            .andExpect(jsonPath("$.[*].pathDoc").value(hasItem(DEFAULT_PATH_DOC)))
            .andExpect(jsonPath("$.[*].typeDoc").value(hasItem(DEFAULT_TYPE_DOC)))
            .andExpect(jsonPath("$.[*].uploadedAt").value(hasItem(DEFAULT_UPLOADED_AT.toString())))
            .andExpect(jsonPath("$.[*].updatedAt").value(hasItem(DEFAULT_UPDATED_AT.toString())));
    }

    @Test
    @Transactional
    public void getReqDocUpload() throws Exception {
        // Initialize the database
        reqDocUploadRepository.saveAndFlush(reqDocUpload);

        // Get the reqDocUpload
        restReqDocUploadMockMvc.perform(get("/api/req-doc-uploads/{id}", reqDocUpload.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(reqDocUpload.getId().intValue()))
            .andExpect(jsonPath("$.pathDoc").value(DEFAULT_PATH_DOC))
            .andExpect(jsonPath("$.typeDoc").value(DEFAULT_TYPE_DOC))
            .andExpect(jsonPath("$.uploadedAt").value(DEFAULT_UPLOADED_AT.toString()))
            .andExpect(jsonPath("$.updatedAt").value(DEFAULT_UPDATED_AT.toString()));
    }
    @Test
    @Transactional
    public void getNonExistingReqDocUpload() throws Exception {
        // Get the reqDocUpload
        restReqDocUploadMockMvc.perform(get("/api/req-doc-uploads/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateReqDocUpload() throws Exception {
        // Initialize the database
        reqDocUploadService.save(reqDocUpload);

        int databaseSizeBeforeUpdate = reqDocUploadRepository.findAll().size();

        // Update the reqDocUpload
        ReqDocUpload updatedReqDocUpload = reqDocUploadRepository.findById(reqDocUpload.getId()).get();
        // Disconnect from session so that the updates on updatedReqDocUpload are not directly saved in db
        em.detach(updatedReqDocUpload);
        updatedReqDocUpload
            .pathDoc(UPDATED_PATH_DOC)
            .typeDoc(UPDATED_TYPE_DOC)
            .updatedAt(UPDATED_UPDATED_AT);

        restReqDocUploadMockMvc.perform(put("/api/req-doc-uploads")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(updatedReqDocUpload)))
            .andExpect(status().isOk());

        // Validate the ReqDocUpload in the database
        List<ReqDocUpload> reqDocUploadList = reqDocUploadRepository.findAll();
        assertThat(reqDocUploadList).hasSize(databaseSizeBeforeUpdate);
        ReqDocUpload testReqDocUpload = reqDocUploadList.get(reqDocUploadList.size() - 1);
        assertThat(testReqDocUpload.getPathDoc()).isEqualTo(UPDATED_PATH_DOC);
        assertThat(testReqDocUpload.getTypeDoc()).isEqualTo(UPDATED_TYPE_DOC);
        assertThat(testReqDocUpload.getUpdatedAt()).isEqualTo(UPDATED_UPDATED_AT);
    }

    @Test
    @Transactional
    public void updateNonExistingReqDocUpload() throws Exception {
        int databaseSizeBeforeUpdate = reqDocUploadRepository.findAll().size();

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restReqDocUploadMockMvc.perform(put("/api/req-doc-uploads")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(reqDocUpload)))
            .andExpect(status().isBadRequest());

        // Validate the ReqDocUpload in the database
        List<ReqDocUpload> reqDocUploadList = reqDocUploadRepository.findAll();
        assertThat(reqDocUploadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteReqDocUpload() throws Exception {
        // Initialize the database
        reqDocUploadService.save(reqDocUpload);

        int databaseSizeBeforeDelete = reqDocUploadRepository.findAll().size();

        // Delete the reqDocUpload
        restReqDocUploadMockMvc.perform(delete("/api/req-doc-uploads/{id}", reqDocUpload.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ReqDocUpload> reqDocUploadList = reqDocUploadRepository.findAll();
        assertThat(reqDocUploadList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
