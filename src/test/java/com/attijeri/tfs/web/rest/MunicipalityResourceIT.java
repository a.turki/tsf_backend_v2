package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsBackendApp;
import com.attijeri.tfs.domain.Municipality;
import com.attijeri.tfs.repository.MunicipalityRepository;
import com.attijeri.tfs.service.MunicipalityService;
import com.attijeri.tfs.service.dto.MunicipalityDTO;
import com.attijeri.tfs.service.mapper.MunicipalityMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link MunicipalityResource} REST controller.
 */
@SpringBootTest(classes = TfsBackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class MunicipalityResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private MunicipalityRepository municipalityRepository;

    @Autowired
    private MunicipalityMapper municipalityMapper;

    @Autowired
    private MunicipalityService municipalityService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMunicipalityMockMvc;

    private Municipality municipality;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Municipality createEntity(EntityManager em) {
        Municipality municipality = new Municipality()
            .name(DEFAULT_NAME);
        return municipality;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Municipality createUpdatedEntity(EntityManager em) {
        Municipality municipality = new Municipality()
            .name(UPDATED_NAME);
        return municipality;
    }

    @BeforeEach
    public void initTest() {
        municipality = createEntity(em);
    }

    @Test
    @Transactional
    public void createMunicipality() throws Exception {
        int databaseSizeBeforeCreate = municipalityRepository.findAll().size();
        // Create the Municipality
        MunicipalityDTO municipalityDTO = municipalityMapper.toDto(municipality);
        restMunicipalityMockMvc.perform(post("/api/municipalities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(municipalityDTO)))
            .andExpect(status().isCreated());

        // Validate the Municipality in the database
        List<Municipality> municipalityList = municipalityRepository.findAll();
        assertThat(municipalityList).hasSize(databaseSizeBeforeCreate + 1);
        Municipality testMunicipality = municipalityList.get(municipalityList.size() - 1);
        assertThat(testMunicipality.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createMunicipalityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = municipalityRepository.findAll().size();

        // Create the Municipality with an existing ID
        municipality.setId(1L);
        MunicipalityDTO municipalityDTO = municipalityMapper.toDto(municipality);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMunicipalityMockMvc.perform(post("/api/municipalities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(municipalityDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Municipality in the database
        List<Municipality> municipalityList = municipalityRepository.findAll();
        assertThat(municipalityList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllMunicipalities() throws Exception {
        // Initialize the database
        municipalityRepository.saveAndFlush(municipality);

        // Get all the municipalityList
        restMunicipalityMockMvc.perform(get("/api/municipalities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(municipality.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }
    
    @Test
    @Transactional
    public void getMunicipality() throws Exception {
        // Initialize the database
        municipalityRepository.saveAndFlush(municipality);

        // Get the municipality
        restMunicipalityMockMvc.perform(get("/api/municipalities/{id}", municipality.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(municipality.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }
    @Test
    @Transactional
    public void getNonExistingMunicipality() throws Exception {
        // Get the municipality
        restMunicipalityMockMvc.perform(get("/api/municipalities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMunicipality() throws Exception {
        // Initialize the database
        municipalityRepository.saveAndFlush(municipality);

        int databaseSizeBeforeUpdate = municipalityRepository.findAll().size();

        // Update the municipality
        Municipality updatedMunicipality = municipalityRepository.findById(municipality.getId()).get();
        // Disconnect from session so that the updates on updatedMunicipality are not directly saved in db
        em.detach(updatedMunicipality);
        updatedMunicipality
            .name(UPDATED_NAME);
        MunicipalityDTO municipalityDTO = municipalityMapper.toDto(updatedMunicipality);

        restMunicipalityMockMvc.perform(put("/api/municipalities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(municipalityDTO)))
            .andExpect(status().isOk());

        // Validate the Municipality in the database
        List<Municipality> municipalityList = municipalityRepository.findAll();
        assertThat(municipalityList).hasSize(databaseSizeBeforeUpdate);
        Municipality testMunicipality = municipalityList.get(municipalityList.size() - 1);
        assertThat(testMunicipality.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingMunicipality() throws Exception {
        int databaseSizeBeforeUpdate = municipalityRepository.findAll().size();

        // Create the Municipality
        MunicipalityDTO municipalityDTO = municipalityMapper.toDto(municipality);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMunicipalityMockMvc.perform(put("/api/municipalities")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(municipalityDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Municipality in the database
        List<Municipality> municipalityList = municipalityRepository.findAll();
        assertThat(municipalityList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMunicipality() throws Exception {
        // Initialize the database
        municipalityRepository.saveAndFlush(municipality);

        int databaseSizeBeforeDelete = municipalityRepository.findAll().size();

        // Delete the municipality
        restMunicipalityMockMvc.perform(delete("/api/municipalities/{id}", municipality.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Municipality> municipalityList = municipalityRepository.findAll();
        assertThat(municipalityList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
