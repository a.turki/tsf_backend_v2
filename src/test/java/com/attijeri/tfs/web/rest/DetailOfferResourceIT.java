package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsBackendApp;
import com.attijeri.tfs.domain.DetailOffer;
import com.attijeri.tfs.repository.DetailOfferRepository;
import com.attijeri.tfs.service.DetailOfferService;
import com.attijeri.tfs.service.dto.DetailOfferDTO;
import com.attijeri.tfs.service.mapper.DetailOfferMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link DetailOfferResource} REST controller.
 */
@SpringBootTest(classes = TfsBackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class DetailOfferResourceIT {

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    @Autowired
    private DetailOfferRepository detailOfferRepository;

    @Autowired
    private DetailOfferMapper detailOfferMapper;

    @Autowired
    private DetailOfferService detailOfferService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restDetailOfferMockMvc;

    private DetailOffer detailOffer;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DetailOffer createEntity(EntityManager em) {
        DetailOffer detailOffer = new DetailOffer()
            .description(DEFAULT_DESCRIPTION);
        return detailOffer;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static DetailOffer createUpdatedEntity(EntityManager em) {
        DetailOffer detailOffer = new DetailOffer()
            .description(UPDATED_DESCRIPTION);
        return detailOffer;
    }

    @BeforeEach
    public void initTest() {
        detailOffer = createEntity(em);
    }

    @Test
    @Transactional
    public void createDetailOffer() throws Exception {
        int databaseSizeBeforeCreate = detailOfferRepository.findAll().size();
        // Create the DetailOffer
        DetailOfferDTO detailOfferDTO = detailOfferMapper.toDto(detailOffer);
        restDetailOfferMockMvc.perform(post("/api/detail-offers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(detailOfferDTO)))
            .andExpect(status().isCreated());

        // Validate the DetailOffer in the database
        List<DetailOffer> detailOfferList = detailOfferRepository.findAll();
        assertThat(detailOfferList).hasSize(databaseSizeBeforeCreate + 1);
        DetailOffer testDetailOffer = detailOfferList.get(detailOfferList.size() - 1);
        assertThat(testDetailOffer.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    public void createDetailOfferWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = detailOfferRepository.findAll().size();

        // Create the DetailOffer with an existing ID
        detailOffer.setId(1L);
        DetailOfferDTO detailOfferDTO = detailOfferMapper.toDto(detailOffer);

        // An entity with an existing ID cannot be created, so this API call must fail
        restDetailOfferMockMvc.perform(post("/api/detail-offers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(detailOfferDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DetailOffer in the database
        List<DetailOffer> detailOfferList = detailOfferRepository.findAll();
        assertThat(detailOfferList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllDetailOffers() throws Exception {
        // Initialize the database
        detailOfferRepository.saveAndFlush(detailOffer);

        // Get all the detailOfferList
        restDetailOfferMockMvc.perform(get("/api/detail-offers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(detailOffer.getId().intValue())))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }
    
    @Test
    @Transactional
    public void getDetailOffer() throws Exception {
        // Initialize the database
        detailOfferRepository.saveAndFlush(detailOffer);

        // Get the detailOffer
        restDetailOfferMockMvc.perform(get("/api/detail-offers/{id}", detailOffer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(detailOffer.getId().intValue()))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }
    @Test
    @Transactional
    public void getNonExistingDetailOffer() throws Exception {
        // Get the detailOffer
        restDetailOfferMockMvc.perform(get("/api/detail-offers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateDetailOffer() throws Exception {
        // Initialize the database
        detailOfferRepository.saveAndFlush(detailOffer);

        int databaseSizeBeforeUpdate = detailOfferRepository.findAll().size();

        // Update the detailOffer
        DetailOffer updatedDetailOffer = detailOfferRepository.findById(detailOffer.getId()).get();
        // Disconnect from session so that the updates on updatedDetailOffer are not directly saved in db
        em.detach(updatedDetailOffer);
        updatedDetailOffer
            .description(UPDATED_DESCRIPTION);
        DetailOfferDTO detailOfferDTO = detailOfferMapper.toDto(updatedDetailOffer);

        restDetailOfferMockMvc.perform(put("/api/detail-offers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(detailOfferDTO)))
            .andExpect(status().isOk());

        // Validate the DetailOffer in the database
        List<DetailOffer> detailOfferList = detailOfferRepository.findAll();
        assertThat(detailOfferList).hasSize(databaseSizeBeforeUpdate);
        DetailOffer testDetailOffer = detailOfferList.get(detailOfferList.size() - 1);
        assertThat(testDetailOffer.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    public void updateNonExistingDetailOffer() throws Exception {
        int databaseSizeBeforeUpdate = detailOfferRepository.findAll().size();

        // Create the DetailOffer
        DetailOfferDTO detailOfferDTO = detailOfferMapper.toDto(detailOffer);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restDetailOfferMockMvc.perform(put("/api/detail-offers")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(detailOfferDTO)))
            .andExpect(status().isBadRequest());

        // Validate the DetailOffer in the database
        List<DetailOffer> detailOfferList = detailOfferRepository.findAll();
        assertThat(detailOfferList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteDetailOffer() throws Exception {
        // Initialize the database
        detailOfferRepository.saveAndFlush(detailOffer);

        int databaseSizeBeforeDelete = detailOfferRepository.findAll().size();

        // Delete the detailOffer
        restDetailOfferMockMvc.perform(delete("/api/detail-offers/{id}", detailOffer.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<DetailOffer> detailOfferList = detailOfferRepository.findAll();
        assertThat(detailOfferList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
