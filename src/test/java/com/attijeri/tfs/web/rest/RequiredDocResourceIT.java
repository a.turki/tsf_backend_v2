package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsBackendApp;
import com.attijeri.tfs.domain.RequiredDoc;
import com.attijeri.tfs.repository.RequiredDocRepository;
import com.attijeri.tfs.service.RequiredDocService;
import com.attijeri.tfs.service.dto.RequiredDocDTO;
import com.attijeri.tfs.service.mapper.RequiredDocMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RequiredDocResource} REST controller.
 */
@SpringBootTest(classes = TfsBackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class RequiredDocResourceIT {

    private static final String DEFAULT_LABEL = "AAAAAAAAAA";
    private static final String UPDATED_LABEL = "BBBBBBBBBB";

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_NUM_CIN = "AAAAAAAAAA";
    private static final String UPDATED_NUM_CIN = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DELIVERY_DATE_CIN = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DELIVERY_DATE_CIN = LocalDate.now(ZoneId.systemDefault());

    private static final String DEFAULT_RECTO_CIN = "AAAAAAAAAA";
    private static final String UPDATED_RECTO_CIN = "BBBBBBBBBB";

    private static final String DEFAULT_VERSO_CIN = "AAAAAAAAAA";
    private static final String UPDATED_VERSO_CIN = "BBBBBBBBBB";

    private static final String DEFAULT_FATCA = "AAAAAAAAAA";
    private static final String UPDATED_FATCA = "BBBBBBBBBB";

    @Autowired
    private RequiredDocRepository requiredDocRepository;

    @Autowired
    private RequiredDocMapper requiredDocMapper;

    @Autowired
    private RequiredDocService requiredDocService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRequiredDocMockMvc;

    private RequiredDoc requiredDoc;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RequiredDoc createEntity(EntityManager em) {
        RequiredDoc requiredDoc = new RequiredDoc()
            .label(DEFAULT_LABEL)
            .type(DEFAULT_TYPE)
            .numCIN(DEFAULT_NUM_CIN)
            .deliveryDateCin(DEFAULT_DELIVERY_DATE_CIN)
            .rectoCin(DEFAULT_RECTO_CIN)
            .versoCin(DEFAULT_VERSO_CIN)
            .fatca(DEFAULT_FATCA);
        return requiredDoc;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RequiredDoc createUpdatedEntity(EntityManager em) {
        RequiredDoc requiredDoc = new RequiredDoc()
            .label(UPDATED_LABEL)
            .type(UPDATED_TYPE)
            .numCIN(UPDATED_NUM_CIN)
            .deliveryDateCin(UPDATED_DELIVERY_DATE_CIN)
            .rectoCin(UPDATED_RECTO_CIN)
            .versoCin(UPDATED_VERSO_CIN)
            .fatca(UPDATED_FATCA);
        return requiredDoc;
    }

    @BeforeEach
    public void initTest() {
        requiredDoc = createEntity(em);
    }

    @Test
    @Transactional
    public void createRequiredDoc() throws Exception {
        int databaseSizeBeforeCreate = requiredDocRepository.findAll().size();
        // Create the RequiredDoc
        RequiredDocDTO requiredDocDTO = requiredDocMapper.toDto(requiredDoc);
        restRequiredDocMockMvc.perform(post("/api/required-docs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(requiredDocDTO)))
            .andExpect(status().isCreated());

        // Validate the RequiredDoc in the database
        List<RequiredDoc> requiredDocList = requiredDocRepository.findAll();
        assertThat(requiredDocList).hasSize(databaseSizeBeforeCreate + 1);
        RequiredDoc testRequiredDoc = requiredDocList.get(requiredDocList.size() - 1);
        assertThat(testRequiredDoc.getLabel()).isEqualTo(DEFAULT_LABEL);
        assertThat(testRequiredDoc.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testRequiredDoc.getNumCIN()).isEqualTo(DEFAULT_NUM_CIN);
        assertThat(testRequiredDoc.getDeliveryDateCin()).isEqualTo(DEFAULT_DELIVERY_DATE_CIN);
        assertThat(testRequiredDoc.getRectoCin()).isEqualTo(DEFAULT_RECTO_CIN);
        assertThat(testRequiredDoc.getVersoCin()).isEqualTo(DEFAULT_VERSO_CIN);
        assertThat(testRequiredDoc.getFatca()).isEqualTo(DEFAULT_FATCA);
    }

    @Test
    @Transactional
    public void createRequiredDocWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = requiredDocRepository.findAll().size();

        // Create the RequiredDoc with an existing ID
        requiredDoc.setId(1L);
        RequiredDocDTO requiredDocDTO = requiredDocMapper.toDto(requiredDoc);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRequiredDocMockMvc.perform(post("/api/required-docs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(requiredDocDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RequiredDoc in the database
        List<RequiredDoc> requiredDocList = requiredDocRepository.findAll();
        assertThat(requiredDocList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllRequiredDocs() throws Exception {
        // Initialize the database
        requiredDocRepository.saveAndFlush(requiredDoc);

        // Get all the requiredDocList
        restRequiredDocMockMvc.perform(get("/api/required-docs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(requiredDoc.getId().intValue())))
            .andExpect(jsonPath("$.[*].label").value(hasItem(DEFAULT_LABEL)))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].numCIN").value(hasItem(DEFAULT_NUM_CIN)))
            .andExpect(jsonPath("$.[*].deliveryDateCin").value(hasItem(DEFAULT_DELIVERY_DATE_CIN.toString())))
            .andExpect(jsonPath("$.[*].rectoCin").value(hasItem(DEFAULT_RECTO_CIN)))
            .andExpect(jsonPath("$.[*].versoCin").value(hasItem(DEFAULT_VERSO_CIN)))
            .andExpect(jsonPath("$.[*].fatca").value(hasItem(DEFAULT_FATCA)));
    }
    
    @Test
    @Transactional
    public void getRequiredDoc() throws Exception {
        // Initialize the database
        requiredDocRepository.saveAndFlush(requiredDoc);

        // Get the requiredDoc
        restRequiredDocMockMvc.perform(get("/api/required-docs/{id}", requiredDoc.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(requiredDoc.getId().intValue()))
            .andExpect(jsonPath("$.label").value(DEFAULT_LABEL))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.numCIN").value(DEFAULT_NUM_CIN))
            .andExpect(jsonPath("$.deliveryDateCin").value(DEFAULT_DELIVERY_DATE_CIN.toString()))
            .andExpect(jsonPath("$.rectoCin").value(DEFAULT_RECTO_CIN))
            .andExpect(jsonPath("$.versoCin").value(DEFAULT_VERSO_CIN))
            .andExpect(jsonPath("$.fatca").value(DEFAULT_FATCA));
    }
    @Test
    @Transactional
    public void getNonExistingRequiredDoc() throws Exception {
        // Get the requiredDoc
        restRequiredDocMockMvc.perform(get("/api/required-docs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRequiredDoc() throws Exception {
        // Initialize the database
        requiredDocRepository.saveAndFlush(requiredDoc);

        int databaseSizeBeforeUpdate = requiredDocRepository.findAll().size();

        // Update the requiredDoc
        RequiredDoc updatedRequiredDoc = requiredDocRepository.findById(requiredDoc.getId()).get();
        // Disconnect from session so that the updates on updatedRequiredDoc are not directly saved in db
        em.detach(updatedRequiredDoc);
        updatedRequiredDoc
            .label(UPDATED_LABEL)
            .type(UPDATED_TYPE)
            .numCIN(UPDATED_NUM_CIN)
            .deliveryDateCin(UPDATED_DELIVERY_DATE_CIN)
            .rectoCin(UPDATED_RECTO_CIN)
            .versoCin(UPDATED_VERSO_CIN)
            .fatca(UPDATED_FATCA);
        RequiredDocDTO requiredDocDTO = requiredDocMapper.toDto(updatedRequiredDoc);

        restRequiredDocMockMvc.perform(put("/api/required-docs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(requiredDocDTO)))
            .andExpect(status().isOk());

        // Validate the RequiredDoc in the database
        List<RequiredDoc> requiredDocList = requiredDocRepository.findAll();
        assertThat(requiredDocList).hasSize(databaseSizeBeforeUpdate);
        RequiredDoc testRequiredDoc = requiredDocList.get(requiredDocList.size() - 1);
        assertThat(testRequiredDoc.getLabel()).isEqualTo(UPDATED_LABEL);
        assertThat(testRequiredDoc.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testRequiredDoc.getNumCIN()).isEqualTo(UPDATED_NUM_CIN);
        assertThat(testRequiredDoc.getDeliveryDateCin()).isEqualTo(UPDATED_DELIVERY_DATE_CIN);
        assertThat(testRequiredDoc.getRectoCin()).isEqualTo(UPDATED_RECTO_CIN);
        assertThat(testRequiredDoc.getVersoCin()).isEqualTo(UPDATED_VERSO_CIN);
        assertThat(testRequiredDoc.getFatca()).isEqualTo(UPDATED_FATCA);
    }

    @Test
    @Transactional
    public void updateNonExistingRequiredDoc() throws Exception {
        int databaseSizeBeforeUpdate = requiredDocRepository.findAll().size();

        // Create the RequiredDoc
        RequiredDocDTO requiredDocDTO = requiredDocMapper.toDto(requiredDoc);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRequiredDocMockMvc.perform(put("/api/required-docs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(requiredDocDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RequiredDoc in the database
        List<RequiredDoc> requiredDocList = requiredDocRepository.findAll();
        assertThat(requiredDocList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRequiredDoc() throws Exception {
        // Initialize the database
        requiredDocRepository.saveAndFlush(requiredDoc);

        int databaseSizeBeforeDelete = requiredDocRepository.findAll().size();

        // Delete the requiredDoc
        restRequiredDocMockMvc.perform(delete("/api/required-docs/{id}", requiredDoc.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RequiredDoc> requiredDocList = requiredDocRepository.findAll();
        assertThat(requiredDocList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
