package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsBackendApp;
import com.attijeri.tfs.domain.MonthlyNetIncome;
import com.attijeri.tfs.repository.MonthlyNetIncomeRepository;
import com.attijeri.tfs.service.MonthlyNetIncomeService;
import com.attijeri.tfs.service.dto.MonthlyNetIncomeDTO;
import com.attijeri.tfs.service.mapper.MonthlyNetIncomeMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link MonthlyNetIncomeResource} REST controller.
 */
@SpringBootTest(classes = TfsBackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class MonthlyNetIncomeResourceIT {

    private static final String DEFAULT_INCOMES_FR = "AAAAAAAAAA";
    private static final String UPDATED_INCOMES_FR = "BBBBBBBBBB";

    private static final String DEFAULT_INCOMES_EN = "AAAAAAAAAA";
    private static final String UPDATED_INCOMES_EN = "BBBBBBBBBB";

    @Autowired
    private MonthlyNetIncomeRepository monthlyNetIncomeRepository;

    @Autowired
    private MonthlyNetIncomeMapper monthlyNetIncomeMapper;

    @Autowired
    private MonthlyNetIncomeService monthlyNetIncomeService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMonthlyNetIncomeMockMvc;

    private MonthlyNetIncome monthlyNetIncome;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MonthlyNetIncome createEntity(EntityManager em) {
        MonthlyNetIncome monthlyNetIncome = new MonthlyNetIncome()
            .incomesFR(DEFAULT_INCOMES_FR)
            .incomesEN(DEFAULT_INCOMES_EN);
        return monthlyNetIncome;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MonthlyNetIncome createUpdatedEntity(EntityManager em) {
        MonthlyNetIncome monthlyNetIncome = new MonthlyNetIncome()
            .incomesFR(UPDATED_INCOMES_FR)
            .incomesEN(UPDATED_INCOMES_EN);
        return monthlyNetIncome;
    }

    @BeforeEach
    public void initTest() {
        monthlyNetIncome = createEntity(em);
    }

    @Test
    @Transactional
    public void createMonthlyNetIncome() throws Exception {
        int databaseSizeBeforeCreate = monthlyNetIncomeRepository.findAll().size();
        // Create the MonthlyNetIncome
        MonthlyNetIncomeDTO monthlyNetIncomeDTO = monthlyNetIncomeMapper.toDto(monthlyNetIncome);
        restMonthlyNetIncomeMockMvc.perform(post("/api/monthly-net-incomes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(monthlyNetIncomeDTO)))
            .andExpect(status().isCreated());

        // Validate the MonthlyNetIncome in the database
        List<MonthlyNetIncome> monthlyNetIncomeList = monthlyNetIncomeRepository.findAll();
        assertThat(monthlyNetIncomeList).hasSize(databaseSizeBeforeCreate + 1);
        MonthlyNetIncome testMonthlyNetIncome = monthlyNetIncomeList.get(monthlyNetIncomeList.size() - 1);
        assertThat(testMonthlyNetIncome.getIncomesFR()).isEqualTo(DEFAULT_INCOMES_FR);
        assertThat(testMonthlyNetIncome.getIncomesEN()).isEqualTo(DEFAULT_INCOMES_EN);
    }

    @Test
    @Transactional
    public void createMonthlyNetIncomeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = monthlyNetIncomeRepository.findAll().size();

        // Create the MonthlyNetIncome with an existing ID
        monthlyNetIncome.setId(1L);
        MonthlyNetIncomeDTO monthlyNetIncomeDTO = monthlyNetIncomeMapper.toDto(monthlyNetIncome);

        // An entity with an existing ID cannot be created, so this API call must fail
        restMonthlyNetIncomeMockMvc.perform(post("/api/monthly-net-incomes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(monthlyNetIncomeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MonthlyNetIncome in the database
        List<MonthlyNetIncome> monthlyNetIncomeList = monthlyNetIncomeRepository.findAll();
        assertThat(monthlyNetIncomeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllMonthlyNetIncomes() throws Exception {
        // Initialize the database
        monthlyNetIncomeRepository.saveAndFlush(monthlyNetIncome);

        // Get all the monthlyNetIncomeList
        restMonthlyNetIncomeMockMvc.perform(get("/api/monthly-net-incomes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(monthlyNetIncome.getId().intValue())))
            .andExpect(jsonPath("$.[*].incomesFR").value(hasItem(DEFAULT_INCOMES_FR)))
            .andExpect(jsonPath("$.[*].incomesEN").value(hasItem(DEFAULT_INCOMES_EN)));
    }
    
    @Test
    @Transactional
    public void getMonthlyNetIncome() throws Exception {
        // Initialize the database
        monthlyNetIncomeRepository.saveAndFlush(monthlyNetIncome);

        // Get the monthlyNetIncome
        restMonthlyNetIncomeMockMvc.perform(get("/api/monthly-net-incomes/{id}", monthlyNetIncome.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(monthlyNetIncome.getId().intValue()))
            .andExpect(jsonPath("$.incomesFR").value(DEFAULT_INCOMES_FR))
            .andExpect(jsonPath("$.incomesEN").value(DEFAULT_INCOMES_EN));
    }
    @Test
    @Transactional
    public void getNonExistingMonthlyNetIncome() throws Exception {
        // Get the monthlyNetIncome
        restMonthlyNetIncomeMockMvc.perform(get("/api/monthly-net-incomes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateMonthlyNetIncome() throws Exception {
        // Initialize the database
        monthlyNetIncomeRepository.saveAndFlush(monthlyNetIncome);

        int databaseSizeBeforeUpdate = monthlyNetIncomeRepository.findAll().size();

        // Update the monthlyNetIncome
        MonthlyNetIncome updatedMonthlyNetIncome = monthlyNetIncomeRepository.findById(monthlyNetIncome.getId()).get();
        // Disconnect from session so that the updates on updatedMonthlyNetIncome are not directly saved in db
        em.detach(updatedMonthlyNetIncome);
        updatedMonthlyNetIncome
            .incomesFR(UPDATED_INCOMES_FR)
            .incomesEN(UPDATED_INCOMES_EN);
        MonthlyNetIncomeDTO monthlyNetIncomeDTO = monthlyNetIncomeMapper.toDto(updatedMonthlyNetIncome);

        restMonthlyNetIncomeMockMvc.perform(put("/api/monthly-net-incomes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(monthlyNetIncomeDTO)))
            .andExpect(status().isOk());

        // Validate the MonthlyNetIncome in the database
        List<MonthlyNetIncome> monthlyNetIncomeList = monthlyNetIncomeRepository.findAll();
        assertThat(monthlyNetIncomeList).hasSize(databaseSizeBeforeUpdate);
        MonthlyNetIncome testMonthlyNetIncome = monthlyNetIncomeList.get(monthlyNetIncomeList.size() - 1);
        assertThat(testMonthlyNetIncome.getIncomesFR()).isEqualTo(UPDATED_INCOMES_FR);
        assertThat(testMonthlyNetIncome.getIncomesEN()).isEqualTo(UPDATED_INCOMES_EN);
    }

    @Test
    @Transactional
    public void updateNonExistingMonthlyNetIncome() throws Exception {
        int databaseSizeBeforeUpdate = monthlyNetIncomeRepository.findAll().size();

        // Create the MonthlyNetIncome
        MonthlyNetIncomeDTO monthlyNetIncomeDTO = monthlyNetIncomeMapper.toDto(monthlyNetIncome);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMonthlyNetIncomeMockMvc.perform(put("/api/monthly-net-incomes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(monthlyNetIncomeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the MonthlyNetIncome in the database
        List<MonthlyNetIncome> monthlyNetIncomeList = monthlyNetIncomeRepository.findAll();
        assertThat(monthlyNetIncomeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteMonthlyNetIncome() throws Exception {
        // Initialize the database
        monthlyNetIncomeRepository.saveAndFlush(monthlyNetIncome);

        int databaseSizeBeforeDelete = monthlyNetIncomeRepository.findAll().size();

        // Delete the monthlyNetIncome
        restMonthlyNetIncomeMockMvc.perform(delete("/api/monthly-net-incomes/{id}", monthlyNetIncome.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MonthlyNetIncome> monthlyNetIncomeList = monthlyNetIncomeRepository.findAll();
        assertThat(monthlyNetIncomeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
