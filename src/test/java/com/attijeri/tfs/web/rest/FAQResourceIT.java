package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsBackendApp;
import com.attijeri.tfs.domain.FAQ;
import com.attijeri.tfs.repository.FAQRepository;
import com.attijeri.tfs.service.FAQService;
import com.attijeri.tfs.service.dto.FAQDTO;
import com.attijeri.tfs.service.mapper.FAQMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FAQResource} REST controller.
 */
@SpringBootTest(classes = TfsBackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class FAQResourceIT {

    private static final String DEFAULT_QUESTION = "AAAAAAAAAA";
    private static final String UPDATED_QUESTION = "BBBBBBBBBB";

    private static final String DEFAULT_ANSWER = "AAAAAAAAAA";
    private static final String UPDATED_ANSWER = "BBBBBBBBBB";

    @Autowired
    private FAQRepository fAQRepository;

    @Autowired
    private FAQMapper fAQMapper;

    @Autowired
    private FAQService fAQService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFAQMockMvc;

    private FAQ fAQ;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FAQ createEntity(EntityManager em) {
        FAQ fAQ = new FAQ()
            .question(DEFAULT_QUESTION)
            .answer(DEFAULT_ANSWER);
        return fAQ;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FAQ createUpdatedEntity(EntityManager em) {
        FAQ fAQ = new FAQ()
            .question(UPDATED_QUESTION)
            .answer(UPDATED_ANSWER);
        return fAQ;
    }

    @BeforeEach
    public void initTest() {
        fAQ = createEntity(em);
    }

    @Test
    @Transactional
    public void createFAQ() throws Exception {
        int databaseSizeBeforeCreate = fAQRepository.findAll().size();
        // Create the FAQ
        FAQDTO fAQDTO = fAQMapper.toDto(fAQ);
        restFAQMockMvc.perform(post("/api/faqs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fAQDTO)))
            .andExpect(status().isCreated());

        // Validate the FAQ in the database
        List<FAQ> fAQList = fAQRepository.findAll();
        assertThat(fAQList).hasSize(databaseSizeBeforeCreate + 1);
        FAQ testFAQ = fAQList.get(fAQList.size() - 1);
        assertThat(testFAQ.getQuestion()).isEqualTo(DEFAULT_QUESTION);
        assertThat(testFAQ.getAnswer()).isEqualTo(DEFAULT_ANSWER);
    }

    @Test
    @Transactional
    public void createFAQWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = fAQRepository.findAll().size();

        // Create the FAQ with an existing ID
        fAQ.setId(1L);
        FAQDTO fAQDTO = fAQMapper.toDto(fAQ);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFAQMockMvc.perform(post("/api/faqs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fAQDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FAQ in the database
        List<FAQ> fAQList = fAQRepository.findAll();
        assertThat(fAQList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllFAQS() throws Exception {
        // Initialize the database
        fAQRepository.saveAndFlush(fAQ);

        // Get all the fAQList
        restFAQMockMvc.perform(get("/api/faqs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(fAQ.getId().intValue())))
            .andExpect(jsonPath("$.[*].question").value(hasItem(DEFAULT_QUESTION)))
            .andExpect(jsonPath("$.[*].answer").value(hasItem(DEFAULT_ANSWER)));
    }
    
    @Test
    @Transactional
    public void getFAQ() throws Exception {
        // Initialize the database
        fAQRepository.saveAndFlush(fAQ);

        // Get the fAQ
        restFAQMockMvc.perform(get("/api/faqs/{id}", fAQ.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(fAQ.getId().intValue()))
            .andExpect(jsonPath("$.question").value(DEFAULT_QUESTION))
            .andExpect(jsonPath("$.answer").value(DEFAULT_ANSWER));
    }
    @Test
    @Transactional
    public void getNonExistingFAQ() throws Exception {
        // Get the fAQ
        restFAQMockMvc.perform(get("/api/faqs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFAQ() throws Exception {
        // Initialize the database
        fAQRepository.saveAndFlush(fAQ);

        int databaseSizeBeforeUpdate = fAQRepository.findAll().size();

        // Update the fAQ
        FAQ updatedFAQ = fAQRepository.findById(fAQ.getId()).get();
        // Disconnect from session so that the updates on updatedFAQ are not directly saved in db
        em.detach(updatedFAQ);
        updatedFAQ
            .question(UPDATED_QUESTION)
            .answer(UPDATED_ANSWER);
        FAQDTO fAQDTO = fAQMapper.toDto(updatedFAQ);

        restFAQMockMvc.perform(put("/api/faqs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fAQDTO)))
            .andExpect(status().isOk());

        // Validate the FAQ in the database
        List<FAQ> fAQList = fAQRepository.findAll();
        assertThat(fAQList).hasSize(databaseSizeBeforeUpdate);
        FAQ testFAQ = fAQList.get(fAQList.size() - 1);
        assertThat(testFAQ.getQuestion()).isEqualTo(UPDATED_QUESTION);
        assertThat(testFAQ.getAnswer()).isEqualTo(UPDATED_ANSWER);
    }

    @Test
    @Transactional
    public void updateNonExistingFAQ() throws Exception {
        int databaseSizeBeforeUpdate = fAQRepository.findAll().size();

        // Create the FAQ
        FAQDTO fAQDTO = fAQMapper.toDto(fAQ);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFAQMockMvc.perform(put("/api/faqs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(fAQDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FAQ in the database
        List<FAQ> fAQList = fAQRepository.findAll();
        assertThat(fAQList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFAQ() throws Exception {
        // Initialize the database
        fAQRepository.saveAndFlush(fAQ);

        int databaseSizeBeforeDelete = fAQRepository.findAll().size();

        // Delete the fAQ
        restFAQMockMvc.perform(delete("/api/faqs/{id}", fAQ.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FAQ> fAQList = fAQRepository.findAll();
        assertThat(fAQList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
