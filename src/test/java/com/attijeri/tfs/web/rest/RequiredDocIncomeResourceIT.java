package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsBackendApp;
import com.attijeri.tfs.domain.RequiredDocIncome;
import com.attijeri.tfs.repository.RequiredDocIncomeRepository;
import com.attijeri.tfs.service.RequiredDocIncomeService;
import com.attijeri.tfs.service.dto.RequiredDocIncomeDTO;
import com.attijeri.tfs.service.mapper.RequiredDocIncomeMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RequiredDocIncomeResource} REST controller.
 */
@SpringBootTest(classes = TfsBackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class RequiredDocIncomeResourceIT {

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_PATH = "AAAAAAAAAA";
    private static final String UPDATED_PATH = "BBBBBBBBBB";

    @Autowired
    private RequiredDocIncomeRepository requiredDocIncomeRepository;

    @Autowired
    private RequiredDocIncomeMapper requiredDocIncomeMapper;

    @Autowired
    private RequiredDocIncomeService requiredDocIncomeService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRequiredDocIncomeMockMvc;

    private RequiredDocIncome requiredDocIncome;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RequiredDocIncome createEntity(EntityManager em) {
        RequiredDocIncome requiredDocIncome = new RequiredDocIncome()
            .type(DEFAULT_TYPE)
            .path(DEFAULT_PATH);
        return requiredDocIncome;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RequiredDocIncome createUpdatedEntity(EntityManager em) {
        RequiredDocIncome requiredDocIncome = new RequiredDocIncome()
            .type(UPDATED_TYPE)
            .path(UPDATED_PATH);
        return requiredDocIncome;
    }

    @BeforeEach
    public void initTest() {
        requiredDocIncome = createEntity(em);
    }

    @Test
    @Transactional
    public void createRequiredDocIncome() throws Exception {
        int databaseSizeBeforeCreate = requiredDocIncomeRepository.findAll().size();
        // Create the RequiredDocIncome
        RequiredDocIncomeDTO requiredDocIncomeDTO = requiredDocIncomeMapper.toDto(requiredDocIncome);
        restRequiredDocIncomeMockMvc.perform(post("/api/required-doc-incomes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(requiredDocIncomeDTO)))
            .andExpect(status().isCreated());

        // Validate the RequiredDocIncome in the database
        List<RequiredDocIncome> requiredDocIncomeList = requiredDocIncomeRepository.findAll();
        assertThat(requiredDocIncomeList).hasSize(databaseSizeBeforeCreate + 1);
        RequiredDocIncome testRequiredDocIncome = requiredDocIncomeList.get(requiredDocIncomeList.size() - 1);
        assertThat(testRequiredDocIncome.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testRequiredDocIncome.getPath()).isEqualTo(DEFAULT_PATH);
    }

    @Test
    @Transactional
    public void createRequiredDocIncomeWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = requiredDocIncomeRepository.findAll().size();

        // Create the RequiredDocIncome with an existing ID
        requiredDocIncome.setId(1L);
        RequiredDocIncomeDTO requiredDocIncomeDTO = requiredDocIncomeMapper.toDto(requiredDocIncome);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRequiredDocIncomeMockMvc.perform(post("/api/required-doc-incomes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(requiredDocIncomeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RequiredDocIncome in the database
        List<RequiredDocIncome> requiredDocIncomeList = requiredDocIncomeRepository.findAll();
        assertThat(requiredDocIncomeList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllRequiredDocIncomes() throws Exception {
        // Initialize the database
        requiredDocIncomeRepository.saveAndFlush(requiredDocIncome);

        // Get all the requiredDocIncomeList
        restRequiredDocIncomeMockMvc.perform(get("/api/required-doc-incomes?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(requiredDocIncome.getId().intValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].path").value(hasItem(DEFAULT_PATH)));
    }
    
    @Test
    @Transactional
    public void getRequiredDocIncome() throws Exception {
        // Initialize the database
        requiredDocIncomeRepository.saveAndFlush(requiredDocIncome);

        // Get the requiredDocIncome
        restRequiredDocIncomeMockMvc.perform(get("/api/required-doc-incomes/{id}", requiredDocIncome.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(requiredDocIncome.getId().intValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.path").value(DEFAULT_PATH));
    }
    @Test
    @Transactional
    public void getNonExistingRequiredDocIncome() throws Exception {
        // Get the requiredDocIncome
        restRequiredDocIncomeMockMvc.perform(get("/api/required-doc-incomes/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRequiredDocIncome() throws Exception {
        // Initialize the database
        requiredDocIncomeRepository.saveAndFlush(requiredDocIncome);

        int databaseSizeBeforeUpdate = requiredDocIncomeRepository.findAll().size();

        // Update the requiredDocIncome
        RequiredDocIncome updatedRequiredDocIncome = requiredDocIncomeRepository.findById(requiredDocIncome.getId()).get();
        // Disconnect from session so that the updates on updatedRequiredDocIncome are not directly saved in db
        em.detach(updatedRequiredDocIncome);
        updatedRequiredDocIncome
            .type(UPDATED_TYPE)
            .path(UPDATED_PATH);
        RequiredDocIncomeDTO requiredDocIncomeDTO = requiredDocIncomeMapper.toDto(updatedRequiredDocIncome);

        restRequiredDocIncomeMockMvc.perform(put("/api/required-doc-incomes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(requiredDocIncomeDTO)))
            .andExpect(status().isOk());

        // Validate the RequiredDocIncome in the database
        List<RequiredDocIncome> requiredDocIncomeList = requiredDocIncomeRepository.findAll();
        assertThat(requiredDocIncomeList).hasSize(databaseSizeBeforeUpdate);
        RequiredDocIncome testRequiredDocIncome = requiredDocIncomeList.get(requiredDocIncomeList.size() - 1);
        assertThat(testRequiredDocIncome.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testRequiredDocIncome.getPath()).isEqualTo(UPDATED_PATH);
    }

    @Test
    @Transactional
    public void updateNonExistingRequiredDocIncome() throws Exception {
        int databaseSizeBeforeUpdate = requiredDocIncomeRepository.findAll().size();

        // Create the RequiredDocIncome
        RequiredDocIncomeDTO requiredDocIncomeDTO = requiredDocIncomeMapper.toDto(requiredDocIncome);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRequiredDocIncomeMockMvc.perform(put("/api/required-doc-incomes")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(requiredDocIncomeDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RequiredDocIncome in the database
        List<RequiredDocIncome> requiredDocIncomeList = requiredDocIncomeRepository.findAll();
        assertThat(requiredDocIncomeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRequiredDocIncome() throws Exception {
        // Initialize the database
        requiredDocIncomeRepository.saveAndFlush(requiredDocIncome);

        int databaseSizeBeforeDelete = requiredDocIncomeRepository.findAll().size();

        // Delete the requiredDocIncome
        restRequiredDocIncomeMockMvc.perform(delete("/api/required-doc-incomes/{id}", requiredDocIncome.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RequiredDocIncome> requiredDocIncomeList = requiredDocIncomeRepository.findAll();
        assertThat(requiredDocIncomeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
