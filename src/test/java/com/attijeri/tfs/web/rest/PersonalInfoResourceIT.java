package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsBackendApp;
import com.attijeri.tfs.domain.PersonalInfo;
import com.attijeri.tfs.repository.PersonalInfoRepository;
import com.attijeri.tfs.service.PersonalInfoService;
import com.attijeri.tfs.service.dto.PersonalInfoDTO;
import com.attijeri.tfs.service.mapper.PersonalInfoMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.attijeri.tfs.domain.enumeration.Civility;
/**
 * Integration tests for the {@link PersonalInfoResource} REST controller.
 */
@SpringBootTest(classes = TfsBackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class PersonalInfoResourceIT {

    private static final Civility DEFAULT_CIVILITY = Civility.MADAME;
    private static final Civility UPDATED_CIVILITY = Civility.MONSIEUR;

    private static final String DEFAULT_FIRST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_FIRST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_LAST_NAME = "AAAAAAAAAA";
    private static final String UPDATED_LAST_NAME = "BBBBBBBBBB";

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_NATIVE_COUNTRY = "AAAAAAAAAA";
    private static final String UPDATED_NATIVE_COUNTRY = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_BIRTHDAY = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_BIRTHDAY = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_CLIENT_ABT = false;
    private static final Boolean UPDATED_CLIENT_ABT = true;

    private static final String DEFAULT_RIB = "AAAAAAAAAA";
    private static final String UPDATED_RIB = "BBBBBBBBBB";

    private static final String DEFAULT_NATIONALITY = "AAAAAAAAAA";
    private static final String UPDATED_NATIONALITY = "BBBBBBBBBB";

    private static final String DEFAULT_SECOND_NATIONALITY = "AAAAAAAAAA";
    private static final String UPDATED_SECOND_NATIONALITY = "BBBBBBBBBB";

    private static final Integer DEFAULT_NBRKIDS = 1;
    private static final Integer UPDATED_NBRKIDS = 2;

    private static final String DEFAULT_MARITAL_STATUS = "AAAAAAAAAA";
    private static final String UPDATED_MARITAL_STATUS = "BBBBBBBBBB";

    private static final String DEFAULT_PHONE = "AAAAAAAAAA";
    private static final String UPDATED_PHONE = "BBBBBBBBBB";

    private static final Boolean DEFAULT_AMERICAN_INDEX = false;
    private static final Boolean UPDATED_AMERICAN_INDEX = true;

    @Autowired
    private PersonalInfoRepository personalInfoRepository;

    @Autowired
    private PersonalInfoMapper personalInfoMapper;

    @Autowired
    private PersonalInfoService personalInfoService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restPersonalInfoMockMvc;

    private PersonalInfo personalInfo;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PersonalInfo createEntity(EntityManager em) {
        PersonalInfo personalInfo = new PersonalInfo()
            .civility(DEFAULT_CIVILITY)
            .firstName(DEFAULT_FIRST_NAME)
            .lastName(DEFAULT_LAST_NAME)
            .email(DEFAULT_EMAIL)
            .nativeCountry(DEFAULT_NATIVE_COUNTRY)
            .birthday(DEFAULT_BIRTHDAY)
            .clientABT(DEFAULT_CLIENT_ABT)
            .rib(DEFAULT_RIB)
            .nationality(DEFAULT_NATIONALITY)
            .secondNationality(DEFAULT_SECOND_NATIONALITY)
            .nbrkids(DEFAULT_NBRKIDS)
            .maritalStatus(DEFAULT_MARITAL_STATUS)
            .phone(DEFAULT_PHONE)
            .americanIndex(DEFAULT_AMERICAN_INDEX);
        return personalInfo;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static PersonalInfo createUpdatedEntity(EntityManager em) {
        PersonalInfo personalInfo = new PersonalInfo()
            .civility(UPDATED_CIVILITY)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .email(UPDATED_EMAIL)
            .nativeCountry(UPDATED_NATIVE_COUNTRY)
            .birthday(UPDATED_BIRTHDAY)
            .clientABT(UPDATED_CLIENT_ABT)
            .rib(UPDATED_RIB)
            .nationality(UPDATED_NATIONALITY)
            .secondNationality(UPDATED_SECOND_NATIONALITY)
            .nbrkids(UPDATED_NBRKIDS)
            .maritalStatus(UPDATED_MARITAL_STATUS)
            .phone(UPDATED_PHONE)
            .americanIndex(UPDATED_AMERICAN_INDEX);
        return personalInfo;
    }

    @BeforeEach
    public void initTest() {
        personalInfo = createEntity(em);
    }

    @Test
    @Transactional
    public void createPersonalInfo() throws Exception {
        int databaseSizeBeforeCreate = personalInfoRepository.findAll().size();
        // Create the PersonalInfo
        PersonalInfoDTO personalInfoDTO = personalInfoMapper.toDto(personalInfo);
        restPersonalInfoMockMvc.perform(post("/api/personal-infos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(personalInfoDTO)))
            .andExpect(status().isCreated());

        // Validate the PersonalInfo in the database
        List<PersonalInfo> personalInfoList = personalInfoRepository.findAll();
        assertThat(personalInfoList).hasSize(databaseSizeBeforeCreate + 1);
        PersonalInfo testPersonalInfo = personalInfoList.get(personalInfoList.size() - 1);
        assertThat(testPersonalInfo.getCivility()).isEqualTo(DEFAULT_CIVILITY);
        assertThat(testPersonalInfo.getFirstName()).isEqualTo(DEFAULT_FIRST_NAME);
        assertThat(testPersonalInfo.getLastName()).isEqualTo(DEFAULT_LAST_NAME);
        assertThat(testPersonalInfo.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testPersonalInfo.getNativeCountry()).isEqualTo(DEFAULT_NATIVE_COUNTRY);
        assertThat(testPersonalInfo.getBirthday()).isEqualTo(DEFAULT_BIRTHDAY);
        assertThat(testPersonalInfo.isClientABT()).isEqualTo(DEFAULT_CLIENT_ABT);
        assertThat(testPersonalInfo.getRib()).isEqualTo(DEFAULT_RIB);
        assertThat(testPersonalInfo.getNationality()).isEqualTo(DEFAULT_NATIONALITY);
        assertThat(testPersonalInfo.getSecondNationality()).isEqualTo(DEFAULT_SECOND_NATIONALITY);
        assertThat(testPersonalInfo.getNbrkids()).isEqualTo(DEFAULT_NBRKIDS);
        assertThat(testPersonalInfo.getMaritalStatus()).isEqualTo(DEFAULT_MARITAL_STATUS);
        assertThat(testPersonalInfo.getPhone()).isEqualTo(DEFAULT_PHONE);
        assertThat(testPersonalInfo.isAmericanIndex()).isEqualTo(DEFAULT_AMERICAN_INDEX);
    }

    @Test
    @Transactional
    public void createPersonalInfoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = personalInfoRepository.findAll().size();

        // Create the PersonalInfo with an existing ID
        personalInfo.setId(1L);
        PersonalInfoDTO personalInfoDTO = personalInfoMapper.toDto(personalInfo);

        // An entity with an existing ID cannot be created, so this API call must fail
        restPersonalInfoMockMvc.perform(post("/api/personal-infos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(personalInfoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PersonalInfo in the database
        List<PersonalInfo> personalInfoList = personalInfoRepository.findAll();
        assertThat(personalInfoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllPersonalInfos() throws Exception {
        // Initialize the database
        personalInfoRepository.saveAndFlush(personalInfo);

        // Get all the personalInfoList
        restPersonalInfoMockMvc.perform(get("/api/personal-infos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(personalInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].civility").value(hasItem(DEFAULT_CIVILITY.toString())))
            .andExpect(jsonPath("$.[*].firstName").value(hasItem(DEFAULT_FIRST_NAME)))
            .andExpect(jsonPath("$.[*].lastName").value(hasItem(DEFAULT_LAST_NAME)))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].nativeCountry").value(hasItem(DEFAULT_NATIVE_COUNTRY)))
            .andExpect(jsonPath("$.[*].birthday").value(hasItem(DEFAULT_BIRTHDAY.toString())))
            .andExpect(jsonPath("$.[*].clientABT").value(hasItem(DEFAULT_CLIENT_ABT.booleanValue())))
            .andExpect(jsonPath("$.[*].rib").value(hasItem(DEFAULT_RIB)))
            .andExpect(jsonPath("$.[*].nationality").value(hasItem(DEFAULT_NATIONALITY)))
            .andExpect(jsonPath("$.[*].secondNationality").value(hasItem(DEFAULT_SECOND_NATIONALITY)))
            .andExpect(jsonPath("$.[*].nbrkids").value(hasItem(DEFAULT_NBRKIDS)))
            .andExpect(jsonPath("$.[*].maritalStatus").value(hasItem(DEFAULT_MARITAL_STATUS)))
            .andExpect(jsonPath("$.[*].phone").value(hasItem(DEFAULT_PHONE)))
            .andExpect(jsonPath("$.[*].americanIndex").value(hasItem(DEFAULT_AMERICAN_INDEX.booleanValue())));
    }
    
    @Test
    @Transactional
    public void getPersonalInfo() throws Exception {
        // Initialize the database
        personalInfoRepository.saveAndFlush(personalInfo);

        // Get the personalInfo
        restPersonalInfoMockMvc.perform(get("/api/personal-infos/{id}", personalInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(personalInfo.getId().intValue()))
            .andExpect(jsonPath("$.civility").value(DEFAULT_CIVILITY.toString()))
            .andExpect(jsonPath("$.firstName").value(DEFAULT_FIRST_NAME))
            .andExpect(jsonPath("$.lastName").value(DEFAULT_LAST_NAME))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.nativeCountry").value(DEFAULT_NATIVE_COUNTRY))
            .andExpect(jsonPath("$.birthday").value(DEFAULT_BIRTHDAY.toString()))
            .andExpect(jsonPath("$.clientABT").value(DEFAULT_CLIENT_ABT.booleanValue()))
            .andExpect(jsonPath("$.rib").value(DEFAULT_RIB))
            .andExpect(jsonPath("$.nationality").value(DEFAULT_NATIONALITY))
            .andExpect(jsonPath("$.secondNationality").value(DEFAULT_SECOND_NATIONALITY))
            .andExpect(jsonPath("$.nbrkids").value(DEFAULT_NBRKIDS))
            .andExpect(jsonPath("$.maritalStatus").value(DEFAULT_MARITAL_STATUS))
            .andExpect(jsonPath("$.phone").value(DEFAULT_PHONE))
            .andExpect(jsonPath("$.americanIndex").value(DEFAULT_AMERICAN_INDEX.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingPersonalInfo() throws Exception {
        // Get the personalInfo
        restPersonalInfoMockMvc.perform(get("/api/personal-infos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updatePersonalInfo() throws Exception {
        // Initialize the database
        personalInfoRepository.saveAndFlush(personalInfo);

        int databaseSizeBeforeUpdate = personalInfoRepository.findAll().size();

        // Update the personalInfo
        PersonalInfo updatedPersonalInfo = personalInfoRepository.findById(personalInfo.getId()).get();
        // Disconnect from session so that the updates on updatedPersonalInfo are not directly saved in db
        em.detach(updatedPersonalInfo);
        updatedPersonalInfo
            .civility(UPDATED_CIVILITY)
            .firstName(UPDATED_FIRST_NAME)
            .lastName(UPDATED_LAST_NAME)
            .email(UPDATED_EMAIL)
            .nativeCountry(UPDATED_NATIVE_COUNTRY)
            .birthday(UPDATED_BIRTHDAY)
            .clientABT(UPDATED_CLIENT_ABT)
            .rib(UPDATED_RIB)
            .nationality(UPDATED_NATIONALITY)
            .secondNationality(UPDATED_SECOND_NATIONALITY)
            .nbrkids(UPDATED_NBRKIDS)
            .maritalStatus(UPDATED_MARITAL_STATUS)
            .phone(UPDATED_PHONE)
            .americanIndex(UPDATED_AMERICAN_INDEX);
        PersonalInfoDTO personalInfoDTO = personalInfoMapper.toDto(updatedPersonalInfo);

        restPersonalInfoMockMvc.perform(put("/api/personal-infos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(personalInfoDTO)))
            .andExpect(status().isOk());

        // Validate the PersonalInfo in the database
        List<PersonalInfo> personalInfoList = personalInfoRepository.findAll();
        assertThat(personalInfoList).hasSize(databaseSizeBeforeUpdate);
        PersonalInfo testPersonalInfo = personalInfoList.get(personalInfoList.size() - 1);
        assertThat(testPersonalInfo.getCivility()).isEqualTo(UPDATED_CIVILITY);
        assertThat(testPersonalInfo.getFirstName()).isEqualTo(UPDATED_FIRST_NAME);
        assertThat(testPersonalInfo.getLastName()).isEqualTo(UPDATED_LAST_NAME);
        assertThat(testPersonalInfo.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testPersonalInfo.getNativeCountry()).isEqualTo(UPDATED_NATIVE_COUNTRY);
        assertThat(testPersonalInfo.getBirthday()).isEqualTo(UPDATED_BIRTHDAY);
        assertThat(testPersonalInfo.isClientABT()).isEqualTo(UPDATED_CLIENT_ABT);
        assertThat(testPersonalInfo.getRib()).isEqualTo(UPDATED_RIB);
        assertThat(testPersonalInfo.getNationality()).isEqualTo(UPDATED_NATIONALITY);
        assertThat(testPersonalInfo.getSecondNationality()).isEqualTo(UPDATED_SECOND_NATIONALITY);
        assertThat(testPersonalInfo.getNbrkids()).isEqualTo(UPDATED_NBRKIDS);
        assertThat(testPersonalInfo.getMaritalStatus()).isEqualTo(UPDATED_MARITAL_STATUS);
        assertThat(testPersonalInfo.getPhone()).isEqualTo(UPDATED_PHONE);
        assertThat(testPersonalInfo.isAmericanIndex()).isEqualTo(UPDATED_AMERICAN_INDEX);
    }

    @Test
    @Transactional
    public void updateNonExistingPersonalInfo() throws Exception {
        int databaseSizeBeforeUpdate = personalInfoRepository.findAll().size();

        // Create the PersonalInfo
        PersonalInfoDTO personalInfoDTO = personalInfoMapper.toDto(personalInfo);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restPersonalInfoMockMvc.perform(put("/api/personal-infos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(personalInfoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the PersonalInfo in the database
        List<PersonalInfo> personalInfoList = personalInfoRepository.findAll();
        assertThat(personalInfoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deletePersonalInfo() throws Exception {
        // Initialize the database
        personalInfoRepository.saveAndFlush(personalInfo);

        int databaseSizeBeforeDelete = personalInfoRepository.findAll().size();

        // Delete the personalInfo
        restPersonalInfoMockMvc.perform(delete("/api/personal-infos/{id}", personalInfo.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<PersonalInfo> personalInfoList = personalInfoRepository.findAll();
        assertThat(personalInfoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
