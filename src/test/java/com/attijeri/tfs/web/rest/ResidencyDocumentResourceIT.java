package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsBackendApp;
import com.attijeri.tfs.domain.ResidencyDocument;
import com.attijeri.tfs.repository.ResidencyDocumentRepository;
import com.attijeri.tfs.service.ResidencyDocumentService;
import com.attijeri.tfs.service.dto.ResidencyDocumentDTO;
import com.attijeri.tfs.service.mapper.ResidencyDocumentMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link ResidencyDocumentResource} REST controller.
 */
@SpringBootTest(classes = TfsBackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class ResidencyDocumentResourceIT {

    private static final String DEFAULT_LABEL_FR = "AAAAAAAAAA";
    private static final String UPDATED_LABEL_FR = "BBBBBBBBBB";

    private static final String DEFAULT_LABEL_EN = "AAAAAAAAAA";
    private static final String UPDATED_LABEL_EN = "BBBBBBBBBB";

    @Autowired
    private ResidencyDocumentRepository residencyDocumentRepository;

    @Autowired
    private ResidencyDocumentMapper residencyDocumentMapper;

    @Autowired
    private ResidencyDocumentService residencyDocumentService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restResidencyDocumentMockMvc;

    private ResidencyDocument residencyDocument;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ResidencyDocument createEntity(EntityManager em) {
        ResidencyDocument residencyDocument = new ResidencyDocument()
            .labelFR(DEFAULT_LABEL_FR)
            .labelEN(DEFAULT_LABEL_EN);
        return residencyDocument;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ResidencyDocument createUpdatedEntity(EntityManager em) {
        ResidencyDocument residencyDocument = new ResidencyDocument()
            .labelFR(UPDATED_LABEL_FR)
            .labelEN(UPDATED_LABEL_EN);
        return residencyDocument;
    }

    @BeforeEach
    public void initTest() {
        residencyDocument = createEntity(em);
    }

    @Test
    @Transactional
    public void createResidencyDocument() throws Exception {
        int databaseSizeBeforeCreate = residencyDocumentRepository.findAll().size();
        // Create the ResidencyDocument
        ResidencyDocumentDTO residencyDocumentDTO = residencyDocumentMapper.toDto(residencyDocument);
        restResidencyDocumentMockMvc.perform(post("/api/residency-documents")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(residencyDocumentDTO)))
            .andExpect(status().isCreated());

        // Validate the ResidencyDocument in the database
        List<ResidencyDocument> residencyDocumentList = residencyDocumentRepository.findAll();
        assertThat(residencyDocumentList).hasSize(databaseSizeBeforeCreate + 1);
        ResidencyDocument testResidencyDocument = residencyDocumentList.get(residencyDocumentList.size() - 1);
        assertThat(testResidencyDocument.getLabelFR()).isEqualTo(DEFAULT_LABEL_FR);
        assertThat(testResidencyDocument.getLabelEN()).isEqualTo(DEFAULT_LABEL_EN);
    }

    @Test
    @Transactional
    public void createResidencyDocumentWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = residencyDocumentRepository.findAll().size();

        // Create the ResidencyDocument with an existing ID
        residencyDocument.setId(1L);
        ResidencyDocumentDTO residencyDocumentDTO = residencyDocumentMapper.toDto(residencyDocument);

        // An entity with an existing ID cannot be created, so this API call must fail
        restResidencyDocumentMockMvc.perform(post("/api/residency-documents")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(residencyDocumentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ResidencyDocument in the database
        List<ResidencyDocument> residencyDocumentList = residencyDocumentRepository.findAll();
        assertThat(residencyDocumentList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllResidencyDocuments() throws Exception {
        // Initialize the database
        residencyDocumentRepository.saveAndFlush(residencyDocument);

        // Get all the residencyDocumentList
        restResidencyDocumentMockMvc.perform(get("/api/residency-documents?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(residencyDocument.getId().intValue())))
            .andExpect(jsonPath("$.[*].labelFR").value(hasItem(DEFAULT_LABEL_FR)))
            .andExpect(jsonPath("$.[*].labelEN").value(hasItem(DEFAULT_LABEL_EN)));
    }
    
    @Test
    @Transactional
    public void getResidencyDocument() throws Exception {
        // Initialize the database
        residencyDocumentRepository.saveAndFlush(residencyDocument);

        // Get the residencyDocument
        restResidencyDocumentMockMvc.perform(get("/api/residency-documents/{id}", residencyDocument.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(residencyDocument.getId().intValue()))
            .andExpect(jsonPath("$.labelFR").value(DEFAULT_LABEL_FR))
            .andExpect(jsonPath("$.labelEN").value(DEFAULT_LABEL_EN));
    }
    @Test
    @Transactional
    public void getNonExistingResidencyDocument() throws Exception {
        // Get the residencyDocument
        restResidencyDocumentMockMvc.perform(get("/api/residency-documents/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateResidencyDocument() throws Exception {
        // Initialize the database
        residencyDocumentRepository.saveAndFlush(residencyDocument);

        int databaseSizeBeforeUpdate = residencyDocumentRepository.findAll().size();

        // Update the residencyDocument
        ResidencyDocument updatedResidencyDocument = residencyDocumentRepository.findById(residencyDocument.getId()).get();
        // Disconnect from session so that the updates on updatedResidencyDocument are not directly saved in db
        em.detach(updatedResidencyDocument);
        updatedResidencyDocument
            .labelFR(UPDATED_LABEL_FR)
            .labelEN(UPDATED_LABEL_EN);
        ResidencyDocumentDTO residencyDocumentDTO = residencyDocumentMapper.toDto(updatedResidencyDocument);

        restResidencyDocumentMockMvc.perform(put("/api/residency-documents")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(residencyDocumentDTO)))
            .andExpect(status().isOk());

        // Validate the ResidencyDocument in the database
        List<ResidencyDocument> residencyDocumentList = residencyDocumentRepository.findAll();
        assertThat(residencyDocumentList).hasSize(databaseSizeBeforeUpdate);
        ResidencyDocument testResidencyDocument = residencyDocumentList.get(residencyDocumentList.size() - 1);
        assertThat(testResidencyDocument.getLabelFR()).isEqualTo(UPDATED_LABEL_FR);
        assertThat(testResidencyDocument.getLabelEN()).isEqualTo(UPDATED_LABEL_EN);
    }

    @Test
    @Transactional
    public void updateNonExistingResidencyDocument() throws Exception {
        int databaseSizeBeforeUpdate = residencyDocumentRepository.findAll().size();

        // Create the ResidencyDocument
        ResidencyDocumentDTO residencyDocumentDTO = residencyDocumentMapper.toDto(residencyDocument);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restResidencyDocumentMockMvc.perform(put("/api/residency-documents")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(residencyDocumentDTO)))
            .andExpect(status().isBadRequest());

        // Validate the ResidencyDocument in the database
        List<ResidencyDocument> residencyDocumentList = residencyDocumentRepository.findAll();
        assertThat(residencyDocumentList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteResidencyDocument() throws Exception {
        // Initialize the database
        residencyDocumentRepository.saveAndFlush(residencyDocument);

        int databaseSizeBeforeDelete = residencyDocumentRepository.findAll().size();

        // Delete the residencyDocument
        restResidencyDocumentMockMvc.perform(delete("/api/residency-documents/{id}", residencyDocument.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ResidencyDocument> residencyDocumentList = residencyDocumentRepository.findAll();
        assertThat(residencyDocumentList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
