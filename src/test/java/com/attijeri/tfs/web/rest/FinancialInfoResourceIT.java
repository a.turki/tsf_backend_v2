package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsBackendApp;
import com.attijeri.tfs.domain.FinancialInfo;
import com.attijeri.tfs.repository.FinancialInfoRepository;
import com.attijeri.tfs.service.FinancialInfoService;
import com.attijeri.tfs.service.dto.FinancialInfoDTO;
import com.attijeri.tfs.service.mapper.FinancialInfoMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link FinancialInfoResource} REST controller.
 */
@SpringBootTest(classes = TfsBackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class FinancialInfoResourceIT {

    @Autowired
    private FinancialInfoRepository financialInfoRepository;

    @Autowired
    private FinancialInfoMapper financialInfoMapper;

    @Autowired
    private FinancialInfoService financialInfoService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFinancialInfoMockMvc;

    private FinancialInfo financialInfo;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FinancialInfo createEntity(EntityManager em) {
        FinancialInfo financialInfo = new FinancialInfo();
        return financialInfo;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static FinancialInfo createUpdatedEntity(EntityManager em) {
        FinancialInfo financialInfo = new FinancialInfo();
        return financialInfo;
    }

    @BeforeEach
    public void initTest() {
        financialInfo = createEntity(em);
    }

    @Test
    @Transactional
    public void createFinancialInfo() throws Exception {
        int databaseSizeBeforeCreate = financialInfoRepository.findAll().size();
        // Create the FinancialInfo
        FinancialInfoDTO financialInfoDTO = financialInfoMapper.toDto(financialInfo);
        restFinancialInfoMockMvc.perform(post("/api/financial-infos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(financialInfoDTO)))
            .andExpect(status().isCreated());

        // Validate the FinancialInfo in the database
        List<FinancialInfo> financialInfoList = financialInfoRepository.findAll();
        assertThat(financialInfoList).hasSize(databaseSizeBeforeCreate + 1);
        FinancialInfo testFinancialInfo = financialInfoList.get(financialInfoList.size() - 1);
    }

    @Test
    @Transactional
    public void createFinancialInfoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = financialInfoRepository.findAll().size();

        // Create the FinancialInfo with an existing ID
        financialInfo.setId(1L);
        FinancialInfoDTO financialInfoDTO = financialInfoMapper.toDto(financialInfo);

        // An entity with an existing ID cannot be created, so this API call must fail
        restFinancialInfoMockMvc.perform(post("/api/financial-infos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(financialInfoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FinancialInfo in the database
        List<FinancialInfo> financialInfoList = financialInfoRepository.findAll();
        assertThat(financialInfoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllFinancialInfos() throws Exception {
        // Initialize the database
        financialInfoRepository.saveAndFlush(financialInfo);

        // Get all the financialInfoList
        restFinancialInfoMockMvc.perform(get("/api/financial-infos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(financialInfo.getId().intValue())));
    }
    
    @Test
    @Transactional
    public void getFinancialInfo() throws Exception {
        // Initialize the database
        financialInfoRepository.saveAndFlush(financialInfo);

        // Get the financialInfo
        restFinancialInfoMockMvc.perform(get("/api/financial-infos/{id}", financialInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(financialInfo.getId().intValue()));
    }
    @Test
    @Transactional
    public void getNonExistingFinancialInfo() throws Exception {
        // Get the financialInfo
        restFinancialInfoMockMvc.perform(get("/api/financial-infos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateFinancialInfo() throws Exception {
        // Initialize the database
        financialInfoRepository.saveAndFlush(financialInfo);

        int databaseSizeBeforeUpdate = financialInfoRepository.findAll().size();

        // Update the financialInfo
        FinancialInfo updatedFinancialInfo = financialInfoRepository.findById(financialInfo.getId()).get();
        // Disconnect from session so that the updates on updatedFinancialInfo are not directly saved in db
        em.detach(updatedFinancialInfo);
        FinancialInfoDTO financialInfoDTO = financialInfoMapper.toDto(updatedFinancialInfo);

        restFinancialInfoMockMvc.perform(put("/api/financial-infos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(financialInfoDTO)))
            .andExpect(status().isOk());

        // Validate the FinancialInfo in the database
        List<FinancialInfo> financialInfoList = financialInfoRepository.findAll();
        assertThat(financialInfoList).hasSize(databaseSizeBeforeUpdate);
        FinancialInfo testFinancialInfo = financialInfoList.get(financialInfoList.size() - 1);
    }

    @Test
    @Transactional
    public void updateNonExistingFinancialInfo() throws Exception {
        int databaseSizeBeforeUpdate = financialInfoRepository.findAll().size();

        // Create the FinancialInfo
        FinancialInfoDTO financialInfoDTO = financialInfoMapper.toDto(financialInfo);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFinancialInfoMockMvc.perform(put("/api/financial-infos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(financialInfoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the FinancialInfo in the database
        List<FinancialInfo> financialInfoList = financialInfoRepository.findAll();
        assertThat(financialInfoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteFinancialInfo() throws Exception {
        // Initialize the database
        financialInfoRepository.saveAndFlush(financialInfo);

        int databaseSizeBeforeDelete = financialInfoRepository.findAll().size();

        // Delete the financialInfo
        restFinancialInfoMockMvc.perform(delete("/api/financial-infos/{id}", financialInfo.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<FinancialInfo> financialInfoList = financialInfoRepository.findAll();
        assertThat(financialInfoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
