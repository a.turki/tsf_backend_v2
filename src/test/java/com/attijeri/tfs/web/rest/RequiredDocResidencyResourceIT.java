package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsBackendApp;
import com.attijeri.tfs.domain.RequiredDocResidency;
import com.attijeri.tfs.repository.RequiredDocResidencyRepository;
import com.attijeri.tfs.service.RequiredDocResidencyService;
import com.attijeri.tfs.service.dto.RequiredDocResidencyDTO;
import com.attijeri.tfs.service.mapper.RequiredDocResidencyMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link RequiredDocResidencyResource} REST controller.
 */
@SpringBootTest(classes = TfsBackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class RequiredDocResidencyResourceIT {

    private static final String DEFAULT_TYPE = "AAAAAAAAAA";
    private static final String UPDATED_TYPE = "BBBBBBBBBB";

    private static final String DEFAULT_NUM = "AAAAAAAAAA";
    private static final String UPDATED_NUM = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DELIVERY_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DELIVERY_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final LocalDate DEFAULT_EXPERATION_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_EXPERATION_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_ILLIMITED_EXPERATION_DATE = false;
    private static final Boolean UPDATED_ILLIMITED_EXPERATION_DATE = true;

    private static final String DEFAULT_RESIDENCY_RECTO = "AAAAAAAAAA";
    private static final String UPDATED_RESIDENCY_RECTO = "BBBBBBBBBB";

    private static final String DEFAULT_RESIDENCY_VERSO = "AAAAAAAAAA";
    private static final String UPDATED_RESIDENCY_VERSO = "BBBBBBBBBB";

    @Autowired
    private RequiredDocResidencyRepository requiredDocResidencyRepository;

    @Autowired
    private RequiredDocResidencyMapper requiredDocResidencyMapper;

    @Autowired
    private RequiredDocResidencyService requiredDocResidencyService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRequiredDocResidencyMockMvc;

    private RequiredDocResidency requiredDocResidency;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RequiredDocResidency createEntity(EntityManager em) {
        RequiredDocResidency requiredDocResidency = new RequiredDocResidency()
            .type(DEFAULT_TYPE)
            .num(DEFAULT_NUM)
            .deliveryDate(DEFAULT_DELIVERY_DATE)
            .experationDate(DEFAULT_EXPERATION_DATE)
            .illimitedExperationDate(DEFAULT_ILLIMITED_EXPERATION_DATE)
            .residencyRecto(DEFAULT_RESIDENCY_RECTO)
            .residencyVerso(DEFAULT_RESIDENCY_VERSO);
        return requiredDocResidency;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RequiredDocResidency createUpdatedEntity(EntityManager em) {
        RequiredDocResidency requiredDocResidency = new RequiredDocResidency()
            .type(UPDATED_TYPE)
            .num(UPDATED_NUM)
            .deliveryDate(UPDATED_DELIVERY_DATE)
            .experationDate(UPDATED_EXPERATION_DATE)
            .illimitedExperationDate(UPDATED_ILLIMITED_EXPERATION_DATE)
            .residencyRecto(UPDATED_RESIDENCY_RECTO)
            .residencyVerso(UPDATED_RESIDENCY_VERSO);
        return requiredDocResidency;
    }

    @BeforeEach
    public void initTest() {
        requiredDocResidency = createEntity(em);
    }

    @Test
    @Transactional
    public void createRequiredDocResidency() throws Exception {
        int databaseSizeBeforeCreate = requiredDocResidencyRepository.findAll().size();
        // Create the RequiredDocResidency
        RequiredDocResidencyDTO requiredDocResidencyDTO = requiredDocResidencyMapper.toDto(requiredDocResidency);
        restRequiredDocResidencyMockMvc.perform(post("/api/required-doc-residencies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(requiredDocResidencyDTO)))
            .andExpect(status().isCreated());

        // Validate the RequiredDocResidency in the database
        List<RequiredDocResidency> requiredDocResidencyList = requiredDocResidencyRepository.findAll();
        assertThat(requiredDocResidencyList).hasSize(databaseSizeBeforeCreate + 1);
        RequiredDocResidency testRequiredDocResidency = requiredDocResidencyList.get(requiredDocResidencyList.size() - 1);
        assertThat(testRequiredDocResidency.getType()).isEqualTo(DEFAULT_TYPE);
        assertThat(testRequiredDocResidency.getNum()).isEqualTo(DEFAULT_NUM);
        assertThat(testRequiredDocResidency.getDeliveryDate()).isEqualTo(DEFAULT_DELIVERY_DATE);
        assertThat(testRequiredDocResidency.getExperationDate()).isEqualTo(DEFAULT_EXPERATION_DATE);
        assertThat(testRequiredDocResidency.isIllimitedExperationDate()).isEqualTo(DEFAULT_ILLIMITED_EXPERATION_DATE);
        assertThat(testRequiredDocResidency.getResidencyRecto()).isEqualTo(DEFAULT_RESIDENCY_RECTO);
        assertThat(testRequiredDocResidency.getResidencyVerso()).isEqualTo(DEFAULT_RESIDENCY_VERSO);
    }

    @Test
    @Transactional
    public void createRequiredDocResidencyWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = requiredDocResidencyRepository.findAll().size();

        // Create the RequiredDocResidency with an existing ID
        requiredDocResidency.setId(1L);
        RequiredDocResidencyDTO requiredDocResidencyDTO = requiredDocResidencyMapper.toDto(requiredDocResidency);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRequiredDocResidencyMockMvc.perform(post("/api/required-doc-residencies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(requiredDocResidencyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RequiredDocResidency in the database
        List<RequiredDocResidency> requiredDocResidencyList = requiredDocResidencyRepository.findAll();
        assertThat(requiredDocResidencyList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllRequiredDocResidencies() throws Exception {
        // Initialize the database
        requiredDocResidencyRepository.saveAndFlush(requiredDocResidency);

        // Get all the requiredDocResidencyList
        restRequiredDocResidencyMockMvc.perform(get("/api/required-doc-residencies?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(requiredDocResidency.getId().intValue())))
            .andExpect(jsonPath("$.[*].type").value(hasItem(DEFAULT_TYPE)))
            .andExpect(jsonPath("$.[*].num").value(hasItem(DEFAULT_NUM)))
            .andExpect(jsonPath("$.[*].deliveryDate").value(hasItem(DEFAULT_DELIVERY_DATE.toString())))
            .andExpect(jsonPath("$.[*].experationDate").value(hasItem(DEFAULT_EXPERATION_DATE.toString())))
            .andExpect(jsonPath("$.[*].illimitedExperationDate").value(hasItem(DEFAULT_ILLIMITED_EXPERATION_DATE.booleanValue())))
            .andExpect(jsonPath("$.[*].residencyRecto").value(hasItem(DEFAULT_RESIDENCY_RECTO)))
            .andExpect(jsonPath("$.[*].residencyVerso").value(hasItem(DEFAULT_RESIDENCY_VERSO)));
    }
    
    @Test
    @Transactional
    public void getRequiredDocResidency() throws Exception {
        // Initialize the database
        requiredDocResidencyRepository.saveAndFlush(requiredDocResidency);

        // Get the requiredDocResidency
        restRequiredDocResidencyMockMvc.perform(get("/api/required-doc-residencies/{id}", requiredDocResidency.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(requiredDocResidency.getId().intValue()))
            .andExpect(jsonPath("$.type").value(DEFAULT_TYPE))
            .andExpect(jsonPath("$.num").value(DEFAULT_NUM))
            .andExpect(jsonPath("$.deliveryDate").value(DEFAULT_DELIVERY_DATE.toString()))
            .andExpect(jsonPath("$.experationDate").value(DEFAULT_EXPERATION_DATE.toString()))
            .andExpect(jsonPath("$.illimitedExperationDate").value(DEFAULT_ILLIMITED_EXPERATION_DATE.booleanValue()))
            .andExpect(jsonPath("$.residencyRecto").value(DEFAULT_RESIDENCY_RECTO))
            .andExpect(jsonPath("$.residencyVerso").value(DEFAULT_RESIDENCY_VERSO));
    }
    @Test
    @Transactional
    public void getNonExistingRequiredDocResidency() throws Exception {
        // Get the requiredDocResidency
        restRequiredDocResidencyMockMvc.perform(get("/api/required-doc-residencies/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRequiredDocResidency() throws Exception {
        // Initialize the database
        requiredDocResidencyRepository.saveAndFlush(requiredDocResidency);

        int databaseSizeBeforeUpdate = requiredDocResidencyRepository.findAll().size();

        // Update the requiredDocResidency
        RequiredDocResidency updatedRequiredDocResidency = requiredDocResidencyRepository.findById(requiredDocResidency.getId()).get();
        // Disconnect from session so that the updates on updatedRequiredDocResidency are not directly saved in db
        em.detach(updatedRequiredDocResidency);
        updatedRequiredDocResidency
            .type(UPDATED_TYPE)
            .num(UPDATED_NUM)
            .deliveryDate(UPDATED_DELIVERY_DATE)
            .experationDate(UPDATED_EXPERATION_DATE)
            .illimitedExperationDate(UPDATED_ILLIMITED_EXPERATION_DATE)
            .residencyRecto(UPDATED_RESIDENCY_RECTO)
            .residencyVerso(UPDATED_RESIDENCY_VERSO);
        RequiredDocResidencyDTO requiredDocResidencyDTO = requiredDocResidencyMapper.toDto(updatedRequiredDocResidency);

        restRequiredDocResidencyMockMvc.perform(put("/api/required-doc-residencies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(requiredDocResidencyDTO)))
            .andExpect(status().isOk());

        // Validate the RequiredDocResidency in the database
        List<RequiredDocResidency> requiredDocResidencyList = requiredDocResidencyRepository.findAll();
        assertThat(requiredDocResidencyList).hasSize(databaseSizeBeforeUpdate);
        RequiredDocResidency testRequiredDocResidency = requiredDocResidencyList.get(requiredDocResidencyList.size() - 1);
        assertThat(testRequiredDocResidency.getType()).isEqualTo(UPDATED_TYPE);
        assertThat(testRequiredDocResidency.getNum()).isEqualTo(UPDATED_NUM);
        assertThat(testRequiredDocResidency.getDeliveryDate()).isEqualTo(UPDATED_DELIVERY_DATE);
        assertThat(testRequiredDocResidency.getExperationDate()).isEqualTo(UPDATED_EXPERATION_DATE);
        assertThat(testRequiredDocResidency.isIllimitedExperationDate()).isEqualTo(UPDATED_ILLIMITED_EXPERATION_DATE);
        assertThat(testRequiredDocResidency.getResidencyRecto()).isEqualTo(UPDATED_RESIDENCY_RECTO);
        assertThat(testRequiredDocResidency.getResidencyVerso()).isEqualTo(UPDATED_RESIDENCY_VERSO);
    }

    @Test
    @Transactional
    public void updateNonExistingRequiredDocResidency() throws Exception {
        int databaseSizeBeforeUpdate = requiredDocResidencyRepository.findAll().size();

        // Create the RequiredDocResidency
        RequiredDocResidencyDTO requiredDocResidencyDTO = requiredDocResidencyMapper.toDto(requiredDocResidency);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRequiredDocResidencyMockMvc.perform(put("/api/required-doc-residencies")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(requiredDocResidencyDTO)))
            .andExpect(status().isBadRequest());

        // Validate the RequiredDocResidency in the database
        List<RequiredDocResidency> requiredDocResidencyList = requiredDocResidencyRepository.findAll();
        assertThat(requiredDocResidencyList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteRequiredDocResidency() throws Exception {
        // Initialize the database
        requiredDocResidencyRepository.saveAndFlush(requiredDocResidency);

        int databaseSizeBeforeDelete = requiredDocResidencyRepository.findAll().size();

        // Delete the requiredDocResidency
        restRequiredDocResidencyMockMvc.perform(delete("/api/required-doc-residencies/{id}", requiredDocResidency.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RequiredDocResidency> requiredDocResidencyList = requiredDocResidencyRepository.findAll();
        assertThat(requiredDocResidencyList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
