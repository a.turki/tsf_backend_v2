package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsBackendApp;
import com.attijeri.tfs.domain.OtherResidencyFile;
import com.attijeri.tfs.repository.OtherResidencyFileRepository;
import com.attijeri.tfs.service.OtherResidencyFileService;
import com.attijeri.tfs.service.dto.OtherResidencyFileDTO;
import com.attijeri.tfs.service.mapper.OtherResidencyFileMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OtherResidencyFileResource} REST controller.
 */
@SpringBootTest(classes = TfsBackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class OtherResidencyFileResourceIT {

    private static final String DEFAULT_PATH = "AAAAAAAAAA";
    private static final String UPDATED_PATH = "BBBBBBBBBB";

    @Autowired
    private OtherResidencyFileRepository otherResidencyFileRepository;

    @Autowired
    private OtherResidencyFileMapper otherResidencyFileMapper;

    @Autowired
    private OtherResidencyFileService otherResidencyFileService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOtherResidencyFileMockMvc;

    private OtherResidencyFile otherResidencyFile;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OtherResidencyFile createEntity(EntityManager em) {
        OtherResidencyFile otherResidencyFile = new OtherResidencyFile()
            .path(DEFAULT_PATH);
        return otherResidencyFile;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OtherResidencyFile createUpdatedEntity(EntityManager em) {
        OtherResidencyFile otherResidencyFile = new OtherResidencyFile()
            .path(UPDATED_PATH);
        return otherResidencyFile;
    }

    @BeforeEach
    public void initTest() {
        otherResidencyFile = createEntity(em);
    }

    @Test
    @Transactional
    public void createOtherResidencyFile() throws Exception {
        int databaseSizeBeforeCreate = otherResidencyFileRepository.findAll().size();
        // Create the OtherResidencyFile
        OtherResidencyFileDTO otherResidencyFileDTO = otherResidencyFileMapper.toDto(otherResidencyFile);
        restOtherResidencyFileMockMvc.perform(post("/api/other-residency-files")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(otherResidencyFileDTO)))
            .andExpect(status().isCreated());

        // Validate the OtherResidencyFile in the database
        List<OtherResidencyFile> otherResidencyFileList = otherResidencyFileRepository.findAll();
        assertThat(otherResidencyFileList).hasSize(databaseSizeBeforeCreate + 1);
        OtherResidencyFile testOtherResidencyFile = otherResidencyFileList.get(otherResidencyFileList.size() - 1);
        assertThat(testOtherResidencyFile.getPath()).isEqualTo(DEFAULT_PATH);
    }

    @Test
    @Transactional
    public void createOtherResidencyFileWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = otherResidencyFileRepository.findAll().size();

        // Create the OtherResidencyFile with an existing ID
        otherResidencyFile.setId(1L);
        OtherResidencyFileDTO otherResidencyFileDTO = otherResidencyFileMapper.toDto(otherResidencyFile);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOtherResidencyFileMockMvc.perform(post("/api/other-residency-files")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(otherResidencyFileDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OtherResidencyFile in the database
        List<OtherResidencyFile> otherResidencyFileList = otherResidencyFileRepository.findAll();
        assertThat(otherResidencyFileList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllOtherResidencyFiles() throws Exception {
        // Initialize the database
        otherResidencyFileRepository.saveAndFlush(otherResidencyFile);

        // Get all the otherResidencyFileList
        restOtherResidencyFileMockMvc.perform(get("/api/other-residency-files?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(otherResidencyFile.getId().intValue())))
            .andExpect(jsonPath("$.[*].path").value(hasItem(DEFAULT_PATH)));
    }
    
    @Test
    @Transactional
    public void getOtherResidencyFile() throws Exception {
        // Initialize the database
        otherResidencyFileRepository.saveAndFlush(otherResidencyFile);

        // Get the otherResidencyFile
        restOtherResidencyFileMockMvc.perform(get("/api/other-residency-files/{id}", otherResidencyFile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(otherResidencyFile.getId().intValue()))
            .andExpect(jsonPath("$.path").value(DEFAULT_PATH));
    }
    @Test
    @Transactional
    public void getNonExistingOtherResidencyFile() throws Exception {
        // Get the otherResidencyFile
        restOtherResidencyFileMockMvc.perform(get("/api/other-residency-files/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOtherResidencyFile() throws Exception {
        // Initialize the database
        otherResidencyFileRepository.saveAndFlush(otherResidencyFile);

        int databaseSizeBeforeUpdate = otherResidencyFileRepository.findAll().size();

        // Update the otherResidencyFile
        OtherResidencyFile updatedOtherResidencyFile = otherResidencyFileRepository.findById(otherResidencyFile.getId()).get();
        // Disconnect from session so that the updates on updatedOtherResidencyFile are not directly saved in db
        em.detach(updatedOtherResidencyFile);
        updatedOtherResidencyFile
            .path(UPDATED_PATH);
        OtherResidencyFileDTO otherResidencyFileDTO = otherResidencyFileMapper.toDto(updatedOtherResidencyFile);

        restOtherResidencyFileMockMvc.perform(put("/api/other-residency-files")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(otherResidencyFileDTO)))
            .andExpect(status().isOk());

        // Validate the OtherResidencyFile in the database
        List<OtherResidencyFile> otherResidencyFileList = otherResidencyFileRepository.findAll();
        assertThat(otherResidencyFileList).hasSize(databaseSizeBeforeUpdate);
        OtherResidencyFile testOtherResidencyFile = otherResidencyFileList.get(otherResidencyFileList.size() - 1);
        assertThat(testOtherResidencyFile.getPath()).isEqualTo(UPDATED_PATH);
    }

    @Test
    @Transactional
    public void updateNonExistingOtherResidencyFile() throws Exception {
        int databaseSizeBeforeUpdate = otherResidencyFileRepository.findAll().size();

        // Create the OtherResidencyFile
        OtherResidencyFileDTO otherResidencyFileDTO = otherResidencyFileMapper.toDto(otherResidencyFile);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOtherResidencyFileMockMvc.perform(put("/api/other-residency-files")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(otherResidencyFileDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OtherResidencyFile in the database
        List<OtherResidencyFile> otherResidencyFileList = otherResidencyFileRepository.findAll();
        assertThat(otherResidencyFileList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOtherResidencyFile() throws Exception {
        // Initialize the database
        otherResidencyFileRepository.saveAndFlush(otherResidencyFile);

        int databaseSizeBeforeDelete = otherResidencyFileRepository.findAll().size();

        // Delete the otherResidencyFile
        restOtherResidencyFileMockMvc.perform(delete("/api/other-residency-files/{id}", otherResidencyFile.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OtherResidencyFile> otherResidencyFileList = otherResidencyFileRepository.findAll();
        assertThat(otherResidencyFileList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
