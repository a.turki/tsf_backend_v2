package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsBackendApp;
import com.attijeri.tfs.domain.AuthentitficationToSign;
import com.attijeri.tfs.repository.AuthentitficationToSignRepository;
import com.attijeri.tfs.service.AuthentitficationToSignService;
import com.attijeri.tfs.service.dto.AuthentitficationToSignDTO;
import com.attijeri.tfs.service.mapper.AuthentitficationToSignMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AuthentitficationToSignResource} REST controller.
 */
@SpringBootTest(classes = TfsBackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AuthentitficationToSignResourceIT {

    private static final String DEFAULT_EMAIL = "AAAAAAAAAA";
    private static final String UPDATED_EMAIL = "BBBBBBBBBB";

    private static final String DEFAULT_TOKEN = "AAAAAAAAAA";
    private static final String UPDATED_TOKEN = "BBBBBBBBBB";

    private static final LocalDate DEFAULT_DATE_CREATION = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_DATE_CREATION = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_VALIDE = false;
    private static final Boolean UPDATED_VALIDE = true;

    @Autowired
    private AuthentitficationToSignRepository authentitficationToSignRepository;

    @Autowired
    private AuthentitficationToSignMapper authentitficationToSignMapper;

    @Autowired
    private AuthentitficationToSignService authentitficationToSignService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAuthentitficationToSignMockMvc;

    private AuthentitficationToSign authentitficationToSign;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AuthentitficationToSign createEntity(EntityManager em) {
        AuthentitficationToSign authentitficationToSign = new AuthentitficationToSign()
            .email(DEFAULT_EMAIL)
            .token(DEFAULT_TOKEN)
            //.dateCreation(DEFAULT_DATE_CREATION)
            .valide(DEFAULT_VALIDE);
        return authentitficationToSign;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AuthentitficationToSign createUpdatedEntity(EntityManager em) {
        AuthentitficationToSign authentitficationToSign = new AuthentitficationToSign()
            .email(UPDATED_EMAIL)
            .token(UPDATED_TOKEN)
          //  .dateCreation(UPDATED_DATE_CREATION)
            .valide(UPDATED_VALIDE);
        return authentitficationToSign;
    }

    @BeforeEach
    public void initTest() {
        authentitficationToSign = createEntity(em);
    }

    @Test
    @Transactional
    public void createAuthentitficationToSign() throws Exception {
        int databaseSizeBeforeCreate = authentitficationToSignRepository.findAll().size();
        // Create the AuthentitficationToSign
        AuthentitficationToSignDTO authentitficationToSignDTO = authentitficationToSignMapper.toDto(authentitficationToSign);
        restAuthentitficationToSignMockMvc.perform(post("/api/authentitfication-to-signs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(authentitficationToSignDTO)))
            .andExpect(status().isCreated());

        // Validate the AuthentitficationToSign in the database
        List<AuthentitficationToSign> authentitficationToSignList = authentitficationToSignRepository.findAll();
        assertThat(authentitficationToSignList).hasSize(databaseSizeBeforeCreate + 1);
        AuthentitficationToSign testAuthentitficationToSign = authentitficationToSignList.get(authentitficationToSignList.size() - 1);
        assertThat(testAuthentitficationToSign.getEmail()).isEqualTo(DEFAULT_EMAIL);
        assertThat(testAuthentitficationToSign.getToken()).isEqualTo(DEFAULT_TOKEN);
        assertThat(testAuthentitficationToSign.getDateCreation()).isEqualTo(DEFAULT_DATE_CREATION);
        assertThat(testAuthentitficationToSign.isValide()).isEqualTo(DEFAULT_VALIDE);
    }

    @Test
    @Transactional
    public void createAuthentitficationToSignWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = authentitficationToSignRepository.findAll().size();

        // Create the AuthentitficationToSign with an existing ID
        authentitficationToSign.setId(1L);
        AuthentitficationToSignDTO authentitficationToSignDTO = authentitficationToSignMapper.toDto(authentitficationToSign);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAuthentitficationToSignMockMvc.perform(post("/api/authentitfication-to-signs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(authentitficationToSignDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AuthentitficationToSign in the database
        List<AuthentitficationToSign> authentitficationToSignList = authentitficationToSignRepository.findAll();
        assertThat(authentitficationToSignList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAuthentitficationToSigns() throws Exception {
        // Initialize the database
        authentitficationToSignRepository.saveAndFlush(authentitficationToSign);

        // Get all the authentitficationToSignList
        restAuthentitficationToSignMockMvc.perform(get("/api/authentitfication-to-signs?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(authentitficationToSign.getId().intValue())))
            .andExpect(jsonPath("$.[*].email").value(hasItem(DEFAULT_EMAIL)))
            .andExpect(jsonPath("$.[*].token").value(hasItem(DEFAULT_TOKEN)))
            .andExpect(jsonPath("$.[*].dateCreation").value(hasItem(DEFAULT_DATE_CREATION.toString())))
            .andExpect(jsonPath("$.[*].valide").value(hasItem(DEFAULT_VALIDE.booleanValue())));
    }

    @Test
    @Transactional
    public void getAuthentitficationToSign() throws Exception {
        // Initialize the database
        authentitficationToSignRepository.saveAndFlush(authentitficationToSign);

        // Get the authentitficationToSign
        restAuthentitficationToSignMockMvc.perform(get("/api/authentitfication-to-signs/{id}", authentitficationToSign.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(authentitficationToSign.getId().intValue()))
            .andExpect(jsonPath("$.email").value(DEFAULT_EMAIL))
            .andExpect(jsonPath("$.token").value(DEFAULT_TOKEN))
            .andExpect(jsonPath("$.dateCreation").value(DEFAULT_DATE_CREATION.toString()))
            .andExpect(jsonPath("$.valide").value(DEFAULT_VALIDE.booleanValue()));
    }
    @Test
    @Transactional
    public void getNonExistingAuthentitficationToSign() throws Exception {
        // Get the authentitficationToSign
        restAuthentitficationToSignMockMvc.perform(get("/api/authentitfication-to-signs/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAuthentitficationToSign() throws Exception {
        // Initialize the database
        authentitficationToSignRepository.saveAndFlush(authentitficationToSign);

        int databaseSizeBeforeUpdate = authentitficationToSignRepository.findAll().size();

        // Update the authentitficationToSign
        AuthentitficationToSign updatedAuthentitficationToSign = authentitficationToSignRepository.findById(authentitficationToSign.getId()).get();
        // Disconnect from session so that the updates on updatedAuthentitficationToSign are not directly saved in db
        em.detach(updatedAuthentitficationToSign);
        updatedAuthentitficationToSign
            .email(UPDATED_EMAIL)
            .token(UPDATED_TOKEN)
         //   .dateCreation(UPDATED_DATE_CREATION)
            .valide(UPDATED_VALIDE);
        AuthentitficationToSignDTO authentitficationToSignDTO = authentitficationToSignMapper.toDto(updatedAuthentitficationToSign);

        restAuthentitficationToSignMockMvc.perform(put("/api/authentitfication-to-signs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(authentitficationToSignDTO)))
            .andExpect(status().isOk());

        // Validate the AuthentitficationToSign in the database
        List<AuthentitficationToSign> authentitficationToSignList = authentitficationToSignRepository.findAll();
        assertThat(authentitficationToSignList).hasSize(databaseSizeBeforeUpdate);
        AuthentitficationToSign testAuthentitficationToSign = authentitficationToSignList.get(authentitficationToSignList.size() - 1);
        assertThat(testAuthentitficationToSign.getEmail()).isEqualTo(UPDATED_EMAIL);
        assertThat(testAuthentitficationToSign.getToken()).isEqualTo(UPDATED_TOKEN);
        assertThat(testAuthentitficationToSign.getDateCreation()).isEqualTo(UPDATED_DATE_CREATION);
        assertThat(testAuthentitficationToSign.isValide()).isEqualTo(UPDATED_VALIDE);
    }

    @Test
    @Transactional
    public void updateNonExistingAuthentitficationToSign() throws Exception {
        int databaseSizeBeforeUpdate = authentitficationToSignRepository.findAll().size();

        // Create the AuthentitficationToSign
        AuthentitficationToSignDTO authentitficationToSignDTO = authentitficationToSignMapper.toDto(authentitficationToSign);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAuthentitficationToSignMockMvc.perform(put("/api/authentitfication-to-signs")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(authentitficationToSignDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AuthentitficationToSign in the database
        List<AuthentitficationToSign> authentitficationToSignList = authentitficationToSignRepository.findAll();
        assertThat(authentitficationToSignList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAuthentitficationToSign() throws Exception {
        // Initialize the database
        authentitficationToSignRepository.saveAndFlush(authentitficationToSign);

        int databaseSizeBeforeDelete = authentitficationToSignRepository.findAll().size();

        // Delete the authentitficationToSign
        restAuthentitficationToSignMockMvc.perform(delete("/api/authentitfication-to-signs/{id}", authentitficationToSign.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AuthentitficationToSign> authentitficationToSignList = authentitficationToSignRepository.findAll();
        assertThat(authentitficationToSignList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
