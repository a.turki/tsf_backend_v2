package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsBackendApp;
import com.attijeri.tfs.domain.Governorate;
import com.attijeri.tfs.repository.GovernorateRepository;
import com.attijeri.tfs.service.GovernorateService;
import com.attijeri.tfs.service.dto.GovernorateDTO;
import com.attijeri.tfs.service.mapper.GovernorateMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link GovernorateResource} REST controller.
 */
@SpringBootTest(classes = TfsBackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class GovernorateResourceIT {

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private GovernorateRepository governorateRepository;

    @Autowired
    private GovernorateMapper governorateMapper;

    @Autowired
    private GovernorateService governorateService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restGovernorateMockMvc;

    private Governorate governorate;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Governorate createEntity(EntityManager em) {
        Governorate governorate = new Governorate()
            .name(DEFAULT_NAME);
        return governorate;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Governorate createUpdatedEntity(EntityManager em) {
        Governorate governorate = new Governorate()
            .name(UPDATED_NAME);
        return governorate;
    }

    @BeforeEach
    public void initTest() {
        governorate = createEntity(em);
    }

    @Test
    @Transactional
    public void createGovernorate() throws Exception {
        int databaseSizeBeforeCreate = governorateRepository.findAll().size();
        // Create the Governorate
        GovernorateDTO governorateDTO = governorateMapper.toDto(governorate);
        restGovernorateMockMvc.perform(post("/api/governorates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(governorateDTO)))
            .andExpect(status().isCreated());

        // Validate the Governorate in the database
        List<Governorate> governorateList = governorateRepository.findAll();
        assertThat(governorateList).hasSize(databaseSizeBeforeCreate + 1);
        Governorate testGovernorate = governorateList.get(governorateList.size() - 1);
        assertThat(testGovernorate.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createGovernorateWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = governorateRepository.findAll().size();

        // Create the Governorate with an existing ID
        governorate.setId(1L);
        GovernorateDTO governorateDTO = governorateMapper.toDto(governorate);

        // An entity with an existing ID cannot be created, so this API call must fail
        restGovernorateMockMvc.perform(post("/api/governorates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(governorateDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Governorate in the database
        List<Governorate> governorateList = governorateRepository.findAll();
        assertThat(governorateList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllGovernorates() throws Exception {
        // Initialize the database
        governorateRepository.saveAndFlush(governorate);

        // Get all the governorateList
        restGovernorateMockMvc.perform(get("/api/governorates?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(governorate.getId().intValue())))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME)));
    }
    
    @Test
    @Transactional
    public void getGovernorate() throws Exception {
        // Initialize the database
        governorateRepository.saveAndFlush(governorate);

        // Get the governorate
        restGovernorateMockMvc.perform(get("/api/governorates/{id}", governorate.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(governorate.getId().intValue()))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME));
    }
    @Test
    @Transactional
    public void getNonExistingGovernorate() throws Exception {
        // Get the governorate
        restGovernorateMockMvc.perform(get("/api/governorates/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateGovernorate() throws Exception {
        // Initialize the database
        governorateRepository.saveAndFlush(governorate);

        int databaseSizeBeforeUpdate = governorateRepository.findAll().size();

        // Update the governorate
        Governorate updatedGovernorate = governorateRepository.findById(governorate.getId()).get();
        // Disconnect from session so that the updates on updatedGovernorate are not directly saved in db
        em.detach(updatedGovernorate);
        updatedGovernorate
            .name(UPDATED_NAME);
        GovernorateDTO governorateDTO = governorateMapper.toDto(updatedGovernorate);

        restGovernorateMockMvc.perform(put("/api/governorates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(governorateDTO)))
            .andExpect(status().isOk());

        // Validate the Governorate in the database
        List<Governorate> governorateList = governorateRepository.findAll();
        assertThat(governorateList).hasSize(databaseSizeBeforeUpdate);
        Governorate testGovernorate = governorateList.get(governorateList.size() - 1);
        assertThat(testGovernorate.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingGovernorate() throws Exception {
        int databaseSizeBeforeUpdate = governorateRepository.findAll().size();

        // Create the Governorate
        GovernorateDTO governorateDTO = governorateMapper.toDto(governorate);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restGovernorateMockMvc.perform(put("/api/governorates")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(governorateDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Governorate in the database
        List<Governorate> governorateList = governorateRepository.findAll();
        assertThat(governorateList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteGovernorate() throws Exception {
        // Initialize the database
        governorateRepository.saveAndFlush(governorate);

        int databaseSizeBeforeDelete = governorateRepository.findAll().size();

        // Delete the governorate
        restGovernorateMockMvc.perform(delete("/api/governorates/{id}", governorate.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Governorate> governorateList = governorateRepository.findAll();
        assertThat(governorateList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
