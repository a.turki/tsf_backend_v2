package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsBackendApp;
import com.attijeri.tfs.domain.OtherRevenuFile;
import com.attijeri.tfs.repository.OtherRevenuFileRepository;
import com.attijeri.tfs.service.OtherRevenuFileService;
import com.attijeri.tfs.service.dto.OtherRevenuFileDTO;
import com.attijeri.tfs.service.mapper.OtherRevenuFileMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link OtherRevenuFileResource} REST controller.
 */
@SpringBootTest(classes = TfsBackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class OtherRevenuFileResourceIT {

    private static final String DEFAULT_PATH = "AAAAAAAAAA";
    private static final String UPDATED_PATH = "BBBBBBBBBB";

    @Autowired
    private OtherRevenuFileRepository otherRevenuFileRepository;

    @Autowired
    private OtherRevenuFileMapper otherRevenuFileMapper;

    @Autowired
    private OtherRevenuFileService otherRevenuFileService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restOtherRevenuFileMockMvc;

    private OtherRevenuFile otherRevenuFile;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OtherRevenuFile createEntity(EntityManager em) {
        OtherRevenuFile otherRevenuFile = new OtherRevenuFile()
            .path(DEFAULT_PATH);
        return otherRevenuFile;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static OtherRevenuFile createUpdatedEntity(EntityManager em) {
        OtherRevenuFile otherRevenuFile = new OtherRevenuFile()
            .path(UPDATED_PATH);
        return otherRevenuFile;
    }

    @BeforeEach
    public void initTest() {
        otherRevenuFile = createEntity(em);
    }

    @Test
    @Transactional
    public void createOtherRevenuFile() throws Exception {
        int databaseSizeBeforeCreate = otherRevenuFileRepository.findAll().size();
        // Create the OtherRevenuFile
        OtherRevenuFileDTO otherRevenuFileDTO = otherRevenuFileMapper.toDto(otherRevenuFile);
        restOtherRevenuFileMockMvc.perform(post("/api/other-revenu-files")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(otherRevenuFileDTO)))
            .andExpect(status().isCreated());

        // Validate the OtherRevenuFile in the database
        List<OtherRevenuFile> otherRevenuFileList = otherRevenuFileRepository.findAll();
        assertThat(otherRevenuFileList).hasSize(databaseSizeBeforeCreate + 1);
        OtherRevenuFile testOtherRevenuFile = otherRevenuFileList.get(otherRevenuFileList.size() - 1);
        assertThat(testOtherRevenuFile.getPath()).isEqualTo(DEFAULT_PATH);
    }

    @Test
    @Transactional
    public void createOtherRevenuFileWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = otherRevenuFileRepository.findAll().size();

        // Create the OtherRevenuFile with an existing ID
        otherRevenuFile.setId(1L);
        OtherRevenuFileDTO otherRevenuFileDTO = otherRevenuFileMapper.toDto(otherRevenuFile);

        // An entity with an existing ID cannot be created, so this API call must fail
        restOtherRevenuFileMockMvc.perform(post("/api/other-revenu-files")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(otherRevenuFileDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OtherRevenuFile in the database
        List<OtherRevenuFile> otherRevenuFileList = otherRevenuFileRepository.findAll();
        assertThat(otherRevenuFileList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllOtherRevenuFiles() throws Exception {
        // Initialize the database
        otherRevenuFileRepository.saveAndFlush(otherRevenuFile);

        // Get all the otherRevenuFileList
        restOtherRevenuFileMockMvc.perform(get("/api/other-revenu-files?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(otherRevenuFile.getId().intValue())))
            .andExpect(jsonPath("$.[*].path").value(hasItem(DEFAULT_PATH)));
    }
    
    @Test
    @Transactional
    public void getOtherRevenuFile() throws Exception {
        // Initialize the database
        otherRevenuFileRepository.saveAndFlush(otherRevenuFile);

        // Get the otherRevenuFile
        restOtherRevenuFileMockMvc.perform(get("/api/other-revenu-files/{id}", otherRevenuFile.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(otherRevenuFile.getId().intValue()))
            .andExpect(jsonPath("$.path").value(DEFAULT_PATH));
    }
    @Test
    @Transactional
    public void getNonExistingOtherRevenuFile() throws Exception {
        // Get the otherRevenuFile
        restOtherRevenuFileMockMvc.perform(get("/api/other-revenu-files/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateOtherRevenuFile() throws Exception {
        // Initialize the database
        otherRevenuFileRepository.saveAndFlush(otherRevenuFile);

        int databaseSizeBeforeUpdate = otherRevenuFileRepository.findAll().size();

        // Update the otherRevenuFile
        OtherRevenuFile updatedOtherRevenuFile = otherRevenuFileRepository.findById(otherRevenuFile.getId()).get();
        // Disconnect from session so that the updates on updatedOtherRevenuFile are not directly saved in db
        em.detach(updatedOtherRevenuFile);
        updatedOtherRevenuFile
            .path(UPDATED_PATH);
        OtherRevenuFileDTO otherRevenuFileDTO = otherRevenuFileMapper.toDto(updatedOtherRevenuFile);

        restOtherRevenuFileMockMvc.perform(put("/api/other-revenu-files")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(otherRevenuFileDTO)))
            .andExpect(status().isOk());

        // Validate the OtherRevenuFile in the database
        List<OtherRevenuFile> otherRevenuFileList = otherRevenuFileRepository.findAll();
        assertThat(otherRevenuFileList).hasSize(databaseSizeBeforeUpdate);
        OtherRevenuFile testOtherRevenuFile = otherRevenuFileList.get(otherRevenuFileList.size() - 1);
        assertThat(testOtherRevenuFile.getPath()).isEqualTo(UPDATED_PATH);
    }

    @Test
    @Transactional
    public void updateNonExistingOtherRevenuFile() throws Exception {
        int databaseSizeBeforeUpdate = otherRevenuFileRepository.findAll().size();

        // Create the OtherRevenuFile
        OtherRevenuFileDTO otherRevenuFileDTO = otherRevenuFileMapper.toDto(otherRevenuFile);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restOtherRevenuFileMockMvc.perform(put("/api/other-revenu-files")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(otherRevenuFileDTO)))
            .andExpect(status().isBadRequest());

        // Validate the OtherRevenuFile in the database
        List<OtherRevenuFile> otherRevenuFileList = otherRevenuFileRepository.findAll();
        assertThat(otherRevenuFileList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteOtherRevenuFile() throws Exception {
        // Initialize the database
        otherRevenuFileRepository.saveAndFlush(otherRevenuFile);

        int databaseSizeBeforeDelete = otherRevenuFileRepository.findAll().size();

        // Delete the otherRevenuFile
        restOtherRevenuFileMockMvc.perform(delete("/api/other-revenu-files/{id}", otherRevenuFile.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<OtherRevenuFile> otherRevenuFileList = otherRevenuFileRepository.findAll();
        assertThat(otherRevenuFileList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
