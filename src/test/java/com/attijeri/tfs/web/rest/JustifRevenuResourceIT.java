package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsBackendApp;
import com.attijeri.tfs.domain.JustifRevenu;
import com.attijeri.tfs.repository.JustifRevenuRepository;
import com.attijeri.tfs.service.JustifRevenuService;
import com.attijeri.tfs.service.dto.JustifRevenuDTO;
import com.attijeri.tfs.service.mapper.JustifRevenuMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link JustifRevenuResource} REST controller.
 */
@SpringBootTest(classes = TfsBackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class JustifRevenuResourceIT {

    private static final String DEFAULT_LABEL_EN = "AAAAAAAAAA";
    private static final String UPDATED_LABEL_EN = "BBBBBBBBBB";

    private static final String DEFAULT_LABEL_FR = "AAAAAAAAAA";
    private static final String UPDATED_LABEL_FR = "BBBBBBBBBB";

    @Autowired
    private JustifRevenuRepository justifRevenuRepository;

    @Autowired
    private JustifRevenuMapper justifRevenuMapper;

    @Autowired
    private JustifRevenuService justifRevenuService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restJustifRevenuMockMvc;

    private JustifRevenu justifRevenu;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static JustifRevenu createEntity(EntityManager em) {
        JustifRevenu justifRevenu = new JustifRevenu()
            .labelEN(DEFAULT_LABEL_EN)
            .labelFR(DEFAULT_LABEL_FR);
        return justifRevenu;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static JustifRevenu createUpdatedEntity(EntityManager em) {
        JustifRevenu justifRevenu = new JustifRevenu()
            .labelEN(UPDATED_LABEL_EN)
            .labelFR(UPDATED_LABEL_FR);
        return justifRevenu;
    }

    @BeforeEach
    public void initTest() {
        justifRevenu = createEntity(em);
    }

    @Test
    @Transactional
    public void createJustifRevenu() throws Exception {
        int databaseSizeBeforeCreate = justifRevenuRepository.findAll().size();
        // Create the JustifRevenu
        JustifRevenuDTO justifRevenuDTO = justifRevenuMapper.toDto(justifRevenu);
        restJustifRevenuMockMvc.perform(post("/api/justif-revenus")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(justifRevenuDTO)))
            .andExpect(status().isCreated());

        // Validate the JustifRevenu in the database
        List<JustifRevenu> justifRevenuList = justifRevenuRepository.findAll();
        assertThat(justifRevenuList).hasSize(databaseSizeBeforeCreate + 1);
        JustifRevenu testJustifRevenu = justifRevenuList.get(justifRevenuList.size() - 1);
        assertThat(testJustifRevenu.getLabelEN()).isEqualTo(DEFAULT_LABEL_EN);
        assertThat(testJustifRevenu.getLabelFR()).isEqualTo(DEFAULT_LABEL_FR);
    }

    @Test
    @Transactional
    public void createJustifRevenuWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = justifRevenuRepository.findAll().size();

        // Create the JustifRevenu with an existing ID
        justifRevenu.setId(1L);
        JustifRevenuDTO justifRevenuDTO = justifRevenuMapper.toDto(justifRevenu);

        // An entity with an existing ID cannot be created, so this API call must fail
        restJustifRevenuMockMvc.perform(post("/api/justif-revenus")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(justifRevenuDTO)))
            .andExpect(status().isBadRequest());

        // Validate the JustifRevenu in the database
        List<JustifRevenu> justifRevenuList = justifRevenuRepository.findAll();
        assertThat(justifRevenuList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllJustifRevenus() throws Exception {
        // Initialize the database
        justifRevenuRepository.saveAndFlush(justifRevenu);

        // Get all the justifRevenuList
        restJustifRevenuMockMvc.perform(get("/api/justif-revenus?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(justifRevenu.getId().intValue())))
            .andExpect(jsonPath("$.[*].labelEN").value(hasItem(DEFAULT_LABEL_EN)))
            .andExpect(jsonPath("$.[*].labelFR").value(hasItem(DEFAULT_LABEL_FR)));
    }
    
    @Test
    @Transactional
    public void getJustifRevenu() throws Exception {
        // Initialize the database
        justifRevenuRepository.saveAndFlush(justifRevenu);

        // Get the justifRevenu
        restJustifRevenuMockMvc.perform(get("/api/justif-revenus/{id}", justifRevenu.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(justifRevenu.getId().intValue()))
            .andExpect(jsonPath("$.labelEN").value(DEFAULT_LABEL_EN))
            .andExpect(jsonPath("$.labelFR").value(DEFAULT_LABEL_FR));
    }
    @Test
    @Transactional
    public void getNonExistingJustifRevenu() throws Exception {
        // Get the justifRevenu
        restJustifRevenuMockMvc.perform(get("/api/justif-revenus/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateJustifRevenu() throws Exception {
        // Initialize the database
        justifRevenuRepository.saveAndFlush(justifRevenu);

        int databaseSizeBeforeUpdate = justifRevenuRepository.findAll().size();

        // Update the justifRevenu
        JustifRevenu updatedJustifRevenu = justifRevenuRepository.findById(justifRevenu.getId()).get();
        // Disconnect from session so that the updates on updatedJustifRevenu are not directly saved in db
        em.detach(updatedJustifRevenu);
        updatedJustifRevenu
            .labelEN(UPDATED_LABEL_EN)
            .labelFR(UPDATED_LABEL_FR);
        JustifRevenuDTO justifRevenuDTO = justifRevenuMapper.toDto(updatedJustifRevenu);

        restJustifRevenuMockMvc.perform(put("/api/justif-revenus")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(justifRevenuDTO)))
            .andExpect(status().isOk());

        // Validate the JustifRevenu in the database
        List<JustifRevenu> justifRevenuList = justifRevenuRepository.findAll();
        assertThat(justifRevenuList).hasSize(databaseSizeBeforeUpdate);
        JustifRevenu testJustifRevenu = justifRevenuList.get(justifRevenuList.size() - 1);
        assertThat(testJustifRevenu.getLabelEN()).isEqualTo(UPDATED_LABEL_EN);
        assertThat(testJustifRevenu.getLabelFR()).isEqualTo(UPDATED_LABEL_FR);
    }

    @Test
    @Transactional
    public void updateNonExistingJustifRevenu() throws Exception {
        int databaseSizeBeforeUpdate = justifRevenuRepository.findAll().size();

        // Create the JustifRevenu
        JustifRevenuDTO justifRevenuDTO = justifRevenuMapper.toDto(justifRevenu);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restJustifRevenuMockMvc.perform(put("/api/justif-revenus")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(justifRevenuDTO)))
            .andExpect(status().isBadRequest());

        // Validate the JustifRevenu in the database
        List<JustifRevenu> justifRevenuList = justifRevenuRepository.findAll();
        assertThat(justifRevenuList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteJustifRevenu() throws Exception {
        // Initialize the database
        justifRevenuRepository.saveAndFlush(justifRevenu);

        int databaseSizeBeforeDelete = justifRevenuRepository.findAll().size();

        // Delete the justifRevenu
        restJustifRevenuMockMvc.perform(delete("/api/justif-revenus/{id}", justifRevenu.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<JustifRevenu> justifRevenuList = justifRevenuRepository.findAll();
        assertThat(justifRevenuList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
