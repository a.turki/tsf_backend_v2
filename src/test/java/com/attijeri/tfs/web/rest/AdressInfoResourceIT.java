package com.attijeri.tfs.web.rest;

import com.attijeri.tfs.TfsBackendApp;
import com.attijeri.tfs.domain.AdressInfo;
import com.attijeri.tfs.repository.AdressInfoRepository;
import com.attijeri.tfs.service.AdressInfoService;
import com.attijeri.tfs.service.dto.AdressInfoDTO;
import com.attijeri.tfs.service.mapper.AdressInfoMapper;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Integration tests for the {@link AdressInfoResource} REST controller.
 */
@SpringBootTest(classes = TfsBackendApp.class)
@AutoConfigureMockMvc
@WithMockUser
public class AdressInfoResourceIT {

    private static final String DEFAULT_COUNTRY_OF_RESIDENCE = "AAAAAAAAAA";
    private static final String UPDATED_COUNTRY_OF_RESIDENCE = "BBBBBBBBBB";

    private static final String DEFAULT_ADDRESS = "AAAAAAAAAA";
    private static final String UPDATED_ADDRESS = "BBBBBBBBBB";

    private static final Integer DEFAULT_ZIP = 1;
    private static final Integer UPDATED_ZIP = 2;

    private static final String DEFAULT_CITY = "AAAAAAAAAA";
    private static final String UPDATED_CITY = "BBBBBBBBBB";

    @Autowired
    private AdressInfoRepository adressInfoRepository;

    @Autowired
    private AdressInfoMapper adressInfoMapper;

    @Autowired
    private AdressInfoService adressInfoService;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restAdressInfoMockMvc;

    private AdressInfo adressInfo;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AdressInfo createEntity(EntityManager em) {
        AdressInfo adressInfo = new AdressInfo()
            .countryOfResidence(DEFAULT_COUNTRY_OF_RESIDENCE)
            .address(DEFAULT_ADDRESS)
            .zip(DEFAULT_ZIP)
            .city(DEFAULT_CITY);
        return adressInfo;
    }
    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static AdressInfo createUpdatedEntity(EntityManager em) {
        AdressInfo adressInfo = new AdressInfo()
            .countryOfResidence(UPDATED_COUNTRY_OF_RESIDENCE)
            .address(UPDATED_ADDRESS)
            .zip(UPDATED_ZIP)
            .city(UPDATED_CITY);
        return adressInfo;
    }

    @BeforeEach
    public void initTest() {
        adressInfo = createEntity(em);
    }

    @Test
    @Transactional
    public void createAdressInfo() throws Exception {
        int databaseSizeBeforeCreate = adressInfoRepository.findAll().size();
        // Create the AdressInfo
        AdressInfoDTO adressInfoDTO = adressInfoMapper.toDto(adressInfo);
        restAdressInfoMockMvc.perform(post("/api/adress-infos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(adressInfoDTO)))
            .andExpect(status().isCreated());

        // Validate the AdressInfo in the database
        List<AdressInfo> adressInfoList = adressInfoRepository.findAll();
        assertThat(adressInfoList).hasSize(databaseSizeBeforeCreate + 1);
        AdressInfo testAdressInfo = adressInfoList.get(adressInfoList.size() - 1);
        assertThat(testAdressInfo.getCountryOfResidence()).isEqualTo(DEFAULT_COUNTRY_OF_RESIDENCE);
        assertThat(testAdressInfo.getAddress()).isEqualTo(DEFAULT_ADDRESS);
        assertThat(testAdressInfo.getZip()).isEqualTo(DEFAULT_ZIP);
        assertThat(testAdressInfo.getCity()).isEqualTo(DEFAULT_CITY);
    }

    @Test
    @Transactional
    public void createAdressInfoWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = adressInfoRepository.findAll().size();

        // Create the AdressInfo with an existing ID
        adressInfo.setId(1L);
        AdressInfoDTO adressInfoDTO = adressInfoMapper.toDto(adressInfo);

        // An entity with an existing ID cannot be created, so this API call must fail
        restAdressInfoMockMvc.perform(post("/api/adress-infos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(adressInfoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AdressInfo in the database
        List<AdressInfo> adressInfoList = adressInfoRepository.findAll();
        assertThat(adressInfoList).hasSize(databaseSizeBeforeCreate);
    }


    @Test
    @Transactional
    public void getAllAdressInfos() throws Exception {
        // Initialize the database
        adressInfoRepository.saveAndFlush(adressInfo);

        // Get all the adressInfoList
        restAdressInfoMockMvc.perform(get("/api/adress-infos?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(adressInfo.getId().intValue())))
            .andExpect(jsonPath("$.[*].countryOfResidence").value(hasItem(DEFAULT_COUNTRY_OF_RESIDENCE)))
            .andExpect(jsonPath("$.[*].address").value(hasItem(DEFAULT_ADDRESS)))
            .andExpect(jsonPath("$.[*].zip").value(hasItem(DEFAULT_ZIP)))
            .andExpect(jsonPath("$.[*].city").value(hasItem(DEFAULT_CITY)));
    }
    
    @Test
    @Transactional
    public void getAdressInfo() throws Exception {
        // Initialize the database
        adressInfoRepository.saveAndFlush(adressInfo);

        // Get the adressInfo
        restAdressInfoMockMvc.perform(get("/api/adress-infos/{id}", adressInfo.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(adressInfo.getId().intValue()))
            .andExpect(jsonPath("$.countryOfResidence").value(DEFAULT_COUNTRY_OF_RESIDENCE))
            .andExpect(jsonPath("$.address").value(DEFAULT_ADDRESS))
            .andExpect(jsonPath("$.zip").value(DEFAULT_ZIP))
            .andExpect(jsonPath("$.city").value(DEFAULT_CITY));
    }
    @Test
    @Transactional
    public void getNonExistingAdressInfo() throws Exception {
        // Get the adressInfo
        restAdressInfoMockMvc.perform(get("/api/adress-infos/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateAdressInfo() throws Exception {
        // Initialize the database
        adressInfoRepository.saveAndFlush(adressInfo);

        int databaseSizeBeforeUpdate = adressInfoRepository.findAll().size();

        // Update the adressInfo
        AdressInfo updatedAdressInfo = adressInfoRepository.findById(adressInfo.getId()).get();
        // Disconnect from session so that the updates on updatedAdressInfo are not directly saved in db
        em.detach(updatedAdressInfo);
        updatedAdressInfo
            .countryOfResidence(UPDATED_COUNTRY_OF_RESIDENCE)
            .address(UPDATED_ADDRESS)
            .zip(UPDATED_ZIP)
            .city(UPDATED_CITY);
        AdressInfoDTO adressInfoDTO = adressInfoMapper.toDto(updatedAdressInfo);

        restAdressInfoMockMvc.perform(put("/api/adress-infos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(adressInfoDTO)))
            .andExpect(status().isOk());

        // Validate the AdressInfo in the database
        List<AdressInfo> adressInfoList = adressInfoRepository.findAll();
        assertThat(adressInfoList).hasSize(databaseSizeBeforeUpdate);
        AdressInfo testAdressInfo = adressInfoList.get(adressInfoList.size() - 1);
        assertThat(testAdressInfo.getCountryOfResidence()).isEqualTo(UPDATED_COUNTRY_OF_RESIDENCE);
        assertThat(testAdressInfo.getAddress()).isEqualTo(UPDATED_ADDRESS);
        assertThat(testAdressInfo.getZip()).isEqualTo(UPDATED_ZIP);
        assertThat(testAdressInfo.getCity()).isEqualTo(UPDATED_CITY);
    }

    @Test
    @Transactional
    public void updateNonExistingAdressInfo() throws Exception {
        int databaseSizeBeforeUpdate = adressInfoRepository.findAll().size();

        // Create the AdressInfo
        AdressInfoDTO adressInfoDTO = adressInfoMapper.toDto(adressInfo);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restAdressInfoMockMvc.perform(put("/api/adress-infos")
            .contentType(MediaType.APPLICATION_JSON)
            .content(TestUtil.convertObjectToJsonBytes(adressInfoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the AdressInfo in the database
        List<AdressInfo> adressInfoList = adressInfoRepository.findAll();
        assertThat(adressInfoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    public void deleteAdressInfo() throws Exception {
        // Initialize the database
        adressInfoRepository.saveAndFlush(adressInfo);

        int databaseSizeBeforeDelete = adressInfoRepository.findAll().size();

        // Delete the adressInfo
        restAdressInfoMockMvc.perform(delete("/api/adress-infos/{id}", adressInfo.getId())
            .accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<AdressInfo> adressInfoList = adressInfoRepository.findAll();
        assertThat(adressInfoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
