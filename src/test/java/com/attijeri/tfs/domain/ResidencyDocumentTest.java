package com.attijeri.tfs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.attijeri.tfs.web.rest.TestUtil;

public class ResidencyDocumentTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ResidencyDocument.class);
        ResidencyDocument residencyDocument1 = new ResidencyDocument();
        residencyDocument1.setId(1L);
        ResidencyDocument residencyDocument2 = new ResidencyDocument();
        residencyDocument2.setId(residencyDocument1.getId());
        assertThat(residencyDocument1).isEqualTo(residencyDocument2);
        residencyDocument2.setId(2L);
        assertThat(residencyDocument1).isNotEqualTo(residencyDocument2);
        residencyDocument1.setId(null);
        assertThat(residencyDocument1).isNotEqualTo(residencyDocument2);
    }
}
