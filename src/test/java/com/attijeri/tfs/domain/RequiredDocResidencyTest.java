package com.attijeri.tfs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.attijeri.tfs.web.rest.TestUtil;

public class RequiredDocResidencyTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RequiredDocResidency.class);
        RequiredDocResidency requiredDocResidency1 = new RequiredDocResidency();
        requiredDocResidency1.setId(1L);
        RequiredDocResidency requiredDocResidency2 = new RequiredDocResidency();
        requiredDocResidency2.setId(requiredDocResidency1.getId());
        assertThat(requiredDocResidency1).isEqualTo(requiredDocResidency2);
        requiredDocResidency2.setId(2L);
        assertThat(requiredDocResidency1).isNotEqualTo(requiredDocResidency2);
        requiredDocResidency1.setId(null);
        assertThat(requiredDocResidency1).isNotEqualTo(requiredDocResidency2);
    }
}
