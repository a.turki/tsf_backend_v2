package com.attijeri.tfs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.attijeri.tfs.web.rest.TestUtil;

public class OtherResidencyFileTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OtherResidencyFile.class);
        OtherResidencyFile otherResidencyFile1 = new OtherResidencyFile();
        otherResidencyFile1.setId(1L);
        OtherResidencyFile otherResidencyFile2 = new OtherResidencyFile();
        otherResidencyFile2.setId(otherResidencyFile1.getId());
        assertThat(otherResidencyFile1).isEqualTo(otherResidencyFile2);
        otherResidencyFile2.setId(2L);
        assertThat(otherResidencyFile1).isNotEqualTo(otherResidencyFile2);
        otherResidencyFile1.setId(null);
        assertThat(otherResidencyFile1).isNotEqualTo(otherResidencyFile2);
    }
}
