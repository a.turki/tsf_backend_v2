package com.attijeri.tfs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.attijeri.tfs.web.rest.TestUtil;

public class OtherRevenuFileTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(OtherRevenuFile.class);
        OtherRevenuFile otherRevenuFile1 = new OtherRevenuFile();
        otherRevenuFile1.setId(1L);
        OtherRevenuFile otherRevenuFile2 = new OtherRevenuFile();
        otherRevenuFile2.setId(otherRevenuFile1.getId());
        assertThat(otherRevenuFile1).isEqualTo(otherRevenuFile2);
        otherRevenuFile2.setId(2L);
        assertThat(otherRevenuFile1).isNotEqualTo(otherRevenuFile2);
        otherRevenuFile1.setId(null);
        assertThat(otherRevenuFile1).isNotEqualTo(otherRevenuFile2);
    }
}
