package com.attijeri.tfs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.attijeri.tfs.web.rest.TestUtil;

public class RequiredDocTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RequiredDoc.class);
        RequiredDoc requiredDoc1 = new RequiredDoc();
        requiredDoc1.setId(1L);
        RequiredDoc requiredDoc2 = new RequiredDoc();
        requiredDoc2.setId(requiredDoc1.getId());
        assertThat(requiredDoc1).isEqualTo(requiredDoc2);
        requiredDoc2.setId(2L);
        assertThat(requiredDoc1).isNotEqualTo(requiredDoc2);
        requiredDoc1.setId(null);
        assertThat(requiredDoc1).isNotEqualTo(requiredDoc2);
    }
}
