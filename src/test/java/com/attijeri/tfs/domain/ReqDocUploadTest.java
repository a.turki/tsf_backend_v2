package com.attijeri.tfs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.attijeri.tfs.web.rest.TestUtil;

public class ReqDocUploadTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ReqDocUpload.class);
        ReqDocUpload reqDocUpload1 = new ReqDocUpload();
        reqDocUpload1.setId(1L);
        ReqDocUpload reqDocUpload2 = new ReqDocUpload();
        reqDocUpload2.setId(reqDocUpload1.getId());
        assertThat(reqDocUpload1).isEqualTo(reqDocUpload2);
        reqDocUpload2.setId(2L);
        assertThat(reqDocUpload1).isNotEqualTo(reqDocUpload2);
        reqDocUpload1.setId(null);
        assertThat(reqDocUpload1).isNotEqualTo(reqDocUpload2);
    }
}
