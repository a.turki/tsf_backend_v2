package com.attijeri.tfs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.attijeri.tfs.web.rest.TestUtil;

public class JustifRevenuTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(JustifRevenu.class);
        JustifRevenu justifRevenu1 = new JustifRevenu();
        justifRevenu1.setId(1L);
        JustifRevenu justifRevenu2 = new JustifRevenu();
        justifRevenu2.setId(justifRevenu1.getId());
        assertThat(justifRevenu1).isEqualTo(justifRevenu2);
        justifRevenu2.setId(2L);
        assertThat(justifRevenu1).isNotEqualTo(justifRevenu2);
        justifRevenu1.setId(null);
        assertThat(justifRevenu1).isNotEqualTo(justifRevenu2);
    }
}
