package com.attijeri.tfs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.attijeri.tfs.web.rest.TestUtil;

public class AuthentitficationToSignTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(AuthentitficationToSign.class);
        AuthentitficationToSign authentitficationToSign1 = new AuthentitficationToSign();
        authentitficationToSign1.setId(1L);
        AuthentitficationToSign authentitficationToSign2 = new AuthentitficationToSign();
        authentitficationToSign2.setId(authentitficationToSign1.getId());
        assertThat(authentitficationToSign1).isEqualTo(authentitficationToSign2);
        authentitficationToSign2.setId(2L);
        assertThat(authentitficationToSign1).isNotEqualTo(authentitficationToSign2);
        authentitficationToSign1.setId(null);
        assertThat(authentitficationToSign1).isNotEqualTo(authentitficationToSign2);
    }
}
