package com.attijeri.tfs.domain;

import org.junit.jupiter.api.Test;
import static org.assertj.core.api.Assertions.assertThat;
import com.attijeri.tfs.web.rest.TestUtil;

public class RequiredDocIncomeTest {

    @Test
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RequiredDocIncome.class);
        RequiredDocIncome requiredDocIncome1 = new RequiredDocIncome();
        requiredDocIncome1.setId(1L);
        RequiredDocIncome requiredDocIncome2 = new RequiredDocIncome();
        requiredDocIncome2.setId(requiredDocIncome1.getId());
        assertThat(requiredDocIncome1).isEqualTo(requiredDocIncome2);
        requiredDocIncome2.setId(2L);
        assertThat(requiredDocIncome1).isNotEqualTo(requiredDocIncome2);
        requiredDocIncome1.setId(null);
        assertThat(requiredDocIncome1).isNotEqualTo(requiredDocIncome2);
    }
}
