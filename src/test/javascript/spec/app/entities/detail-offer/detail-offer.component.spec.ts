import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TfsBackendTestModule } from '../../../test.module';
import { DetailOfferComponent } from 'app/entities/detail-offer/detail-offer.component';
import { DetailOfferService } from 'app/entities/detail-offer/detail-offer.service';
import { DetailOffer } from 'app/shared/model/detail-offer.model';

describe('Component Tests', () => {
  describe('DetailOffer Management Component', () => {
    let comp: DetailOfferComponent;
    let fixture: ComponentFixture<DetailOfferComponent>;
    let service: DetailOfferService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TfsBackendTestModule],
        declarations: [DetailOfferComponent],
      })
        .overrideTemplate(DetailOfferComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(DetailOfferComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(DetailOfferService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new DetailOffer(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.detailOffers && comp.detailOffers[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
