import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TfsBackendTestModule } from '../../../test.module';
import { JustifRevenuComponent } from 'app/entities/justif-revenu/justif-revenu.component';
import { JustifRevenuService } from 'app/entities/justif-revenu/justif-revenu.service';
import { JustifRevenu } from 'app/shared/model/justif-revenu.model';

describe('Component Tests', () => {
  describe('JustifRevenu Management Component', () => {
    let comp: JustifRevenuComponent;
    let fixture: ComponentFixture<JustifRevenuComponent>;
    let service: JustifRevenuService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TfsBackendTestModule],
        declarations: [JustifRevenuComponent],
      })
        .overrideTemplate(JustifRevenuComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(JustifRevenuComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(JustifRevenuService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new JustifRevenu(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.justifRevenus && comp.justifRevenus[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
