import { ComponentFixture, TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { TfsBackendTestModule } from '../../../test.module';
import { AuthentitficationToSignComponent } from 'app/entities/authentitfication-to-sign/authentitfication-to-sign.component';
import { AuthentitficationToSignService } from 'app/entities/authentitfication-to-sign/authentitfication-to-sign.service';
import { AuthentitficationToSign } from 'app/shared/model/authentitfication-to-sign.model';

describe('Component Tests', () => {
  describe('AuthentitficationToSign Management Component', () => {
    let comp: AuthentitficationToSignComponent;
    let fixture: ComponentFixture<AuthentitficationToSignComponent>;
    let service: AuthentitficationToSignService;

    beforeEach(() => {
      TestBed.configureTestingModule({
        imports: [TfsBackendTestModule],
        declarations: [AuthentitficationToSignComponent],
      })
        .overrideTemplate(AuthentitficationToSignComponent, '')
        .compileComponents();

      fixture = TestBed.createComponent(AuthentitficationToSignComponent);
      comp = fixture.componentInstance;
      service = fixture.debugElement.injector.get(AuthentitficationToSignService);
    });

    it('Should call load all on init', () => {
      // GIVEN
      const headers = new HttpHeaders().append('link', 'link;link');
      spyOn(service, 'query').and.returnValue(
        of(
          new HttpResponse({
            body: [new AuthentitficationToSign(123)],
            headers,
          })
        )
      );

      // WHEN
      comp.ngOnInit();

      // THEN
      expect(service.query).toHaveBeenCalled();
      expect(comp.authentitficationToSigns && comp.authentitficationToSigns[0]).toEqual(jasmine.objectContaining({ id: 123 }));
    });
  });
});
